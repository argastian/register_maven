$(function () {
    $('#menu-data-diri:not(.active)').on('mouseover', function () {
        $(this).find('img').attr('src', base_url + 'assets/images/client/data_diri_bold.png');
    }).on('mouseout', function () {
        $(this).find('img').attr('src', base_url + 'assets/images/client/data_diri_grey.png');
    });
    $('#menu-dokumen:not(.active)').on('mouseover', function () {
        $(this).find('img').attr('src', base_url + 'assets/images/client/dokumen_bold.png');
    }).on('mouseout', function () {
        $(this).find('img').attr('src', base_url + 'assets/images/client/dokumen_grey.png');
    });
    $('#menu-progress:not(.active)').on('mouseover', function () {
        $(this).find('img').attr('src', base_url + 'assets/images/client/progress_bold.png');
    }).on('mouseout', function () {
        $(this).find('img').attr('src', base_url + 'assets/images/client/progress_grey.png');
    });
    $('.menu-acordion').on('click', '.panel-heading', function () {
        $('.menu-acordion .panel-heading i').removeClass('fa-caret-up').addClass('fa-caret-down');
        $('.panel .panel-info').removeClass('hide');       
        if ($(this).hasClass('collapsed')) {
            $(this).find('i').removeClass('fa-caret-down').addClass('fa-caret-up');
            $(this).closest('.panel').find('.panel-info').addClass('hide');
        }
        $('.menu-acordion .panel-heading').each(function () {
            var error_count = $(this).closest('.panel').find('form .form-group.has-error').length;
            if (error_count == 0) {
                $(this).closest('.panel').find('.panel-info').html('<span class="text-success"><strong>Everything\'s Good!</strong></span>');
            } else {
                $(this).closest('.panel').find('.panel-info').html('<span class="text-danger"><strong>' + error_count + ' Issue' + (error_count > 1 ? 's' : '') + ' Unsolved!</strong></span>');
            }
        });
    });
})