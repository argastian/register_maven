$('.btn-delete').click(function () {
    var id_faq = $(this).data('id_faq');
    var csrftoken = $(this).data('token');
    if (confirm('Anda yakin ingin menghapus data ini?')) {
        $.ajax({
           url: site_url + 'administrator/faq/delete',
           type: 'post',
           data: {id_faq : id_faq, register_maven_token: csrftoken},
           success: function(result) {
               if(result == 'success') {
                   window.location = site_url + 'administrator/faq';
               } else {
                   window.location = site_url + 'administrator/faq';
               }
           },
           error: function() {
               window.location = site_url + 'administrator/faq';
           }
        });
    }
});