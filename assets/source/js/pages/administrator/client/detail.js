$(function () {
    new SimpleBar(document.getElementById('container-detail-client'), {autoHide: false});
    new SimpleBar(document.getElementById('container-download'), {autoHide: false});
    $('#range_progress').change(function () {
        var persentase = $(this).val();
        $('#persentase_progress').text(persentase);
    });
})