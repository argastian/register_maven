$(function () {
    $(".reset form").on("submit", function (e) {
        e.preventDefault();
        $(".alert").remove();
        var tmp_btn = $(".reset button").html();
        $(".reset button").html("<img src='"+base_url+"assets/images/loader.gif'>");
        var param = $(".reset form").serialize();
        $.ajax({
            url: site_url + 'administrator/reset/check_email',
            type: "POST",
            data: param,
            success: function (result) {
                if (result == 'sukses') {
                    window.location = site_url + 'administrator/reset/success';
                } else {
                    window.location = site_url + 'administrator/reset';
                }
            }, 
            error: function() {
                window.location = site_url + 'administrator/reset';
            }
        });
    });

});