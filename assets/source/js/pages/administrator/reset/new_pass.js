$(function () {
    $(".new_pass form").on("submit", function (e) {
        e.preventDefault();
        $(".alert").remove();
        var tmp_btn = $(".new_pass button").html();
        $(".new_pass button").html("<img src='"+base_url+"assets/images/loader.gif'>");
        var param = $(".new_pass form").serialize();
        $.ajax({
            url: site_url + 'administrator/reset/submit',
            type: "POST",
            data: param,
            success: function (result) {
                if (result == 'sukses') {
                    window.location = site_url + 'administrator';
                } else if (result == 'invalid') {
                    window.location = site_url + 'administrator/reset';
                } else {
                    window.location = site_url + 'administrator/reset/token/' + $('.new_pass form input[name="token"]').val();
                }
            }, 
            error: function() {
                window.location = site_url + 'administrator/reset/token/' + $('.new_pass form input[name="token"]').val();
            }
        });
    });

});