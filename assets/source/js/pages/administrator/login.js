$(function () {
    $(".login form").on("submit", function (e) {
        e.preventDefault();
        $(".alert").remove();
        var tmp_btn = $(".login button").html();
        $(".login button").html("<img src='"+base_url+"assets/images/loader.gif'>");
        var param = $(".login form").serialize();
        $.ajax({
            url: site_url + 'administrator/login/authentication',
            type: "POST",
            data: param,
            success: function (result) {
                if (result == 'valid') {
                    window.location = site_url + 'administrator/home';
                } else {
                    window.location = site_url + 'administrator';
                }
            },
            error: function (){
                window.location = site_url + 'administrator';
            }
        });
    });

});