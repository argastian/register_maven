var el_scroll_pemegang_saham = new SimpleBar(document.getElementById('container-pemegang-saham'), {autoHide: false});
var el_scroll_pengurus = new SimpleBar(document.getElementById('container-pengurus'), {autoHide: false});
$(function () {
    $('form').submit(function (e) {
        if ($(this).find('.form-group.has-error').length) {
            return false;
        } else {
            return true;
        }
    });
    $('[data-toggle="popover"]').popover();
    $('#provinsi').select2({
        placeholder: 'Pilih Provinsi',
        theme: 'bootstrap',
        width: '100%',
        autofill: false
    });
    $('#kota_madya').select2({
        placeholder: 'Pilih Kota Madya',
        theme: 'bootstrap',
        width: '100%'
    });
    $('#kecamatan').select2({
        placeholder: 'Pilih Kecamatan',
        theme: 'bootstrap',
        width: '100%'
    });
    $('#kelurahan').select2({
        placeholder: 'Pilih Kelurahan',
        theme: 'bootstrap',
        width: '100%'
    });
    $('#provinsi').change(function () {
        get_option_kota_kab($('#provinsi').val(), 'kota_madya');
    });
    $('#kota_madya').change(function () {
        get_option_kecamatan($('#kota_madya').val(), 'kecamatan');
    });
    $('#kecamatan').change(function () {
        get_option_kelurahan($('#kecamatan').val(), 'kelurahan');
    });

    $('.btn-add-pemegang-saham').click(function () {
        var type = $(this).data('type');
        var jumlah_pemegang_saham = $('.item-pemegang-saham').length;
        var item_pemegang_saham_perusahaan = '<div class="item-pemegang-saham">\n\
                                        <div class="item-label">Pemegang Saham ' + (jumlah_pemegang_saham + 1) + '</div>\n\
                                        <hr>\n\
                                        <div class="pull-right">\n\
                                            <div class="btn btn-default btn-delete-pemegang-saham"><i class="fa fa-trash"></i></div>\n\
                                        </div>\n\
                                        <div class="form-group">\n\
                                            <label for="jenis_pemegang_saham[' + jumlah_pemegang_saham + ']" class="col-md-5">Jenis Pemegang Saham</label>\n\
                                            <div class="col-md-19">\n\
                                                <select class="form-control" id="jenis_pemegang_saham[' + jumlah_pemegang_saham + ']" name="jenis_pemegang_saham[' + jumlah_pemegang_saham + ']" disabled>\n\
                                                    <option value="perusahaan">Perusahaan</option>\n\
                                                </select>\n\
                                            </div>\n\
                                            <input type="hidden" name="jenis_pemegang_saham[' + jumlah_pemegang_saham + ']" value="perusahaan">\n\
                                        </div>\n\
                                        <div class="group-pemegang-saham saham-perusahaan">\n\
                                            <div class="form-group">\n\
                                                <label for="pemegang_saham_perusahaan[' + jumlah_pemegang_saham + ']" class="col-md-5">Nama PT</label>\n\
                                                <div class="col-md-19">\n\
                                                    <div class="form-group">\n\
                                                        <div class="input-group">\n\
                                                            <div class="input-group-addon">\n\
                                                                PT\n\
                                                            </div>\n\
                                                            <input type="text" class="form-control" id="pemegang_saham_perusahaan[' + jumlah_pemegang_saham + ']" name="pemegang_saham[perusahaan][' + jumlah_pemegang_saham + '][nama]" placeholder="Kolaborasi Kapital Indonesia" onKeyPress="return prevent_number(event)">\n\
                                                        </div>\n\
                                                    </div>\n\
                                                </div>\n\
                                            </div>\n\
                                            <div class="form-group">\n\
                                                <label for="no_akta_perusahaan[' + jumlah_pemegang_saham + ']" class="col-md-5">No Akta Pendirian</label>\n\
                                                <div class="col-md-19">\n\
                                                    <input type="text" class="form-control" id="no_akta_perusahaan[' + jumlah_pemegang_saham + ']" name="pemegang_saham[perusahaan][' + jumlah_pemegang_saham + '][no_akta]" placeholder="1234567890">\n\
                                                </div>\n\
                                            </div>\n\
                                            <div class="form-group">\n\
                                                <label for="tanggal_pendirian_perusahaan[' + jumlah_pemegang_saham + ']" class="col-md-5">Tanggal Pendirian</label>\n\
                                                <div class="col-md-19">\n\
                                                    <input type="text" class="form-control datepicker" id="tanggal_pendirian_perusahaan[' + jumlah_pemegang_saham + ']" name="pemegang_saham[perusahaan][' + jumlah_pemegang_saham + '][tanggal_pendirian]" placeholder="2018-01-11" data-date-max-date="now">\n\
                                                </div>\n\
                                            </div>\n\
                                            <div class="form-group">\n\
                                                <label for="sk_menkumham_perusahaan[' + jumlah_pemegang_saham + ']" class="col-md-5">No SK Menkumham</label>\n\
                                                <div class="col-md-19">\n\
                                                    <input type="text" class="form-control" id="sk_menkumham_perusahaan[' + jumlah_pemegang_saham + ']" name="pemegang_saham[perusahaan][' + jumlah_pemegang_saham + '][no_sk]" placeholder="1234567890">\n\
                                                </div>\n\
                                            </div>\n\
                                            <div class="form-group">\n\
                                                <label for="npwp_pemegang_saham_perusahaan[' + jumlah_pemegang_saham + ']" class="col-md-5">No NPWP Perusahaan</label>\n\
                                                <div class="col-md-19">\n\
                                                    <input type="text" class="form-control" id="npwp_pemegang_saham_perusahaan[' + jumlah_pemegang_saham + ']" name="pemegang_saham[perusahaan][' + jumlah_pemegang_saham + '][no_npwp]" placeholder="1234567890" onKeyPress="return good_chars(event, \'0123456789\', this)">\n\
                                                </div>\n\
                                            </div>\n\
                                            <div class="form-group">\n\
                                                <label for="persentase_pemegang_saham_perusahaan[' + jumlah_pemegang_saham + ']" class="col-md-5">Jumlah Persentase Saham</label>\n\
                                                <div class="col-md-19">\n\
                                                    <div class="form-group">\n\
                                                        <div class="input-group">\n\
                                                            <input type="text" class="form-control" id="persentase_pemegang_saham_perusahaan[' + jumlah_pemegang_saham + ']" name="pemegang_saham[perusahaan][' + jumlah_pemegang_saham + '][persentase_saham]" placeholder="30" onKeyPress="return good_chars(event, \'.0123456789\', this)">\n\
                                                            <div class="input-group-addon">\n\
                                                                %\n\
                                                            </div>\n\
                                                        </div>\n\
                                                    </div>\n\
                                                </div>\n\
                                            </div>\n\
                                        </div>\n\
                                    </div>';
        var item_pemegang_saham_perseorangan = '<div class="item-pemegang-saham">\n\
                                        <div class="item-label">Pemegang Saham ' + (jumlah_pemegang_saham + 1) + '</div>\n\
                                        <hr>\n\
                                        <div class="pull-right">\n\
                                            <div class="btn btn-default btn-delete-pemegang-saham"><i class="fa fa-trash"></i></div>\n\
                                        </div>\n\
                                        <div class="form-group">\n\
                                            <label for="jenis_pemegang_saham[' + jumlah_pemegang_saham + ']" class="col-md-5">Jenis Pemegang Saham</label>\n\
                                            <div class="col-md-19">\n\
                                                <select class="form-control" id="jenis_pemegang_saham[' + jumlah_pemegang_saham + ']" disabled>\n\
                                                    <option value="perseorangan">Perseorangan</option>\n\
                                                </select>\n\
                                            </div>\n\
                                            <input type="hidden" name="jenis_pemegang_saham[' + jumlah_pemegang_saham + ']" value="perseorangan">\n\
                                        </div>\n\
                                        <div class="group-pemegang-saham saham-perseorangan">\n\
                                            <div class="form-group">\n\
                                                <label for="pemegang_saham_perseorangan[' + jumlah_pemegang_saham + ']" class="col-md-5">Nama Lengkap</label>\n\
                                                <div class="col-md-19">\n\
                                                    <input type="text" class="form-control" id="pemegang_saham_perseorangan[' + jumlah_pemegang_saham + ']" name="pemegang_saham[perseorangan][' + jumlah_pemegang_saham + '][nama]" placeholder="Sutansyah Marahakim">\n\
                                                </div>\n\
                                            </div>\n\
                                            <div class="form-group">\n\
                                                <label for="ktp_pemegang_saham_perseorangan[' + jumlah_pemegang_saham + ']" class="col-md-5">No KTP</label>\n\
                                                <div class="col-md-19">\n\
                                                    <input type="text" class="form-control" id="ktp_pemegang_saham_perseorangan[' + jumlah_pemegang_saham + ']" name="pemegang_saham[perseorangan][' + jumlah_pemegang_saham + '][no_ktp]" placeholder="1234567890" onKeyPress="return good_chars(event, \'0123456789\', this)">\n\
                                                </div>\n\
                                            </div>\n\
                                            <div class="form-group">\n\
                                                <label for="alamat_pemegang_saham_perseorangan[' + jumlah_pemegang_saham + ']" class="col-md-5">Alamat Sesuai KTP</label>\n\
                                                <div class="col-md-19">\n\
                                                    <input type="text" class="form-control" id="alamat_pemegang_saham_perseorangan[' + jumlah_pemegang_saham + ']" name="pemegang_saham[perseorangan][' + jumlah_pemegang_saham + '][alamat]" placeholder="Jl. ABC No. 123">\n\
                                                </div>\n\
                                            </div>\n\
                                            <div class="form-group">\n\
                                                <label for="npwp_pemegang_saham_perseorangan[' + jumlah_pemegang_saham + ']" class="col-md-5">No NPWP</label>\n\
                                                <div class="col-md-19">\n\
                                                    <input type="text" class="form-control" id="npwp_pemegang_saham_perseorangan[' + jumlah_pemegang_saham + ']" name="pemegang_saham[perseorangan][' + jumlah_pemegang_saham + '][no_npwp]" placeholder="1234567890" onKeyPress="return good_chars(event, \'0123456789\', this)">\n\
                                                </div>\n\
                                            </div>\n\
                                            <div class="form-group">\n\
                                                <label for="persentase_pemegang_saham_perseorangan[' + jumlah_pemegang_saham + ']" class="col-md-5">Jumlah Persentase Saham</label>\n\
                                                <div class="col-md-19">\n\
                                                    <div class="form-group">\n\
                                                        <div class="input-group">\n\
                                                            <input type="text" class="form-control" id="persentase_pemegang_saham_perseorangan[' + jumlah_pemegang_saham + ']" name="pemegang_saham[perseorangan][' + jumlah_pemegang_saham + '][persentase_saham]" placeholder="30" onKeyPress="return good_chars(event, \'.0123456789\', this)">\n\
                                                            <div class="input-group-addon">\n\
                                                                %\n\
                                                            </div>\n\
                                                        </div>\n\
                                                    </div>\n\
                                                </div>\n\
                                            </div>\n\
                                        </div>\n\
                                    </div>';
        $('#container-pemegang-saham .simplebar-content').append((type == 'perseorangan') ? item_pemegang_saham_perseorangan : item_pemegang_saham_perusahaan);
        el_scroll_pemegang_saham.recalculate();
        if (type == 'perseorangan') {
            $(document).on('focusout', '.form-4 #persentase_pemegang_saham_perseorangan\\[' + jumlah_pemegang_saham + '\\]', function () {
                check_total_saham();
            });
        } else {
            $(document).on('focusout', '.form-4 #persentase_pemegang_saham_perusahaan\\[' + jumlah_pemegang_saham + '\\]', function () {
                check_total_saham();
            });
        }
        $('.form-4').find('.datepicker').datetimepicker({
            format: 'YYYY-MM-DD'
        }).on('dp.change', function (e) {
            check_input_val($(this).attr('id'));
        });
    });

    $('.btn-add-pengurus-perusahaan').click(function () {
        var type = $(this).data('type');
        var option_jabatan;
        var placeholder_jabatan;
        if (type === 'direksi') {
            option_jabatan = '<option value="direksi">Dewan Direksi</option>';
            placeholder_jabatan = 'Direktur';
        } else {
            option_jabatan = '<option value="komisaris">Dewan Komisaris</option>';
            placeholder_jabatan = 'Komisaris';
        }
        var jumlah_pengurus = $('.item-pengurus').length;
        var item_pengurus = ' <div class="item-pengurus">\n\
                                <div class="item-label">Pengurus Perusahaan ' + (jumlah_pengurus + 1) + '</div>\n\
                                <hr>\n\
                                <div class="pull-right">\n\
                                    <div class="btn btn-default btn-delete-pengurus"><i class="fa fa-trash"></i></div>\n\
                                </div>\n\
                                <div class="form-group">\n\
                                    <label for="jenis_jabatan[' + jumlah_pengurus + ']" class="col-md-5">Jenis Jabatan</label>\n\
                                    <div class="col-md-19">\n\
                                        <select class="form-control" id="jenis_jabatan[' + jumlah_pengurus + ']" disabled>\n\
                                            ' + option_jabatan + '\n\
                                        </select>\n\
                                    </div>\n\
                                    <input type="hidden" name="pengurus[' + jumlah_pengurus + '][jenis_jabatan]" value="' + type + '">\n\
                                </div>\n\
                                <div class="form-group">\n\
                                    <label for="jabatan[' + jumlah_pengurus + ']" class="col-md-5">Nama Jabatan</label>\n\
                                    <div class="col-md-19">\n\
                                        <input type="text" class="form-control" id="jabatan[' + jumlah_pengurus + ']" name="pengurus[' + jumlah_pengurus + '][nama_jabatan]" placeholder="' + placeholder_jabatan + '">\n\
                                    </div>\n\
                                </div>\n\
                                <div class="form-group">\n\
                                    <label for="pengurus[' + jumlah_pengurus + ']" class="col-md-5">Nama Pengurus</label>\n\
                                    <div class="col-md-19">\n\
                                        <input type="text" class="form-control" id="pengurus[' + jumlah_pengurus + ']" name="pengurus[' + jumlah_pengurus + '][nama_pengurus]" placeholder="Sutansyah Marahakim">\n\
                                    </div>\n\
                                </div>\n\
                                <div class="form-group">\n\
                                    <label for="ktp_pengurus[' + jumlah_pengurus + ']" class="col-md-5">No KTP</label>\n\
                                    <div class="col-md-19">\n\
                                        <input type="text" class="form-control" id="ktp_pengurus[' + jumlah_pengurus + ']" name="pengurus[' + jumlah_pengurus + '][no_ktp]" placeholder="1234567890" onKeyPress="return good_chars(event, \'0123456789\', this)">\n\
                                    </div>\n\
                                </div>\n\
                                <div class="form-group">\n\
                                    <label for="npwp_pengurus[' + jumlah_pengurus + ']" class="col-md-5">No NPWP</label>\n\
                                    <div class="col-md-19">\n\
                                        <input type="text" class="form-control" id="npwp_pengurus[' + jumlah_pengurus + ']" name="pengurus[' + jumlah_pengurus + '][no_npwp]" placeholder="1234567890" onKeyPress="return good_chars(event, \'0123456789\', this)">\n\
                                    </div>\n\
                                </div>\n\
                                <div class="form-group">\n\
                                    <label for="alamat_pengurus[' + jumlah_pengurus + ']" class="col-md-5">Alamat Sesuai KTP</label>\n\
                                    <div class="col-md-19">\n\
                                        <input type="text" class="form-control" id="alamat_pengurus[' + jumlah_pengurus + ']" name="pengurus[' + jumlah_pengurus + '][alamat]" placeholder="Jl. ABC No. 123">\n\
                                    </div>\n\
                                </div>\n\
                            </div>';
        $('#container-pengurus .simplebar-content').append(item_pengurus);
        el_scroll_pengurus.recalculate();
        $('#pengurus\\[' + jumlah_pengurus + '\\]').autocomplete({
            lookup: function (query, done) {
                var data_pemegang_saham = get_data_pemegang_saham_perseorangan(query);
                done(data_pemegang_saham);
            },
            onSelect: function (suggestion) {
                if (select_autocomplete) {
                    $(this).parents('.item-pengurus').find('input[id^=ktp_pengurus]').val(suggestion.data.ktp);
                    $(this).parents('.item-pengurus').find('input[id^=npwp_pengurus]').val(suggestion.data.npwp);
                    $(this).parents('.item-pengurus').find('input[id^=alamat_pengurus]').val(suggestion.data.alamat);
                    $('.form-5 input[type!="hidden"]').each(function () {
                        check_input_val($(this).attr('id'));
                    });
                    select_autocomplete = false;
                }
            },
            onHide: function (container) {
                select_autocomplete = true;
            }
        });
    });
    $('.form-5 input[type="text"]').each(function () {
        if ($(this).attr('id').startsWith('pengurus')) {
            $(this).autocomplete({
                lookup: function (query, done) {
                    var data_pemegang_saham = get_data_pemegang_saham_perseorangan(query);
                    done(data_pemegang_saham);
                },
                onSelect: function (suggestion) {
                    if (select_autocomplete) {
                        $(this).parents('.item-pengurus').find('input[id^=ktp_pengurus]').val(suggestion.data.ktp);
                        $(this).parents('.item-pengurus').find('input[id^=npwp_pengurus]').val(suggestion.data.npwp);
                        $(this).parents('.item-pengurus').find('input[id^=alamat_pengurus]').val(suggestion.data.alamat);
                        $('.form-5 input[type!="hidden"]').each(function () {
                            check_input_val($(this).attr('id'));
                        });
                        select_autocomplete = false;
                    }
                },
                onHide: function (container) {
                    select_autocomplete = true;
                }
            });
        }
    });
    $('.form-4 input[type="text"]').each(function () {
        if ($(this).attr('id').startsWith('persentase_pemegang_saham_')) {
            $(this).focusout(function () {
                check_total_saham();
            });
        }
    });
    $('.form-4').find('.datepicker').datetimepicker({
        format: 'YYYY-MM-DD',
        widgetPositioning: {
            horizontal: 'left',
            vertical: 'top'
        }
    }).on('dp.change', function (e) {
        check_input_val($(this).attr('id'));
    });
    $(document).on('change', '.form-1 input', function () {
        var input_id = $(this).attr('id');
        input_id = input_id.replace('[', '\\[').replace(']', '\\]');
        var this_input = $('#' + input_id);
        var this_parent = this_input.closest('.form-group');
        this_parent.removeClass('valid-language');
        this_parent.removeClass('check-language');
    }).on('focusout', '.form-1 input', function () {
        var input_id = $(this).attr('id');
        $('.form-1 input[type!="hidden"]').each(function () {
            var input_id = $(this).attr('id');
            var this_parent = $('#' + input_id).closest('.form-group');
            var array_input_id = [];
            switch ($(this).attr('id')) {
                case 'nama_perusahaan_1':
                    array_input_id = ["nama_perusahaan_2", "nama_perusahaan_3"];
                    break;
                case 'nama_perusahaan_2':
                    array_input_id = ["nama_perusahaan_1", "nama_perusahaan_3"];
                    break;
                case 'nama_perusahaan_3':
                    array_input_id = ["nama_perusahaan_1", "nama_perusahaan_2"];
                    break;
            }
            if (this_parent.hasClass('check-language') != true) {
                if (check_unique_val(input_id, array_input_id)) {
                    if ($(this).attr('id') == input_id) {
                        validate_language_not_english(input_id);
                    }
                }
            }
        });
    }).on('keyup', '.form-1 textarea#tujuan_pendirian', function () {
        check_input_val($(this).attr('id'));
    }).on('keyup', '.form-2 input', function () {
        check_input_val($(this).attr('id'));
    }).on('change', '.form-2 input', function () {
        check_input_val($(this).attr('id'));
    }).on('change', '.form-2 select', function () {
        check_input_val($(this).attr('id'));
    }).on('keyup', '.form-3 input#modal_dasar', function () {
        check_modal_dasar($(this).attr('id'));
    }).on('keyup', '.form-3 input#modal_disetor', function () {
        check_modal_disetor($(this).attr('id'));
    }).on('change', '.form-3 input#modal_dasar', function () {
        check_modal_dasar($(this).attr('id'));
    }).on('change', '.form-3 input#modal_disetor', function () {
        check_modal_disetor($(this).attr('id'));
    }).on('keyup', '.form-3 input#nominal_saham', function () {
        check_input_val($(this).attr('id'));
    }).on('change', '.form-4 select', function () {
        var jenis_pemegang_saham = $(this).val();
        $(this).closest('.item-pemegang-saham').find('.group-pemegang-saham').addClass('hide');
        $(this).closest('.item-pemegang-saham').find('.saham-' + jenis_pemegang_saham).removeClass('hide');
        $(this).closest('.item-pemegang-saham').find('input[type!="hidden"]').each(function () {
            $(this).val('');
            clear_form_status($(this).attr('id'));
        });
    }).on('keyup', '.form-4 input', function () {
        if (!$(this).attr('id').startsWith('persentase_pemegang_saham_')) {
            check_input_val($(this).attr('id'));
        }
    }).on('change', '.form-4 input', function () {
        if (!$(this).attr('id').startsWith('persentase_pemegang_saham_')) {
            check_input_val($(this).attr('id'));
        }
    }).on('focusout', '.form-4 input', function () {
        var focus_input_id = $(this).attr('id');

        var array_input_pemegang_saham_perusahaan = [];
        var array_input_no_akta_perusahaan = [];
        var array_input_sk_menkumham_perusahaan = [];
        var array_input_npwp_pemegang_saham_perusahaan = [];
        if (focus_input_id.startsWith('pemegang_saham_perusahaan') || focus_input_id.startsWith('no_akta_perusahaan') || focus_input_id.startsWith('sk_menkumham_perusahaan') || focus_input_id.startsWith('npwp_pemegang_saham_perusahaan')) {
            $('.form-4 input[type!="hidden"]').each(function () {
                if ($(this).attr('id').startsWith('pemegang_saham_perusahaan')) {
                    array_input_pemegang_saham_perusahaan.push($(this).attr('id'));
                }
                if ($(this).attr('id').startsWith('no_akta_perusahaan')) {
                    array_input_no_akta_perusahaan.push($(this).attr('id'));
                }
                if ($(this).attr('id').startsWith('sk_menkumham_perusahaan')) {
                    array_input_sk_menkumham_perusahaan.push($(this).attr('id'));
                }
                if ($(this).attr('id').startsWith('npwp_pemegang_saham_perusahaan')) {
                    array_input_npwp_pemegang_saham_perusahaan.push($(this).attr('id'));
                }
            });
            $('.form-4 input[type!="hidden"]').each(function () {
                if ($(this).attr('id').startsWith('pemegang_saham_perusahaan')) {
                    check_unique_val($(this).attr('id'), array_input_pemegang_saham_perusahaan);
                }
                if ($(this).attr('id').startsWith('no_akta_perusahaan')) {
                    check_unique_val($(this).attr('id'), array_input_no_akta_perusahaan);
                }
                if ($(this).attr('id').startsWith('sk_menkumham_perusahaan')) {
                    check_unique_val($(this).attr('id'), array_input_sk_menkumham_perusahaan);
                }
                if ($(this).attr('id').startsWith('npwp_pemegang_saham_perusahaan')) {
                    check_unique_val($(this).attr('id'), array_input_npwp_pemegang_saham_perusahaan);
                }
            });
        }

        var array_input_pemegang_saham_perseorangan = [];
        var array_input_ktp_pemegang_saham_perseorangan = [];
        var array_input_npwp_pemegang_saham_perseorangan = [];
        if (focus_input_id.startsWith('pemegang_saham_perseorangan') || focus_input_id.startsWith('ktp_pemegang_saham_perseorangan') || focus_input_id.startsWith('npwp_pemegang_saham_perseorangan')) {
            $('.form-4 input[type!="hidden"]').each(function () {
                if ($(this).attr('id').startsWith('pemegang_saham_perseorangan')) {
                    array_input_pemegang_saham_perseorangan.push($(this).attr('id'));
                }
                if ($(this).attr('id').startsWith('ktp_pemegang_saham_perseorangan')) {
                    array_input_ktp_pemegang_saham_perseorangan.push($(this).attr('id'));
                }
                if ($(this).attr('id').startsWith('npwp_pemegang_saham_perseorangan')) {
                    array_input_npwp_pemegang_saham_perseorangan.push($(this).attr('id'));
                }
            });
            $('.form-4 input[type!="hidden"]').each(function () {
                if ($(this).attr('id').startsWith('pemegang_saham_perseorangan')) {
                    check_unique_val($(this).attr('id'), array_input_pemegang_saham_perseorangan);
                }
                if ($(this).attr('id').startsWith('ktp_pemegang_saham_perseorangan')) {
                    check_unique_val($(this).attr('id'), array_input_ktp_pemegang_saham_perseorangan);
                }
                if ($(this).attr('id').startsWith('npwp_pemegang_saham_perseorangan')) {
                    check_unique_val($(this).attr('id'), array_input_npwp_pemegang_saham_perseorangan);
                }
            });
        }
    }).on('change', '.form-5 select:nth-child(1)', function () {
        var jenis_jabatan = $(this).val();
        if (jenis_jabatan === 'direksi') {
            $(this).parents('.item-pengurus').find('input[id^=jabatan]').attr("placeholder", "Direktur");
        } else {
            $(this).parents('.item-pengurus').find('input[id^=jabatan]').attr("placeholder", capitalizeFirstLetter(jenis_jabatan));
        }
        $(this).parents('.item-pengurus').find('input[type!="hidden"]').each(function () {
            $(this).val('');
            clear_form_status($(this).attr('id'));
        });
    }).on('change', '.form-5 select:nth-child(2)', function () {
        var jenis_jabatan = $(this).val();
        if (jenis_jabatan === 'direksi') {
            $(this).parents('.item-pengurus').find('input[id^=jabatan]').attr("placeholder", "Direktur");
        } else {
            $(this).parents('.item-pengurus').find('input[id^=jabatan]').attr("placeholder", capitalizeFirstLetter(jenis_jabatan));
        }
        $(this).parents('.item-pengurus').find('input[type!="hidden"]').each(function () {
            $(this).val('');
            clear_form_status($(this).attr('id'));
        });
    }).on('keyup', '.form-5 input', function () {
        check_input_val($(this).attr('id'));
    }).on('change', '.form-5 input', function () {
        check_input_val($(this).attr('id'));
    }).on('keyup', '.form-6 textarea#bidang_usaha', function () {
        check_input_val($(this).attr('id'));
    }).on('click', '.btn-delete-pemegang-saham', function () {
        if (confirm('Anda yakin ingin menghapus pemegang saham ini?')) {
            $(this).closest('.item-pemegang-saham').remove();
            el_scroll_pemegang_saham.recalculate();
            var count_item = 1;
            $.each($('.item-pemegang-saham .item-label'), function () {
                $(this).text('Pemegang Saham ' + count_item);
                count_item++;
            });
        }
    }).on('click', '.btn-delete-pengurus', function () {
        if (confirm('Anda yakin ingin menghapus pengurus ini?')) {
            $(this).closest('.item-pengurus').remove();
            el_scroll_pengurus.recalculate();
            var count_item = 1;
            $.each($('.item-pengurus .item-label'), function () {
                $(this).text('Pengurus Perusahaan ' + count_item);
                count_item++;
            });
        }
    });
});

function get_data_pemegang_saham_perseorangan(keyword) {
    var data_pemegang_saham = {
        suggestions: []
    };
    $('.form-4 .item-pemegang-saham').each(function () {
        if ($('select[id^="jenis_pemegang_saham"]', this).val() == 'perseorangan' && $('input[id^="pemegang_saham_perseorangan"]', this).val()) {
            if ($('input[id^="pemegang_saham_perseorangan"]', this).val().toLowerCase().indexOf(keyword.toLowerCase()) !== -1) {
                var temp_data_pemegang_saham = {};
                temp_data_pemegang_saham.value = $('input[id^="pemegang_saham_perseorangan"]', this).val();
                temp_data_pemegang_saham.data = {
                    nama: $('input[id^="pemegang_saham_perseorangan"]', this).val(),
                    ktp: $('input[id^="ktp_pemegang_saham_perseorangan"]', this).val(),
                    npwp: $('input[id^="npwp_pemegang_saham_perseorangan"]', this).val(),
                    alamat: $('input[id^="alamat_pemegang_saham_perseorangan"]', this).val()
                };
                data_pemegang_saham.suggestions.push(temp_data_pemegang_saham);
            }
        }
    });

    return data_pemegang_saham;
}