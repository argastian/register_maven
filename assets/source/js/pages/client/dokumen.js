new SimpleBar(document.getElementById('container-pemegang-saham'), {autoHide: false});
$(function () {
    $('form').submit(function (e) {
        if ($(this).find('.form-group.has-error').length) {
            return false;
        } else {
            return true;
        }
    });
});
$('.input-file-container').on('change', 'input[type="file"]', function () {
    clear_form_status($(this).attr('id'));
    if ($(this).val()) {
        var allowedFiles = [".jpg", ".png", ".jpeg", ".gif", ".pdf"];
        var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + allowedFiles.join('|') + ")$");
        if (!regex.test($(this).val().toLowerCase())) {
            form_error($(this).attr('id'), 'Ekstensi yang diizinkan .jpg, .png, .jpeg, .gif dan .pdf');
            $(this).val('');
        } else {
            var size = parseFloat($(this)[0].files[0].size / 1024).toFixed(2);
            if (size <= 2048) {
                form_success($(this).attr('id'));
                $(this).closest('.input-file-container').find('input[type="text"]').val($(this).val().replace(/C:\\fakepath\\/i, ''));
            } else {
                form_error($(this).attr('id'), 'Maksimal 2Mb');
                $(this).val('');
            }
        }
    }
}).on('click', 'input[type="text"]', function () {
    $(this).closest('.input-file-container').find('.btn-bs-file').click();
})