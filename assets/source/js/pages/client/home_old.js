var el_scroll_pemegang_saham = new SimpleBar(document.getElementById('container-pemegang-saham'), {autoHide: false});
var el_scroll_pengurus = new SimpleBar(document.getElementById('container-pengurus'), {autoHide: false});
var select_autocomplete = false;
$(function () {
    $('#form-1').removeClass('hide');
    $('#form-maven').submit(function (e) {
        e.preventDefault();
    });
    $('.btn-next').click(function (e) {
        e.preventDefault();
        var form_1 = $('#form-1');
        var form_2 = $('#form-2');
        var form_3 = $('#form-3');
        var form_4 = $('#form-4');
        var form_5 = $('#form-5');
        var resume_form = $('#resume-form');
        var form_finish = $('#form-finish');
        if (!form_1.hasClass('hide')) {
            if (!form_1.find('.form-group.has-error').length) {
                $('.btn-info-status').removeClass('hide');
                setTimeout(function () {
                    form_1.addClass('hide')
                    form_2.removeClass('hide');
                    $('.btn-prev').removeClass('hide');
                    $('.btn-info-status').addClass('hide');
                    $('.header-text p').text('Langkah 2 dari 5');
                }, 300);
            }
        } else if (!form_2.hasClass('hide')) {
            $('.btn-info-status').removeClass('hide');
            setTimeout(function () {
                form_2.addClass('hide')
                form_3.removeClass('hide');
                $('.btn-info-status').addClass('hide');
                $('.header-text p').text('Langkah 3 dari 5');
            }, 300);
        } else if (!form_3.hasClass('hide')) {
            if (!form_3.find('.form-group.has-error').length) {
                $('.btn-info-status').removeClass('hide');
                setTimeout(function () {
                    form_3.addClass('hide')
                    form_4.removeClass('hide');
                    $('.btn-info-status').addClass('hide');
                    $('.header-text p').text('Langkah 4 dari 5');
                }, 300);
            }
        } else if (!form_4.hasClass('hide')) {
            if (!form_4.find('.form-group.has-error').length) {
                $('.btn-info-status').removeClass('hide');
                setTimeout(function () {
                    form_4.addClass('hide')
                    form_5.removeClass('hide');
                    $('.btn-info-status').addClass('hide');
                    $('.header-text p').text('Langkah 5 dari 5');
                }, 300);
            }
        } else if (!form_5.hasClass('hide')) {
            $(this).text('SETUJU DAN SIMPAN');
            $('.embed-responsive').addClass('hide');
            generate_table_resume();
            $('.btn-info-status').removeClass('hide');
            setTimeout(function () {
                form_5.addClass('hide')
                resume_form.removeClass('hide');
                $('.btn-info-status').addClass('hide');
                $('.header-text p').text('Ringkasan Data Perusahaanmu');
            }, 300);
        } else {
            $.ajax({
                url: $('#form-maven').attr('action'),
                type: 'POST',
                data: $('#form-maven').serialize(),
                dataType: 'json',
                beforeSend: function () {
                    $('.btn-next').button('loading');
                },
                success: function (response) {
                    $('#form-maven input[name="register_maven_token"]').val(response.csrf);
                    if (response.status == 'sukses') {
                        $('#form-maven').closest('.col-md-24').prepend(build_success_message('Data perusahaan berhasil diperbaharui'));
                        hide_alert();
                    } else {
                        alert('Terjadi kesalahan. Silahkan ulangi kembali.');
                    }
                    $('.btn-next').button('reset');
                },
                error: function () {
                    alert('Terjadi kesalahan. Silahkan ulangi kembali.');
                    window.location = site_url + 'client/home';
                }
            });
        }
    });

    $('.btn-prev').click(function () {
        var form_1 = $('#form-1');
        var form_2 = $('#form-2');
        var form_3 = $('#form-3');
        var form_4 = $('#form-4');
        var form_5 = $('#form-5');
        var resume_form = $('#resume-form');

        if (!resume_form.hasClass('hide')) {
            resume_form.addClass('hide');
            form_5.removeClass('hide');
            $('.embed-responsive').removeClass('hide');
            $('.btn-next').text('BERIKUTNYA');
        } else if (!form_5.hasClass('hide')) {
            form_5.addClass('hide');
            form_4.removeClass('hide');
        } else if (!form_4.hasClass('hide')) {
            form_4.addClass('hide');
            form_3.removeClass('hide');
        } else if (!form_3.hasClass('hide')) {
            form_3.addClass('hide');
            form_2.removeClass('hide');
        } else if (!form_2.hasClass('hide')) {
            form_2.addClass('hide');
            form_1.removeClass('hide');
            $('.btn-prev').addClass('hide');
        }
    });

    $('#pengurus\\[0\\]').autocomplete({
        lookup: function (query, done) {
            var data_pemegang_saham = get_data_pemegang_saham_perseorangan(query);
            done(data_pemegang_saham);
        },
        onSelect: function (suggestion) {
            if (select_autocomplete) {
                $(this).parents('.item-pengurus').find('input[id^=ktp_pengurus]').val(suggestion.data.ktp);
                $(this).parents('.item-pengurus').find('input[id^=npwp_pengurus]').val(suggestion.data.npwp);
                $(this).parents('.item-pengurus').find('input[id^=alamat_pengurus]').val(suggestion.data.alamat);
                $('#form-maven #form-5 input').each(function () {
                    check_input_val($(this).attr('id'));
                });
                select_autocomplete = false;
            }
        },
        onHide: function (container) {
            select_autocomplete = true;
        }
    });

    $('#pengurus\\[1\\]').autocomplete({
        lookup: function (query, done) {
            var data_pemegang_saham = get_data_pemegang_saham_perseorangan(query);
            done(data_pemegang_saham);
        },
        onSelect: function (suggestion) {
            if (select_autocomplete) {
                $(this).parents('.item-pengurus').find('input[id^=ktp_pengurus]').val(suggestion.data.ktp);
                $(this).parents('.item-pengurus').find('input[id^=npwp_pengurus]').val(suggestion.data.npwp);
                $(this).parents('.item-pengurus').find('input[id^=alamat_pengurus]').val(suggestion.data.alamat);
                $('#form-maven #form-5 input').each(function () {
                    check_input_val($(this).attr('id'));
                });
                select_autocomplete = false;
            }
        },
        onHide: function (container) {
            select_autocomplete = true;
        }
    });

    $('#form-4').find('.datepicker').datetimepicker({
        format: 'YYYY-MM-DD',
        widgetPositioning: {
            horizontal: 'left',
            vertical: 'top'
        }
    }).on('dp.change', function (e) {
        check_input_val($(this).attr('id'));
    });
    $('[data-toggle="popover"]').popover();
    var options_pswd = {};
    options_pswd.common = {
        onScore: function (options, word, totalScoreCalculated) {
            if (totalScoreCalculated <= 0) {
                form_custom('password', 'error', 'Kekuatan password terlalu lemah');
            } else if (totalScoreCalculated < 14) {
                form_custom('password', 'error', 'Kekuatan password lemah');
            } else if (totalScoreCalculated < 26) {
                form_custom('password', 'warning', 'Kekuatan password cukup');
            } else if (totalScoreCalculated < 38) {
                form_custom('password', 'warning', 'Kekuatan password sedang');
            } else if (totalScoreCalculated < 50) {
                form_custom('password', 'success', 'Kekuatan password kuat');
            } else {
                form_custom('password', 'success', 'Kekuatan password sangat kuat');
            }
        }
    };
    options_pswd.ui = {
        showProgressBar: false
    }
    $('#password').pwstrength(options_pswd);
    $('#tab-location a').click(function (e) {
        e.preventDefault();

        var tab_vo_isi = false;
        $('#virtual-office input').each(function () {
            if ($(this).val() != "") {
                tab_vo_isi = true;
            }
        });
        $('#virtual-office select').each(function () {
            if ($(this).val() != "" && $(this).val() != "Pilih Provinsi" && $(this).val() != "Pilih Kota Madya" && $(this).val() != "Pilih Kecamatan" && $(this).val() != "Pilih Kelurahan") {
                tab_vo_isi = true;
            }
        });

        var tab_sewa_isi = false;
        $('#sewa input').each(function () {
            if ($(this).val() != "") {
                tab_sewa_isi = true;
            }
        });
        $('#sewa select').each(function () {
            if ($(this).val() != "" && $(this).val() != "Pilih Provinsi" && $(this).val() != "Pilih Kota Madya" && $(this).val() != "Pilih Kecamatan" && $(this).val() != "Pilih Kelurahan") {
                tab_sewa_isi = true;
            }
        });

        var tab_sendiri_isi = false;
        $('#milik-sendiri input').each(function () {
            if ($(this).val() != "") {
                tab_sendiri_isi = true;
            }
        });
        $('#milik-sendiri select').each(function () {
            if ($(this).val() != "" && $(this).val() != "Pilih Provinsi" && $(this).val() != "Pilih Kota Madya" && $(this).val() != "Pilih Kecamatan" && $(this).val() != "Pilih Kelurahan") {
                tab_sendiri_isi = true;
            }
        });

        if ($(this).attr('href') !== $('#tab-location li.active a').attr('href')) {
            if (!tab_vo_isi && !tab_sewa_isi && !tab_sendiri_isi) {
                $(this).tab('show');
                var tipe_kepemilikan = $(this).attr('href');
                tipe_kepemilikan = tipe_kepemilikan.replace("#", "").replace("-", "_");
                $('input[name="kepemilikan"]').val(tipe_kepemilikan);
            } else {
                if (confirm('Anda hanya diperkenankan mengisi alamat domisili dari salah satu jenis kepemilikan kantor. Jika berpindah jenis kepemilikan kantor maka data sebelunya akan terhapus. Lanjutkan?')) {
                    if (tab_vo_isi) {
                        $('#virtual-office input').each(function () {
                            $(this).val('');
                            clear_form_status($(this).attr('id'));
                        });
                        $("#provinsi_vo")[0].selectedIndex = 0;
                        $('#provinsi_vo').trigger("change");
                    }
                    if (tab_sewa_isi) {
                        $('#sewa input').each(function () {
                            $(this).val('');
                            clear_form_status($(this).attr('id'));
                        });
                        $("#provinsi_sewa")[0].selectedIndex = 0;
                        $('#provinsi_sewa').trigger("change");
                    }
                    if (tab_sendiri_isi) {
                        $('#milik-sendiri input').each(function () {
                            $(this).val('');
                            clear_form_status($(this).attr('id'));
                        });
                        $("#provinsi_sendiri")[0].selectedIndex = 0;
                        $('#provinsi_sendiri').trigger("change");
                    }
                    $(this).tab('show');
                    var tipe_kepemilikan = $(this).attr('href');
                    tipe_kepemilikan = tipe_kepemilikan.replace("#", "");
                    $('input[name="kepemilikan"]').val(tipe_kepemilikan);
                }
            }
        }
    });

    $('#provinsi_vo').select2({
        placeholder: 'Pilih Provinsi',
        theme: 'bootstrap',
        width: '100%',
        autofill: false
    });
    $('#kota_madya_vo').select2({
        placeholder: 'Pilih Kota Madya',
        theme: 'bootstrap',
        width: '100%'
    });
    $('#kecamatan_vo').select2({
        placeholder: 'Pilih Kecamatan',
        theme: 'bootstrap',
        width: '100%'
    });
    $('#kelurahan_vo').select2({
        placeholder: 'Pilih Kelurahan',
        theme: 'bootstrap',
        width: '100%'
    });
    $('#provinsi_vo').change(function () {
        get_option_kota_kab($('#provinsi_vo').val(), 'kota_madya_vo');
    });
    $('#kota_madya_vo').change(function () {
        get_option_kecamatan($('#kota_madya_vo').val(), 'kecamatan_vo');
    });
    $('#kecamatan_vo').change(function () {
        get_option_kelurahan($('#kecamatan_vo').val(), 'kelurahan_vo');
    });

    $('#provinsi_sewa').select2({
        placeholder: 'Pilih Provinsi',
        theme: 'bootstrap',
        width: '100%',
        autofill: false
    });
    $('#kota_madya_sewa').select2({
        placeholder: 'Pilih Kota Madya',
        theme: 'bootstrap',
        width: '100%'
    });
    $('#kecamatan_sewa').select2({
        placeholder: 'Pilih Kecamatan',
        theme: 'bootstrap',
        width: '100%'
    });
    $('#kelurahan_sewa').select2({
        placeholder: 'Pilih Kelurahan',
        theme: 'bootstrap',
        width: '100%'
    });
    $('#provinsi_sewa').change(function () {
        get_option_kota_kab($('#provinsi_sewa').val(), 'kota_madya_sewa');
    });
    $('#kota_madya_sewa').change(function () {
        get_option_kecamatan($('#kota_madya_sewa').val(), 'kecamatan_sewa');
    });
    $('#kecamatan_sewa').change(function () {
        get_option_kelurahan($('#kecamatan_sewa').val(), 'kelurahan_sewa');
    });

    $('#provinsi_sendiri').select2({
        placeholder: 'Pilih Provinsi',
        theme: 'bootstrap',
        width: '100%',
        autofill: false
    });
    $('#kota_madya_sendiri').select2({
        placeholder: 'Pilih Kota Madya',
        theme: 'bootstrap',
        width: '100%'
    });
    $('#kecamatan_sendiri').select2({
        placeholder: 'Pilih Kecamatan',
        theme: 'bootstrap',
        width: '100%'
    });
    $('#kelurahan_sendiri').select2({
        placeholder: 'Pilih Kelurahan',
        theme: 'bootstrap',
        width: '100%'
    });
    $('#provinsi_sendiri').change(function () {
        get_option_kota_kab($('#provinsi_sendiri').val(), 'kota_madya_sendiri');
    });
    $('#kota_madya_sendiri').change(function () {
        get_option_kecamatan($('#kota_madya_sendiri').val(), 'kecamatan_sendiri');
    });
    $('#kecamatan_sendiri').change(function () {
        get_option_kelurahan($('#kecamatan_sendiri').val(), 'kelurahan_sendiri');
    });

    $('.btn-add-pemegang-saham').click(function () {
        var type = $(this).data('type');
        var jumlah_pemegang_saham = $('.item-pemegang-saham').length;
        var item_pemegang_saham_perusahaan = '<div class="item-pemegang-saham">\n\
                                        <div class="item-label">Pemegang Saham ' + (jumlah_pemegang_saham + 1) + '</div>\n\
                                        <hr>\n\
                                        <div class="pull-right">\n\
                                            <div class="btn btn-default btn-delete-pemegang-saham"><i class="fa fa-trash"></i></div>\n\
                                        </div>\n\
                                        <div class="form-group">\n\
                                            <label for="jenis_pemegang_saham[' + jumlah_pemegang_saham + ']">Jenis Pemegang Saham</label>\n\
                                            <select class="form-control" id="jenis_pemegang_saham[' + jumlah_pemegang_saham + ']" name="jenis_pemegang_saham[' + jumlah_pemegang_saham + ']" disabled>\n\
                                                <option value="perusahaan">Perusahaan</option>\n\
                                            </select>\n\
                                            <input type="hidden" name="jenis_pemegang_saham[' + jumlah_pemegang_saham + ']" value="perusahaan">\n\
                                        </div>\n\
                                        <div class="group-pemegang-saham saham-perusahaan">\n\
                                            <div class="row">\n\
                                                <div class="form-group col-sm-12">\n\
                                                    <label for="pemegang_saham_perusahaan[' + jumlah_pemegang_saham + ']">Nama PT</label>\n\
                                                    <div class="form-group">\n\
                                                        <div class="input-group">\n\
                                                            <div class="input-group-addon">\n\
                                                                PT\n\
                                                            </div>\n\
                                                            <input type="text" class="form-control" id="pemegang_saham_perusahaan[' + jumlah_pemegang_saham + ']" name="pemegang_saham[perusahaan][' + jumlah_pemegang_saham + '][nama]" placeholder="Kolaborasi Kapital Indonesia" onKeyPress="return prevent_number(event)">\n\
                                                        </div>\n\
                                                    </div>\n\
                                                </div>\n\
                                                <div class="form-group col-sm-12">\n\
                                                    <label for="no_akta_perusahaan[' + jumlah_pemegang_saham + ']">No Akta Pendirian</label>\n\
                                                    <input type="text" class="form-control" id="no_akta_perusahaan[' + jumlah_pemegang_saham + ']" name="pemegang_saham[perusahaan][' + jumlah_pemegang_saham + '][no_akta]" placeholder="1234567890">\n\
                                                </div>\n\
                                            </div>\n\
                                            <div class="row">\n\
                                                <div class="form-group col-sm-12">\n\
                                                    <label for="tanggal_pendirian_perusahaan[' + jumlah_pemegang_saham + ']">Tanggal Pendirian</label>\n\
                                                    <input type="text" class="form-control datepicker" id="tanggal_pendirian_perusahaan[' + jumlah_pemegang_saham + ']" name="pemegang_saham[perusahaan][' + jumlah_pemegang_saham + '][tanggal_pendirian]" placeholder="2018-01-11" data-date-max-date="now">\n\
                                                </div>\n\
                                                <div class="form-group col-sm-12">\n\
                                                    <label for="sk_menkumham_perusahaan[' + jumlah_pemegang_saham + ']">No SK Menkumham</label>\n\
                                                    <input type="text" class="form-control" id="sk_menkumham_perusahaan[' + jumlah_pemegang_saham + ']" name="pemegang_saham[perusahaan][' + jumlah_pemegang_saham + '][no_sk]" placeholder="1234567890">\n\
                                                </div>\n\
                                            </div>\n\
                                            <div class="row">\n\
                                                <div class="form-group col-sm-12">\n\
                                                    <label for="npwp_pemegang_saham_perusahaan[' + jumlah_pemegang_saham + ']">No NPWP Perusahaan</label>\n\
                                                    <input type="text" class="form-control" id="npwp_pemegang_saham_perusahaan[' + jumlah_pemegang_saham + ']" name="pemegang_saham[perusahaan][' + jumlah_pemegang_saham + '][no_npwp]" placeholder="1234567890" onKeyPress="return good_chars(event, \'0123456789\', this)">\n\
                                                </div>\n\
                                                <div class="form-group col-sm-12">\n\
                                                    <label for="persentase_pemegang_saham_perusahaan[' + jumlah_pemegang_saham + ']">Jumlah Persentase Saham</label>\n\
                                                    <div class="form-group">\n\
                                                        <div class="input-group">\n\
                                                            <input type="text" class="form-control" id="persentase_pemegang_saham_perusahaan[' + jumlah_pemegang_saham + ']" name="pemegang_saham[perusahaan][' + jumlah_pemegang_saham + '][persentase_saham]" placeholder="30" onKeyPress="return good_chars(event, \'.0123456789\', this)">\n\
                                                            <div class="input-group-addon">\n\
                                                                %\n\
                                                            </div>\n\
                                                        </div>\n\
                                                    </div>\n\
                                                </div>\n\
                                            </div>\n\
                                        </div>\n\
                                    </div>';
        var item_pemegang_saham_perseorangan = '<div class="item-pemegang-saham">\n\
                                        <div class="item-label">Pemegang Saham ' + (jumlah_pemegang_saham + 1) + '</div>\n\
                                        <hr>\n\
                                        <div class="pull-right">\n\
                                            <div class="btn btn-default btn-delete-pemegang-saham"><i class="fa fa-trash"></i></div>\n\
                                        </div>\n\
                                        <div class="form-group">\n\
                                            <label for="jenis_pemegang_saham[' + jumlah_pemegang_saham + ']">Jenis Pemegang Saham</label>\n\
                                            <select class="form-control" id="jenis_pemegang_saham[' + jumlah_pemegang_saham + ']" disabled>\n\
                                                <option value="perseorangan">Perseorangan</option>\n\
                                            </select>\n\
\n\                                         <input type="hidden" name="jenis_pemegang_saham[' + jumlah_pemegang_saham + ']" value="perseorangan">\n\
                                        </div>\n\
                                        <div class="group-pemegang-saham saham-perseorangan">\n\
                                            <div class="row">\n\
                                                <div class="form-group col-sm-12">\n\
                                                    <label for="pemegang_saham_perseorangan[' + jumlah_pemegang_saham + ']">Nama Lengkap</label>\n\
                                                    <input type="text" class="form-control" id="pemegang_saham_perseorangan[' + jumlah_pemegang_saham + ']" name="pemegang_saham[perseorangan][' + jumlah_pemegang_saham + '][nama]" placeholder="Sutansyah Marahakim">\n\
                                                </div>\n\
                                                <div class="form-group col-sm-12">\n\
                                                    <label for="ktp_pemegang_saham_perseorangan[' + jumlah_pemegang_saham + ']">No KTP</label>\n\
                                                    <input type="text" class="form-control" id="ktp_pemegang_saham_perseorangan[' + jumlah_pemegang_saham + ']" name="pemegang_saham[perseorangan][' + jumlah_pemegang_saham + '][no_ktp]" placeholder="1234567890" onKeyPress="return good_chars(event, \'0123456789\', this)">\n\
                                                </div>\n\
                                            </div>\n\
                                            <div class="row">\n\
                                                <div class="form-group col-sm-12">\n\
                                                    <label for="alamat_pemegang_saham_perseorangan[' + jumlah_pemegang_saham + ']">Alamat Sesuai KTP</label>\n\
                                                    <input type="text" class="form-control" id="alamat_pemegang_saham_perseorangan[' + jumlah_pemegang_saham + ']" name="pemegang_saham[perseorangan][' + jumlah_pemegang_saham + '][alamat]" placeholder="Jl. ABC No. 123">\n\
                                                </div>\n\
                                                <div class="form-group col-sm-12">\n\
                                                    <label for="npwp_pemegang_saham_perseorangan[' + jumlah_pemegang_saham + ']">No NPWP</label>\n\
                                                    <input type="text" class="form-control" id="npwp_pemegang_saham_perseorangan[' + jumlah_pemegang_saham + ']" name="pemegang_saham[perseorangan][' + jumlah_pemegang_saham + '][no_npwp]" placeholder="1234567890" onKeyPress="return good_chars(event, \'0123456789\', this)">\n\
                                                </div>\n\
                                            </div>\n\
                                            <div class="row">\n\
                                                <div class="form-group col-sm-12">\n\
                                                    <label for="persentase_pemegang_saham_perseorangan[' + jumlah_pemegang_saham + ']">Jumlah Persentase Saham</label>\n\
                                                    <div class="form-group">\n\
                                                        <div class="input-group">\n\
                                                            <input type="text" class="form-control" id="persentase_pemegang_saham_perseorangan[' + jumlah_pemegang_saham + ']" name="pemegang_saham[perseorangan][' + jumlah_pemegang_saham + '][persentase_saham]" placeholder="30" onKeyPress="return good_chars(event, \'.0123456789\', this)">\n\
                                                            <div class="input-group-addon">\n\
                                                                %\n\
                                                            </div>\n\
                                                        </div>\n\
                                                    </div>\n\
                                                </div>\n\
                                            </div>\n\
                                        </div>\n\
                                    </div>';
        $('#container-pemegang-saham .simplebar-content').append((type == 'perseorangan') ? item_pemegang_saham_perseorangan : item_pemegang_saham_perusahaan);
        el_scroll_pemegang_saham.recalculate();
        if (type == 'perseorangan') {
            $(document).on('focusout', '#form-maven #form-4 #persentase_pemegang_saham_perseorangan\\[' + jumlah_pemegang_saham + '\\]', function () {
                check_total_saham();
            });
        } else {
            $(document).on('focusout', '#form-maven #form-4 #persentase_pemegang_saham_perusahaan\\[' + jumlah_pemegang_saham + '\\]', function () {
                check_total_saham();
            });
        }
        $('#form-4').find('.datepicker').datetimepicker({
            format: 'YYYY-MM-DD'
        }).on('dp.change', function (e) {
            check_input_val($(this).attr('id'));
        });
    });

    $('.btn-add-pengurus-perusahaan').click(function () {
        var type = $(this).data('type');
        var option_jabatan;
        var placeholder_jabatan;
        if (type === 'direksi') {
            option_jabatan = '<option value="direksi">Dewan Direksi</option>';
            placeholder_jabatan = 'Direktur';
        } else {
            option_jabatan = '<option value="komisaris">Dewan Komisaris</option>';
            placeholder_jabatan = 'Komisaris';
        }
        var jumlah_pengurus = $('.item-pengurus').length;
        var item_pengurus = ' <div class="item-pengurus">\n\
                                <div class="item-label">Pengurus Perusahaan ' + (jumlah_pengurus + 1) + '</div>\n\
                                <hr>\n\
                                <div class="pull-right">\n\
                                    <div class="btn btn-default btn-delete-pengurus"><i class="fa fa-trash"></i></div>\n\
                                </div>\n\
                                <div class="form-group">\n\
                                    <label for="jenis_jabatan[' + jumlah_pengurus + ']">Jenis Jabatan</label>\n\
                                    <select class="form-control" id="jenis_jabatan[' + jumlah_pengurus + ']" disabled>\n\
                                        ' + option_jabatan + '\n\
                                    </select>\n\
                                    <input type="hidden" name="pengurus[' + jumlah_pengurus + '][jenis_jabatan]" value="' + type + '">\n\
                                </div>\n\
                                <div class="row">\n\
                                    <div class="form-group col-sm-12">\n\
                                        <label for="jabatan[' + jumlah_pengurus + ']">Nama Jabatan</label>\n\
                                        <input type="text" class="form-control" id="jabatan[' + jumlah_pengurus + ']" name="pengurus[' + jumlah_pengurus + '][nama_jabatan]" placeholder="' + placeholder_jabatan + '">\n\
                                    </div>\n\
                                    <div class="form-group col-sm-12">\n\
                                        <label for="pengurus[' + jumlah_pengurus + ']">Nama Pengurus</label>\n\
                                        <input type="text" class="form-control" id="pengurus[' + jumlah_pengurus + ']" name="pengurus[' + jumlah_pengurus + '][nama_pengurus]" placeholder="Sutansyah Marahakim">\n\
                                    </div>\n\
                                </div>\n\
                                <div class="row">\n\
                                    <div class="form-group col-sm-12">\n\
                                        <label for="ktp_pengurus[' + jumlah_pengurus + ']">No KTP</label>\n\
                                        <input type="text" class="form-control" id="ktp_pengurus[' + jumlah_pengurus + ']" name="pengurus[' + jumlah_pengurus + '][no_ktp]" placeholder="1234567890" onKeyPress="return good_chars(event, \'0123456789\', this)">\n\
                                    </div>\n\
                                    <div class="form-group col-sm-12">\n\
                                        <label for="npwp_pengurus[' + jumlah_pengurus + ']">No NPWP</label>\n\
                                        <input type="text" class="form-control" id="npwp_pengurus[' + jumlah_pengurus + ']" name="pengurus[' + jumlah_pengurus + '][no_npwp]" placeholder="1234567890" onKeyPress="return good_chars(event, \'0123456789\', this)">\n\
                                    </div>\n\
                                </div>\n\
                                <div class="row">\n\
                                    <div class="form-group col-sm-12">\n\
                                        <label for="alamat_pengurus[' + jumlah_pengurus + ']">Alamat Sesuai KTP</label>\n\
                                        <input type="text" class="form-control" id="alamat_pengurus[' + jumlah_pengurus + ']" name="pengurus[' + jumlah_pengurus + '][alamat]" placeholder="Jl. ABC No. 123">\n\
                                    </div>\n\
                                </div>';
        $('#container-pengurus .simplebar-content').append(item_pengurus);
        el_scroll_pengurus.recalculate();
        $('#pengurus\\[' + jumlah_pengurus + '\\]').autocomplete({
            lookup: function (query, done) {
                var data_pemegang_saham = get_data_pemegang_saham_perseorangan(query);
                done(data_pemegang_saham);
            },
            onSelect: function (suggestion) {
                if (select_autocomplete) {
                    $(this).parents('.item-pengurus').find('input[id^=ktp_pengurus]').val(suggestion.data.ktp);
                    $(this).parents('.item-pengurus').find('input[id^=npwp_pengurus]').val(suggestion.data.npwp);
                    $(this).parents('.item-pengurus').find('input[id^=alamat_pengurus]').val(suggestion.data.alamat);
                    $('#form-maven #form-5 input').each(function () {
                        check_input_val($(this).attr('id'));
                    });
                    select_autocomplete = false;
                }
            },
            onHide: function (container) {
                select_autocomplete = true;
            }
        });
    });

    $(document).on('change', '#form-maven #form-1 input', function () {
        var input_id = $(this).attr('id');
        input_id = input_id.replace('[', '\\[').replace(']', '\\]');
        var this_input = $('#' + input_id);
        var this_parent = this_input.closest('.form-group');
        this_parent.removeClass('valid-language');
        this_parent.removeClass('check-language');
    }).on('focusout', '#form-maven #form-1 input', function () {
        var input_id = $(this).attr('id');
        $('#form-maven #form-1 input').each(function () {
            var input_id = $(this).attr('id');
            var this_parent = $('#' + input_id).closest('.form-group');
            var array_input_id = [];
            switch ($(this).attr('id')) {
                case 'nama_perusahaan_1':
                    array_input_id = ["nama_perusahaan_2", "nama_perusahaan_3"];
                    break;
                case 'nama_perusahaan_2':
                    array_input_id = ["nama_perusahaan_1", "nama_perusahaan_3"];
                    break;
                case 'nama_perusahaan_3':
                    array_input_id = ["nama_perusahaan_1", "nama_perusahaan_2"];
                    break;
            }
            if (this_parent.hasClass('check-language') != true) {
                if (check_unique_val(input_id, array_input_id)) {
                    if ($(this).attr('id') == input_id) {
                        validate_language_not_english(input_id);
                    }
                }
            }
        });
    }).on('keyup', '#form-maven #form-1 textarea#tujuan_pendirian', function () {
        check_input_val($(this).attr('id'));
    }).on('keyup', '#form-maven #form-2 input', function () {
        check_input_val($(this).attr('id'));
    }).on('change', '#form-maven #form-2 input', function () {
        check_input_val($(this).attr('id'));
    }).on('change', '#form-maven #form-2 select', function () {
        check_input_val($(this).attr('id'));
    }).on('keyup', '#form-maven #form-3 input#modal_dasar', function () {
        check_modal_dasar($(this).attr('id'));
    }).on('keyup', '#form-maven #form-3 input#modal_disetor', function () {
        check_modal_disetor($(this).attr('id'));
    }).on('change', '#form-maven #form-3 input#modal_dasar', function () {
        check_modal_dasar($(this).attr('id'));
    }).on('change', '#form-maven #form-3 input#modal_disetor', function () {
        check_modal_disetor($(this).attr('id'));
    }).on('keyup', '#form-maven #form-3 input#nominal_saham', function () {
        check_input_val($(this).attr('id'));
    }).on('change', '#form-maven #form-4 select', function () {
        var jenis_pemegang_saham = $(this).val();
        $(this).closest('.item-pemegang-saham').find('.group-pemegang-saham').addClass('hide');
        $(this).closest('.item-pemegang-saham').find('.saham-' + jenis_pemegang_saham).removeClass('hide');
        $(this).closest('.item-pemegang-saham').find('input').each(function () {
            $(this).val('');
            clear_form_status($(this).attr('id'));
        });
    }).on('keyup', '#form-maven #form-4 input', function () {
        if (!$(this).attr('id').startsWith('persentase_pemegang_saham_')) {
            check_input_val($(this).attr('id'));
        }
    }).on('change', '#form-maven #form-4 input', function () {
        if (!$(this).attr('id').startsWith('persentase_pemegang_saham_')) {
            check_input_val($(this).attr('id'));
        }
    }).on('focusout', '#form-maven #form-4 #persentase_pemegang_saham_perusahaan\\[0\\]', function () {
        check_total_saham();
    }).on('focusout', '#form-maven #form-4 #persentase_pemegang_saham_perseorangan\\[0\\]', function () {
        check_total_saham();
    }).on('focusout', '#form-maven #form-4 input', function () {
        var focus_input_id = $(this).attr('id');

        var array_input_pemegang_saham_perusahaan = [];
        var array_input_no_akta_perusahaan = [];
        var array_input_sk_menkumham_perusahaan = [];
        var array_input_npwp_pemegang_saham_perusahaan = [];
        if (focus_input_id.startsWith('pemegang_saham_perusahaan') || focus_input_id.startsWith('no_akta_perusahaan') || focus_input_id.startsWith('sk_menkumham_perusahaan') || focus_input_id.startsWith('npwp_pemegang_saham_perusahaan')) {
            $('#form-maven #form-4 input').each(function () {
                if ($(this).attr('id').startsWith('pemegang_saham_perusahaan')) {
                    array_input_pemegang_saham_perusahaan.push($(this).attr('id'));
                }
                if ($(this).attr('id').startsWith('no_akta_perusahaan')) {
                    array_input_no_akta_perusahaan.push($(this).attr('id'));
                }
                if ($(this).attr('id').startsWith('sk_menkumham_perusahaan')) {
                    array_input_sk_menkumham_perusahaan.push($(this).attr('id'));
                }
                if ($(this).attr('id').startsWith('npwp_pemegang_saham_perusahaan')) {
                    array_input_npwp_pemegang_saham_perusahaan.push($(this).attr('id'));
                }
            });
            $('#form-maven #form-4 input').each(function () {
                if ($(this).attr('id').startsWith('pemegang_saham_perusahaan')) {
                    check_unique_val($(this).attr('id'), array_input_pemegang_saham_perusahaan);
                }
                if ($(this).attr('id').startsWith('no_akta_perusahaan')) {
                    check_unique_val($(this).attr('id'), array_input_no_akta_perusahaan);
                }
                if ($(this).attr('id').startsWith('sk_menkumham_perusahaan')) {
                    check_unique_val($(this).attr('id'), array_input_sk_menkumham_perusahaan);
                }
                if ($(this).attr('id').startsWith('npwp_pemegang_saham_perusahaan')) {
                    check_unique_val($(this).attr('id'), array_input_npwp_pemegang_saham_perusahaan);
                }
            });
        }

        var array_input_pemegang_saham_perseorangan = [];
        var array_input_ktp_pemegang_saham_perseorangan = [];
        var array_input_npwp_pemegang_saham_perseorangan = [];
        if (focus_input_id.startsWith('pemegang_saham_perseorangan') || focus_input_id.startsWith('ktp_pemegang_saham_perseorangan') || focus_input_id.startsWith('npwp_pemegang_saham_perseorangan')) {
            $('#form-maven #form-4 input').each(function () {
                if ($(this).attr('id').startsWith('pemegang_saham_perseorangan')) {
                    array_input_pemegang_saham_perseorangan.push($(this).attr('id'));
                }
                if ($(this).attr('id').startsWith('ktp_pemegang_saham_perseorangan')) {
                    array_input_ktp_pemegang_saham_perseorangan.push($(this).attr('id'));
                }
                if ($(this).attr('id').startsWith('npwp_pemegang_saham_perseorangan')) {
                    array_input_npwp_pemegang_saham_perseorangan.push($(this).attr('id'));
                }
            });
            $('#form-maven #form-4 input').each(function () {
                if ($(this).attr('id').startsWith('pemegang_saham_perseorangan')) {
                    check_unique_val($(this).attr('id'), array_input_pemegang_saham_perseorangan);
                }
                if ($(this).attr('id').startsWith('ktp_pemegang_saham_perseorangan')) {
                    check_unique_val($(this).attr('id'), array_input_ktp_pemegang_saham_perseorangan);
                }
                if ($(this).attr('id').startsWith('npwp_pemegang_saham_perseorangan')) {
                    check_unique_val($(this).attr('id'), array_input_npwp_pemegang_saham_perseorangan);
                }
            });
        }
    }).on('change', '#form-maven #form-5 select:nth-child(1)', function () {
        var jenis_jabatan = $(this).val();
        if (jenis_jabatan === 'direksi') {
            $(this).parents('.item-pengurus').find('input[id^=jabatan]').attr("placeholder", "Direktur");
        } else {
            $(this).parents('.item-pengurus').find('input[id^=jabatan]').attr("placeholder", capitalizeFirstLetter(jenis_jabatan));
        }
        $(this).parents('.item-pengurus').find('input').each(function () {
            $(this).val('');
            clear_form_status($(this).attr('id'));
        });
    }).on('change', '#form-maven #form-5 select:nth-child(2)', function () {
        var jenis_jabatan = $(this).val();
        if (jenis_jabatan === 'direksi') {
            $(this).parents('.item-pengurus').find('input[id^=jabatan]').attr("placeholder", "Direktur");
        } else {
            $(this).parents('.item-pengurus').find('input[id^=jabatan]').attr("placeholder", capitalizeFirstLetter(jenis_jabatan));
        }
        $(this).parents('.item-pengurus').find('input').each(function () {
            $(this).val('');
            clear_form_status($(this).attr('id'));
        });
    }).on('keyup', '#form-maven #form-5 input', function () {
        check_input_val($(this).attr('id'));
    }).on('change', '#form-maven #form-5 input', function () {
        check_input_val($(this).attr('id'));
    }).on('click', '.btn-delete-pemegang-saham', function () {
        if (confirm('Anda yakin ingin menghapus pemegang saham ini?')) {
            $(this).closest('.item-pemegang-saham').remove();
            el_scroll_pemegang_saham.recalculate();
            var count_item = 1;
            $.each($('.item-pemegang-saham .item-label'), function () {
                $(this).text('Pemegang Saham ' + count_item);
                count_item++;
            });
        }
    }).on('click', '.btn-delete-pengurus', function () {
        if (confirm('Anda yakin ingin menghapus pengurus ini?')) {
            $(this).closest('.item-pengurus').remove();
            el_scroll_pengurus.recalculate();
            var count_item = 1;
            $.each($('.item-pengurus .item-label'), function () {
                $(this).text('Pengurus Perusahaan ' + count_item);
                count_item++;
            });
        }
    });
});

function get_data_pemegang_saham_perseorangan(keyword) {
    var data_pemegang_saham = {
        suggestions: []
    };
    $('#form-4 .item-pemegang-saham').each(function () {
        if ($('select[id^="jenis_pemegang_saham"]', this).val() == 'perseorangan' && $('input[id^="pemegang_saham_perseorangan"]', this).val()) {
            if ($('input[id^="pemegang_saham_perseorangan"]', this).val().toLowerCase().indexOf(keyword.toLowerCase()) !== -1) {
                var temp_data_pemegang_saham = {};
                temp_data_pemegang_saham.value = $('input[id^="pemegang_saham_perseorangan"]', this).val();
                temp_data_pemegang_saham.data = {
                    nama: $('input[id^="pemegang_saham_perseorangan"]', this).val(),
                    ktp: $('input[id^="ktp_pemegang_saham_perseorangan"]', this).val(),
                    npwp: $('input[id^="npwp_pemegang_saham_perseorangan"]', this).val(),
                    alamat: $('input[id^="alamat_pemegang_saham_perseorangan"]', this).val()
                };
                data_pemegang_saham.suggestions.push(temp_data_pemegang_saham);
            }
        }
    });

    return data_pemegang_saham;
}

function generate_table_resume() {
    var el_scroll_resume_form = new SimpleBar(document.getElementById('resume-form'), {autoHide: false});
    $('#col_tujuan_pendirian').text($('#form-1 #tujuan_pendirian').val());
    $('#col_nama_perusahaan_1').text($('#form-1 #nama_perusahaan_1').val());
    $('#col_nama_perusahaan_2').text($('#form-1 #nama_perusahaan_3').val());
    $('#col_nama_perusahaan_3').text($('#form-1 #nama_perusahaan_3').val());

    var show_vo = false;
    var show_sewa = false;
    var show_sendiri = false;
    $('#table-domisili-vo').addClass('hide');
    $('#table-domisili-sewa').addClass('hide');
    $('#table-domisili-sendiri').addClass('hide');
    if ($('#form-2 input[name="kepemilikan"]').val() == 'virtual_office') {
        show_vo = true;
    } else if ($('#form-2 input[name="kepemilikan"]').val() == 'sewa') {
        show_sewa = true;
    } else if ($('#form-2 input[name="kepemilikan"]').val() == 'milik_sendiri') {
        show_sendiri = true;
    }
    if (show_vo) {
        $('#col_alamat_domisili_vo').text($('#form-2 #alamat_domisili_vo').val());
        $('#col_nama_gedung_vo').text($('#form-2 #nama_gedung_vo').val());
        $('#col_provinsi_vo').text(($('#form-2 #provinsi_vo').val() != 'Pilih Provinsi') ? $('#form-2 #provinsi_vo').closest('.form-group').find('.select2-selection__rendered').text() : '');
        $('#col_kota_madya_vo').text(($('#form-2 #kota_madya_vo').val() != 'Pilih Kota Madya') ? $('#form-2 #kota_madya_vo').closest('.form-group').find('.select2-selection__rendered').text() : '');
        $('#col_kecamatan_vo').text(($('#form-2 #kecamatan_vo').val() != 'Pilih Kecamatan') ? $('#form-2 #kecamatan_vo').closest('.form-group').find('.select2-selection__rendered').text() : '');
        $('#col_kelurahan_vo').text(($('#form-2 #kelurahan_vo').val() != 'Pilih Kelurahan') ? $('#form-2 #kelurahan_vo').closest('.form-group').find('.select2-selection__rendered').text() : '');
        $('#table-domisili-vo').removeClass('hide');
    }

    if (show_sewa) {
        $('#col_alamat_domisili_sewa').text($('#form-2 #alamat_domisili_sewa').val());
        $('#col_nama_gedung_sewa').text($('#form-2 #nama_gedung_sewa').val());
        $('#col_provinsi_sewa').text(($('#form-2 #provinsi_sewa').val() != 'Pilih Provinsi') ? $('#form-2 #provinsi_sewa').closest('.form-group').find('.select2-selection__rendered').text() : '');
        $('#col_kota_madya_sewa').text(($('#form-2 #kota_madya_sewa').val() != 'Pilih Kota Madya') ? $('#form-2 #kota_madya_sewa').closest('.form-group').find('.select2-selection__rendered').text() : '');
        $('#col_kecamatan_sewa').text(($('#form-2 #kecamatan_sewa').val() != 'Pilih Kecamatan') ? $('#form-2 #kecamatan_sewa').closest('.form-group').find('.select2-selection__rendered').text() : '');
        $('#col_kelurahan_sewa').text(($('#form-2 #kelurahan_sewa').val() != 'Pilih Kelurahan') ? $('#form-2 #kelurahan_sewa').closest('.form-group').find('.select2-selection__rendered').text() : '');
        $('#table-domisili-sewa').removeClass('hide');
    }

    if (show_sendiri) {
        $('#col_alamat_domisili_sendiri').text($('#form-2 #alamat_domisili_sendiri').val());
        $('#col_nama_gedung_sendiri').text($('#form-2 #nama_gedung_sendiri').val());
        $('#col_provinsi_sendiri').text(($('#form-2 #provinsi_sendiri').val() != 'Pilih Provinsi') ? $('#form-2 #provinsi_sendiri').closest('.form-group').find('.select2-selection__rendered').text() : '');
        $('#col_kota_madya_sendiri').text(($('#form-2 #kota_madya_sendiri').val() != 'Pilih Kota Madya') ? $('#form-2 #kota_madya_sendiri').closest('.form-group').find('.select2-selection__rendered').text() : '');
        $('#col_kecamatan_sendiri').text(($('#form-2 #kecamatan_sendiri').val() != 'Pilih Kecamatan') ? $('#form-2 #kecamatan_sendiri').closest('.form-group').find('.select2-selection__rendered').text() : '');
        $('#col_kelurahan_sendiri').text(($('#form-2 #kelurahan_sendiri').val() != 'Pilih Kelurahan') ? $('#form-2 #kelurahan_sendiri').closest('.form-group').find('.select2-selection__rendered').text() : '');
        $('#table-domisili-sendiri').removeClass('hide');
    }

    $('#col_modal_dasar').text(($('#form-3 #modal_dasar').val()) ? 'Rp ' + thousand_format($('#form-3 #modal_dasar').val()) : '');
    $('#col_modal_disetor').text(($('#form-3 #modal_disetor').val()) ? 'Rp ' + thousand_format($('#form-3 #modal_disetor').val()) : '');
    $('#col_nominal_saham').text(($('#form-3 #nominal_saham').val()) ? 'Rp ' + thousand_format($('#form-3 #nominal_saham').val()) : '');
    var modal_disetor = parseInt($('#form-3 #modal_disetor').val().replace(/\./g, ""));
    var nominal_saham = parseInt($('#form-3 #nominal_saham').val().replace(/\./g, ""));
    var jumlah_saham = '';
    if (!isNaN(modal_disetor) && !isNaN(nominal_saham)) {
        if (nominal_saham > 0) {
            jumlah_saham = modal_disetor / nominal_saham;
        }
    }
    $('#col_jumlah_saham').text(Math.round(jumlah_saham));

    var table_pemegang_saham = '';
    $('#form-4 .item-pemegang-saham').each(function () {
        if ($(this).find('select[id^="jenis_pemegang_saham"]').val() == 'perusahaan') {
            table_pemegang_saham += '<table class="table table-bordered">\n\
                                    <tr>\n\
                                        <td colspan="2" class="text-center"><strong>' + $(this).find('.item-label').text() + '</strong></td>\n\
                                    </tr>\n\
                                    <tr>\n\
                                        <td><strong>Jenis Pemegang Saham</strong></td>\n\
                                        <td>Perusahaan</td>\n\
                                    </tr>\n\
                                    <tr>\n\
                                        <td><strong>Nama PT</strong></td>\n\
                                        <td>' + $(this).find('input[id^="pemegang_saham_perusahaan"]').val() + '</td>\n\
                                    </tr>\n\
                                    <tr>\n\
                                        <td><strong>No Akta Pendirian</strong></td>\n\
                                        <td>' + $(this).find('input[id^="no_akta_perusahaan"]').val() + '</td>\n\
                                    </tr>\n\
                                    <tr>\n\
                                        <td><strong>Tanggal Pendirian</strong></td>\n\
                                        <td>' + $(this).find('input[id^="tanggal_pendirian_perusahaan"]').val() + '</td>\n\
                                    </tr>\n\
                                    <tr>\n\
                                        <td><strong>No SK Menkumham</strong></td>\n\
                                        <td>' + $(this).find('input[id^="sk_menkumham_perusahaan"]').val() + '</td>\n\
                                    </tr>\n\
                                    <tr>\n\
                                        <td><strong>No NPWP Perusahaan</strong></td>\n\
                                        <td>' + $(this).find('input[id^="npwp_pemegang_saham_perusahaan"]').val() + '</td>\n\
                                    </tr>\n\
                                    <tr>\n\
                                        <td><strong>Jumlah Persentase Saham</strong></td>\n\
                                        <td>' + $(this).find('input[id^="persentase_pemegang_saham_perusahaan"]').val() + '</td>\n\
                                    </tr>\n\
                                </table>';
        } else {
            table_pemegang_saham += '<table class="table table-bordered">\n\
                                    <tr>\n\
                                        <td colspan="2" class="text-center"><strong>' + $(this).find('.item-label').text() + '</strong></td>\n\
                                    </tr>\n\
                                    <tr>\n\
                                        <td><strong>Jenis Pemegang Saham</strong></td>\n\
                                        <td>Perseorangan</td>\n\
                                    </tr>\n\
                                    <tr>\n\
                                        <td><strong>Nama Lengkap</strong></td>\n\
                                        <td>' + $(this).find('input[id^="pemegang_saham_perseorangan"]').val() + '</td>\n\
                                    </tr>\n\
                                    <tr>\n\
                                        <td><strong>Alamat</strong></td>\n\
                                        <td>' + $(this).find('input[id^="alamat_pemegang_saham_perseorangan"]').val() + '</td>\n\
                                    </tr>\n\
                                    <tr>\n\
                                        <td><strong>No KTP</strong></td>\n\
                                        <td>' + $(this).find('input[id^="ktp_pemegang_saham_perseorangan"]').val() + '</td>\n\
                                    </tr>\n\
                                    <tr>\n\
                                        <td><strong>No NPWP</strong></td>\n\
                                        <td>' + $(this).find('input[id^="npwp_pemegang_saham_perseorangan"]').val() + '</td>\n\
                                    </tr>\n\
                                    <tr>\n\
                                        <td><strong>Jumlah Persentase Saham</strong></td>\n\
                                        <td>' + $(this).find('input[id^="persentase_pemegang_saham_perseorangan"]').val() + '</td>\n\
                                    </tr>\n\
                                </table>';
        }
    });
    $('#table-pemegang-saham').html(table_pemegang_saham);

    var table_pengurus = '';
    $('#form-5 .item-pengurus').each(function () {
        table_pengurus += '<table class="table table-bordered">\n\
                                <tr>\n\
                                    <td colspan="2" class="text-center"><strong>' + $(this).find('.item-label').text() + '</strong></td>\n\
                                </tr>\n\
                                <tr>\n\
                                    <td><strong>Jenis Jabatan</strong></td>\n\
                                    <td>Dewan ' + capitalizeFirstLetter($(this).find('select[id^="jenis_jabatan"]').val()) + '</td>\n\
                                </tr>\n\
                                <tr>\n\
                                    <td><strong>Nama Jabatan</strong></td>\n\
                                    <td>' + $(this).find('input[id^="jabatan"]').val() + '</td>\n\
                                </tr>\n\
                                <tr>\n\
                                    <td><strong>Nama Pengurus</strong></td>\n\
                                    <td>' + $(this).find('input[id^="pengurus"]').val() + '</td>\n\
                                </tr>\n\
                                <tr>\n\
                                    <td><strong>Alamat</strong></td>\n\
                                    <td>' + $(this).find('input[id^="alamat_pengurus"]').val() + '</td>\n\
                                </tr>\n\
                                <tr>\n\
                                    <td><strong>KTP</strong></td>\n\
                                    <td>' + $(this).find('input[id^="ktp_pengurus"]').val() + '</td>\n\
                                </tr>\n\
                                <tr>\n\
                                    <td><strong>NPWP</strong></td>\n\
                                    <td>' + $(this).find('input[id^="npwp_pengurus"]').val() + '</td>\n\
                                </tr>\n\
                            </table>';
    });
    $('#table-pengurus').html(table_pengurus);
    el_scroll_resume_form.recalculate();
}