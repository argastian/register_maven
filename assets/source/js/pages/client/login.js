$(function () {
    $(".login form").on("submit", function (e) {
        e.preventDefault();
        $(".alert").remove();
        var tmp_btn = $(".login button").html();
        $(".login button").html("<img src='"+base_url+"assets/images/loader.gif'>");
        var param = $(".login form").serialize();
        $.ajax({
            url: site_url + 'client/login/authentication',
            type: "POST",
            data: param,
            success: function (result) {
                if (result == 'valid') {
                    window.location = site_url + 'client/home';
                } else {
                    window.location = site_url + 'client';
                }
            }, 
            error: function() {
                window.location = site_url + 'client';
            }
        });
    });

});