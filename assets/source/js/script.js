function get_key(e)
{
    if (window.event)
        return window.event.keyCode;
    else if (e)
        return e.which;
    else
        return null;
}
function good_chars(e, goods, field)
{
    var key, keychar;
    key = get_key(e);
    if (key == null)
        return true;
    keychar = String.fromCharCode(key);
    keychar = keychar.toLowerCase();
    goods = goods.toLowerCase();
    if (goods.indexOf(keychar) != -1)
        return true;
    if (key == null || key == 0 || key == 8 || key == 9 || key == 27)
        return true;
    if (key == 13)
    {
        var i;
        for (i = 0; i < field.form.elements.length; i++)
            if (field == field.form.elements[i])
                break;
        i = (i + 1) % field.form.elements.length;
        field.form.elements[i].focus();
        return false;
    }
    return false;
}
function prevent_number(e) {
    var key = get_key(e);
    if (key > 47 && key < 58) {
        e.preventDefault();
    }
}
/*Fungsi untuk mengecek apakah form input diisi atau tidak*/
/*parameter input_id berisi attribute id dari form input yang akan dicek*/
function check_input_val(input_id) {
    if (input_id) {
        var escape_input_id = input_id.replace('[', '\\[').replace(']', '\\]');
        var this_input = $('#' + escape_input_id);
        var this_val = this_input.val();
        this_val = (this_val != null) ? this_val.trim() : this_val;
        clear_form_status(input_id);
        if (this_val && this_val != null && this_val != "Pilih Provinsi") {
            form_success(input_id);
            return true;
        } else {
            return false;
        }
    }
}
/*Fungsi untuk mengecek apakah form input required diisi atau tidak*/
/*parameter input_id berisi attribute id dari form input yang akan dicek*/
function check_required_val(input_id) {
    var escape_input_id = input_id.replace('[', '\\[').replace(']', '\\]');
    var this_input = $('#' + escape_input_id);
    var this_val = this_input.val();

    if (this_val.trim()) {
        form_success(input_id);
        return true;
    } else {
        form_error(input_id, "Bidang ini harus diisi!");
        return false;
    }
}
/*Fungsi untuk mengecek apakah isi dari form input sama dengan form input yang lain atau tidak*/
/*parameter input_id berisi attribute id dari form input yang akan dicek dan array_input_id merupakan array attribute id dari form input yang dijadikan perbandingan*/
function check_unique_val(input_id, array_input_id) {
    var escape_input_id = input_id.replace('[', '\\[').replace(']', '\\]');
    var this_input = $('#' + escape_input_id);
    var this_val = this_input.val();
    var data_unique = true;

    if (this_val) {
        $(array_input_id).each(function (key, item) {
            var escape_item = item.replace('[', '\\[').replace(']', '\\]');
            if (input_id != item) {
                if (this_val.toLowerCase() == $('#' + escape_item).val().toLowerCase()) {
                    data_unique = false;
                }
            }
        });
        if (data_unique) {
            form_success(input_id);
            return true;
        } else {
            form_error(input_id, "Sudah ada!");
            return false;
        }
    } else {
        clear_form_status(input_id);
    }
}

function check_total_saham() {
    var total_saham = 0;
    $('input[id^="persentase_pemegang_saham_perusahaan"]').each(function () {
        var persentase_saham = parseInt($(this).val());
        if (!isNaN(persentase_saham)) {
            total_saham += persentase_saham;
        }
    });
    $('input[id^="persentase_pemegang_saham_perseorangan"]').each(function () {
        var persentase_saham = parseInt($(this).val());
        if (!isNaN(persentase_saham)) {
            total_saham += persentase_saham;
        }
    });
    if (total_saham == 100) {
        $('input[id^="persentase_pemegang_saham_perusahaan"]').each(function () {
            clear_form_status($(this).attr('id'));
            if ($(this).val()) {
                if ($(this).val() == 0) {
                    form_error($(this).attr('id'), 'Tidak boleh 0%');
                    return false;
                } else if ($(this).val() == 100) {
                    form_error($(this).attr('id'), 'Tidak boleh 100%');
                    return false;
                } else {
                    form_success($(this).attr('id'));
                }
            }
        });
        $('input[id^="persentase_pemegang_saham_perseorangan"]').each(function () {
            clear_form_status($(this).attr('id'));
            if ($(this).val()) {
                if ($(this).val() == 100) {
                    form_error($(this).attr('id'), 'Tidak boleh 100%');
                    return false;
                } else {
                    form_success($(this).attr('id'));
                }
            }
        });
    } else {
        $('input[id^="persentase_pemegang_saham_perusahaan"]').each(function () {
            clear_form_status($(this).attr('id'));
            if ($(this).val()) {
                if ($(this).val() <= 0) {
                    form_error($(this).attr('id'), 'Tidak boleh 0%');
                } else {
                    form_error($(this).attr('id'), 'Total harus 100%');
                }
            }
        });
        $('input[id^="persentase_pemegang_saham_perseorangan"]').each(function () {
            clear_form_status($(this).attr('id'));
            if ($(this).val()) {
                if ($(this).val() <= 0) {
                    form_error($(this).attr('id'), 'Tidak boleh 0%');
                } else {
                    form_error($(this).attr('id'), 'Total harus 100%');
                }
            }
        });
    }
}

function check_modal_dasar(input_id) {
    clear_form_status(input_id);
    var modal_dasar = parseInt($('#modal_dasar').val());
    var modal_disetor = parseInt($('#modal_disetor').val());
    if (!isNaN(modal_dasar) && !isNaN(modal_disetor)) {
        if (modal_dasar < 50000000) {
            form_error(input_id, 'Modal dasar minimal Rp 50.000.000!');
        } else if (modal_dasar < modal_disetor) {
            form_error(input_id, 'Modal dasar tidak boleh kurang dari modal disetor!');
        } else {
            form_success(input_id);
        }
    } else {
        if ($('#' + input_id).val()) {
            if (modal_dasar < 50000000) {
                form_error(input_id, 'Modal dasar minimal Rp 50.000.000!');
            } else {
                form_success(input_id);
            }
        }
    }
}

function check_modal_disetor(input_id) {
    clear_form_status(input_id);
    var modal_dasar = ($('#modal_dasar').val()) ? parseInt($('#modal_dasar').val()) : 0;
    var modal_disetor = parseInt($('#modal_disetor').val());
    if (!isNaN(modal_dasar) && !isNaN(modal_disetor)) {
        var minimum_modal_disetor = 25 * modal_dasar / 100;
        if (modal_disetor < minimum_modal_disetor) {
            form_error(input_id, 'Modal disetor minimal 25% dari modal dasar!');
        } else if (modal_dasar < modal_disetor) {
            form_error(input_id, 'Modal disetor tidak boleh lebih dari modal dasar!');
        } else {
            form_success(input_id);
        }
    } else {
        if ($('#' + input_id).val()) {
            form_success(input_id);
        }
    }
}

/*Fungsi untuk mengecek apakah form input berisi alamat email yang valid atau tidak*/
/*parameter input_id berisi attribute id dari form input yang akan dicek*/
function validate_email(input_id) {
    var escape_input_id = input_id.replace('[', '\\[').replace(']', '\\]');
    var this_input = $('#' + escape_input_id);
    var this_val = this_input.val();
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (re.test(this_val)) {
        form_success(input_id);
        return true;
    } else {
        form_error(input_id, "Format email salah!");
        return false;
    }
}

/*Fungsi untuk mengecek apakah form input retype password sesuai dengan form input password*/
/*parameter input_id_pass berisi attribute id dari form input password dan input_id_retype_pass berisi attribute id dari form input re-type password*/
function validate_retype_password(input_id_retype_pass, input_id_pass) {
    input_id_retype_pass = input_id_retype_pass.replace('[', '\\[').replace(']', '\\]');
    var re_pass_input = $('#' + input_id_retype_pass);

    input_id_pass = input_id_pass.replace('[', '\\[').replace(']', '\\]');
    var pass_input = $('#' + input_id_pass);

    if (pass_input.val() && re_pass_input.val()) {
        if (pass_input.val() == re_pass_input.val()) {
            if ($('#password').closest('.form-group').hasClass('has-error')) {
                form_error(input_id_retype_pass, "Perbaiki password terlebih dahulu!");
                return false;
            } else {
                form_success(input_id_retype_pass);
                return true;
            }
        } else {
            form_error(input_id_retype_pass, "Ulangi password dengan benar!");
            return false;
        }
    }
}

function validate_language_not_english(input_id) {
    var escape_input_id = input_id.replace('[', '\\[').replace(']', '\\]');
    var this_input = $('#' + escape_input_id);
    var this_parent = this_input.closest('.form-group');
    var this_val = this_input.val();
    if (this_val && this_parent.hasClass('check-language') != true && this_parent.hasClass('valid-language') != true) {
        $.ajax({
            url: site_url + 'ajax/check_language',
            type: 'POST',
            data: {data: this_val, register_maven_token: $('#form-maven input[name="register_maven_token"]').val()},
            dataType: 'json',
            beforeSend: function () {
                clear_form_status(input_id);
                this_parent.addClass('has-feedback');
                this_parent.addClass('check-language');
                this_parent.addClass('has-warning').append('<span class="fa fa-spin fa-spinner form-control-feedback" aria-hidden="true"></span>');
            },
            success: function (response) {
                $('#form-maven input[name="register_maven_token"]').val(response.csrf);
                if (response.data == 'no') {
                    this_parent.addClass('valid-language');
                    this_parent.removeClass('check-language');
                    form_success(input_id);
                    return true;
                } else {
                    form_error(input_id, "Nama harus berbahasa Indonesia!");
                    return false;
                }
            },
            error: function () {
                form_custom(input_id, 'warning', "Kami tidak bisa memastikan nama ini berbahasa Indonesia");
            }
        });
    }
}

/*Fungsi untuk membuat tanda error dan menampilkan pesan error pada form*/
function form_error(input_id, msg) {
    var escape_input_id = input_id.replace('[', '\\[').replace(']', '\\]');
    var this_input = $('#' + escape_input_id);
    var this_parent = this_input.closest('.form-group');

    clear_form_status(input_id);
    this_parent.addClass('has-feedback');
    this_parent.addClass('has-error').append('<span class="fa fa-times-circle form-control-feedback" aria-hidden="true"></span>');
    this_parent.append('<div class="error-msg"><small>' + msg + '</small></div>');
}

/*Fungsi untuk membuat tanda sukses (valid) pada form*/
function form_success(input_id) {
    var escape_input_id = input_id.replace('[', '\\[').replace(']', '\\]');
    var this_input = $('#' + escape_input_id);
    var this_parent = this_input.closest('.form-group');

    clear_form_status(input_id);
    this_parent.addClass('has-feedback');
    this_parent.addClass('has-success').append('<span class="fa fa-check-circle form-control-feedback" aria-hidden="true"></span>');
}

/*Fungsi untuk membuat tanda custom pada form*/
function form_custom(input_id, type, msg) {
    var escape_input_id = input_id.replace('[', '\\[').replace(']', '\\]');
    var this_input = $('#' + escape_input_id);
    var this_parent = this_input.closest('.form-group');

    clear_form_status(input_id);
    this_parent.addClass('has-feedback');
    var class_feedback;
    var icon_msg;
    var class_msg;
    switch (type) {
        case 'success':
            class_feedback = 'has-success';
            icon_msg = '<span class="fa fa-check-circle form-control-feedback" aria-hidden="true"></span>';
            class_msg = 'success-msg';
            break;
        case 'warning':
            class_feedback = 'has-warning';
            icon_msg = '<span class="fa fa-check-circle form-control-feedback" aria-hidden="true"></span>';
            class_msg = 'warning-msg';
            break;
        case 'error':
            class_feedback = 'has-error';
            icon_msg = '<span class="fa fa-times-circle form-control-feedback" aria-hidden="true"></span>';
            class_msg = 'error-msg';
            break;
    }
    this_parent.addClass(class_feedback).append(icon_msg);
    this_parent.append('<div class="' + class_msg + '"><small>' + msg + '</small></div>');
}

/*Fungsi untuk menghapus tanda sukses (valid) atau error pada form*/
function clear_form_status(input_id) {
    var escape_input_id = input_id.replace('[', '\\[').replace(']', '\\]');
    var this_input = $('#' + escape_input_id);
    var this_parent = this_input.closest('.form-group');
    this_parent.removeClass('has-success').removeClass('has-error').removeClass('has-warning');
    this_parent.find('.form-control-feedback').remove();
    this_parent.find('.error-msg').remove();
    this_parent.find('.warning-msg').remove();
    this_parent.find('.success-msg').remove();
}

function get_option_kota_kab(prov, id_select) {
    if (prov !== 'Pilih Provinsi') {
        $.ajax({
            url: site_url + 'ajax/get_kota_kab',
            type: 'POST',
            data: {prov: prov, register_maven_token: $('#form-maven input[name="register_maven_token"]').val()},
            dataType: 'json',
            beforeSend: function () {
                $('#' + id_select).html('<option>loading...<option>');
            },
            success: function (response) {
                $('#form-maven input[name="register_maven_token"]').val(response.csrf);
                $('#' + id_select).html('');
                $(response.data).each(function (key, val) {
                    $('#' + id_select).append('<option value="' + val.kode + '">' + display_nama_wilayah(val.nama) + '</option>');
                });
                $('#' + id_select).trigger('change');
            }
        });
    } else {
        $('#' + id_select).html('<option>Pilih Kota Madya</option>').trigger('change');
        clear_form_status(id_select);
    }
}

function get_option_kecamatan(kota_kab, id_select) {
    if (kota_kab !== 'Pilih Kota Madya') {
        $.ajax({
            url: site_url + 'ajax/get_kecamatan',
            type: 'POST',
            data: {kota_kab: kota_kab, register_maven_token: $('#form-maven input[name="register_maven_token"]').val()},
            dataType: 'json',
            beforeSend: function () {
                $('#' + id_select).html('<option>loading...<option>');
            },
            success: function (response) {
                $('#form-maven input[name="register_maven_token"]').val(response.csrf);
                $('#' + id_select).html('');
                $(response.data).each(function (key, val) {
                    $('#' + id_select).append('<option value="' + val.kode + '">' + display_nama_wilayah(val.nama) + '</option>');
                });
                $('#' + id_select).trigger('change');
            }
        });
    } else {
        $('#' + id_select).html('<option>Pilih Kecamatan</option>').trigger('change');
        clear_form_status(id_select);
    }
}

function get_option_kelurahan(kec, id_select) {
    if (kec !== 'Pilih Kecamatan') {
        $.ajax({
            url: site_url + 'ajax/get_kelurahan',
            type: 'POST',
            data: {kec: kec, register_maven_token: $('#form-maven input[name="register_maven_token"]').val()},
            dataType: 'json',
            beforeSend: function () {
                $('#' + id_select).html('<option>loading...<option>');
            },
            success: function (response) {
                $('#form-maven input[name="register_maven_token"]').val(response.csrf);
                $('#' + id_select).html('');
                $(response.data).each(function (key, val) {
                    $('#' + id_select).append('<option value="' + val.kode + '">' + display_nama_wilayah(val.nama) + '</option>');
                });
                $('#' + id_select).trigger('change');
            }
        });
    } else {
        $('#' + id_select).html('<option>Pilih Kelurahan</option>').trigger('change');
        clear_form_status(id_select);
    }
}

function get_wilayah(kode) {
    $.ajax({
        url: site_url + 'ajax/get_wilayah',
        type: 'POST',
        data: {kode: kode, register_maven_token: $('#form-maven input[name="register_maven_token"]').val()},
        dataType: 'json',
        success: function (response) {
            $('#form-maven input[name="register_maven_token"]').val(response.csrf);
            if (response.data) {
                return display_nama_wilayah(response.nama);
            }
        }
    });
}

function unique_email(input_id) {
    var escape_input_id = input_id.replace('[', '\\[').replace(']', '\\]');
    var this_input = $('#' + escape_input_id);
    var this_parent = this_input.closest('.form-group');
    var this_val = this_input.val();
    if (this_val) {
        $.ajax({
            url: site_url + 'ajax/unique_email',
            type: 'POST',
            data: {email: this_val, type: 'client', register_maven_token: $('#form-maven input[name="register_maven_token"]').val()},
            dataType: 'json',
            beforeSend: function () {
                clear_form_status(input_id);
                this_parent.addClass('has-feedback');
                this_parent.addClass('check-language');
                this_parent.addClass('has-warning').append('<span class="fa fa-spin fa-spinner form-control-feedback" aria-hidden="true"></span>');
            },
            success: function (response) {
                $('#form-maven input[name="register_maven_token"]').val(response.csrf);
                var result = parseInt(response.data);
                if (result < 1) {
                    this_parent.addClass('valid-language');
                    this_parent.removeClass('check-language');
                    form_success(input_id);
                    return true;
                } else {
                    form_error(input_id, "Email sudah terdaftar!");
                    return false;
                }
            },
            error: function () {
                form_custom(input_id, 'warning', "Kami tidak bisa memastikan email ini belum terdaftar");
            }
        });
    }
}

function display_nama_wilayah(id_wilayah) {
    var split = id_wilayah.split(" ");
    var exclude = ['DKI'];
    var result = [];
    $.each(split, function (key, val) {
        result.push(($.inArray(val, exclude) > -1) ? val : capitalizeFirstLetter(val.toLowerCase()));
    });
    return result.join(' ');
}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function search_equal_data(keyword, class_container_search) {
    var found = '';
    $('.' + class_container_search).each(function () {
        if (keyword.toLowerCase() == $(this).find('input[id^=pemegang_saham_perseorangan]').val().toLowerCase()) {
            found = $(this);
        }
    });
    return found;
}

function thousand_format(number) {
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}

function build_success_message(str) {
    return "<div class='alert alert-success alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>" + str + "</div>";
}

function build_warning_message(str) {
    return "<div class='alert alert-warning alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>" + str + "</div>";
}

function build_error_message(str) {
    return "<div class='alert alert-danger alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>" + str + "</div>";
}
function hide_alert() {
    setTimeout(function () {
        $('.alert.alert-dismissible').fadeOut("slow", function () {
            $(this).remove();
        });
    }, 6000);
}
var mvtable;
$(function () {
    hide_alert();
    if ($("#mvtable").length) {
        mvtable = $("#mvtable").DataTable({
            lengthChange: false,
            "bSort": false,
            "iDisplayLength": 7,
            "language": {
                "lengthMenu": "Display _MENU_ records per page",
                "zeroRecords": "Tidak ada data",
                "info": "Menampilkan halaman _PAGE_ dari total _PAGES_ halaman",
                "infoEmpty": "",
                "infoFiltered": "",
                "oPaginate": {
                    "sPrevious": "&lt;",
                    "sFirst": "Awal",
                    "sNext": "&gt;",
                    "sLast": "Akhir"
                },
                "sSearch": "Cari :"
            }
        });

        mvtable.on('order.dt search.dt', function () {
            mvtable.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();
    }
});