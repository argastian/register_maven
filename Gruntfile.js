module.exports = function (grunt) {

    // Project configuration.
    grunt.initConfig({
        uglify: {
            script: {
                files: [{
                        expand: true,
                        cwd: 'assets/source/js',
                        src: ['**/**/*.js'],
                        dest: 'assets/js',
                        rename: function (dst, src) {
                            return dst + '/' + src.replace('.js', '.min.js');
                        }
                    }]
            }
        },
        less: {
            style: {
                options: {
                    compress: true
                },
                expand: true,
                cwd: 'assets/source/less',
                src: ['**/**/*.less'],
                dest: 'assets/css',
                ext: '.min.css'
            }
        }
    });

    // Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-less');

    // Default task(s).
    grunt.registerTask('default', ['less']);

};