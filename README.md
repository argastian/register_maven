#MAVEN

Aplikasi untuk registrasi pembuatan PT

## Fitur

- Formulir Registrasi
- Halaman Administrasi
- ~~Generate data registrasi ke bentuk file .doc~~ (belum selesai)
 
### 1. Formulir Registrasi

#### Halaman pertama

Data profile client, wajib diisi. Password minimal 6 karakter, dengan kombinasi angka dan huruf besar dan huruf kecil.

#### Halaman kedua

Nama dan tujuan perusahaan, nama perusahaan harus menggunakan Bahasa Indonesia. Sebagai validasi, nama perusahaan akan dicek ke dalam list kata dasar bahasa Indonesia yang diperoleh dari [sini](https://github.com/sastrawi/sastrawi/blob/master/data/kata-dasar.txt). Jika ada dalam list berarti nama tersebut berbahasa Indonesia, jika tidak ada maka dilakukan pengecekan kembali menggunakan  API di [https://detectlanguage.com/](https://detectlanguage.com/) untuk memastikan nama tersebut bukan bahasa asing.

#### Halaman ketiga

Data domisili, diisi salah satu

#### Halaman keempat

Modal dasar minimal 50.000.000, modal disetor tidak boleh dari modal dasar dan minimal 25% dari modal dasar.

#### Halaman kelima

Pemegang saham, jika diisi minimal harus 2 pemegang saham, atau bisa dikosongkan untuk diisi dilain waktu melalui menu client area (halaman administrasi, perlu login). 

#### Halaman keenam

Pengurus perusahaan, jika nama pengurus sudah ada dalam data pemegang saham maka akan muncul auto suggest nama pengurus perusahaan.

#### Halaman ketujuh

Konfirmasi data, klik tombol setuju dan simpan maka akan terkirim data registrasi ke email client, dan client sudah bisa login ke halaman administrasi.

### 2. Halaman Administrasi

- Client akses melalui \[alamat domain maven\]/client, misal **http://maven.id/client**, **username**: administrator **password**: Maven2017
- Admin akses melalui \[alamat domain maven\]/administrator, misal **http://maven.id/administrator**, **username**: administrator **password**: Maven2017 

#### Halaman Administrasi untuk Admin

##### Menu Client

Berisi list client yang terdaftar, klik tombol detail untuk melihat detail data client beserta data perusahaan, download gambar dokumen yang telah client upload dan status progress pembuatan perusahaan.

##### Menu FAQ

Mengelola daftar pertanyaan dan jawaban yang akan ditampilkan pada Halaman Administrasi untuk Client.

##### Menu Setting

###### Tab Halaman Registrasi

Pengaturan untuk Formulir Registrasi. **API Key Detect Language** harus diisi. **Email Notifikasi Client Baru** jika diisi maka ketika ada client baru yang registrasi akan otomatis dikirim pemberitahuan dan data client ke email yang telah disetting sebelumnya.

###### Tab Mail Server

Pengaturan untuk keperluan pengiriman email saat ada client mendaftar, reset password dan lain sebagainya.

## Komponen yang digunakan

Framework menggunakan CodeIgniter 3.1.6, DBMS menggunakan MySQL, framework css Bootstrap v3.3.7.

## Development

Import database yang berada di dalam folder db, kemudian sesuaikan pengaturan database yang ada di **application\config\database.php**. Sesuaikan **$config\['base_url'\]** di **application\config\config.php**. js dan css di minify melalui grunt. Sebelum menggunakan grunt install node.js, kemudian install npm, setelah itu install [grunt](https://gruntjs.com/getting-started). Cara penggunaan buka cmd, arahkan ke directory tempat project ini disimpan, kemudian ketik **grunt less** untuk minify css dan **grunt uglify** untuk minify js. File js dan css yang belum diminify ada di **assets/source/js** untuk file js dan **assets/source/less** untuk file less, jika ingin ditambah atau diubah maka source ini yang tambah atau diubahnya. Hasil dari minify ini akan ada di **assets/js** dan **assets/css**.