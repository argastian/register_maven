<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Model_wilayah extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function get_all_provinsi() {
        $sql = "SELECT kode,nama FROM wilayah WHERE CHAR_LENGTH(kode)=2 ORDER BY nama";
        $query = $this->db->query($sql);
        return $query->result();
    }
    
    public function get_kota_kab_by_provinsi($id_provinsi) {
        $sql = "SELECT * FROM wilayah WHERE LEFT(kode,?)=? AND CHAR_LENGTH(kode)=5 ORDER BY nama";
        $query = $this->db->query($sql, array(strlen($id_provinsi), $id_provinsi));
        return $query->result();
    }
    
    public function get_kecamatan_by_kota_kab($id_kota_kab) {
        $sql = "SELECT * FROM wilayah WHERE LEFT(kode,?)=? AND CHAR_LENGTH(kode)=8 ORDER BY nama";
        $query = $this->db->query($sql, array(strlen($id_kota_kab), $id_kota_kab));
        return $query->result();
    }
    
    public function get_kelurahan_by_kecamatan($id_kec) {
        $sql = "SELECT * FROM wilayah WHERE LEFT(kode,?)=? AND CHAR_LENGTH(kode)=13 ORDER BY nama";
        $query = $this->db->query($sql, array(strlen($id_kec), $id_kec));
        return $query->result();
    }
    
    public function get_wilayah_by_id($id_wilayah) {
        $sql = "SELECT nama FROM wilayah WHERE kode=?";
        $query = $this->db->query($sql, array($id_wilayah));
        return $query->row();
    }

}
