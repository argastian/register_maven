<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Model_client extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function check_login($email) {
        $sql = "SELECT kode, nama_lengkap, alamat_email, password FROM client WHERE alamat_email=?";
        $query = $this->db->query($sql, $email);
        return $query->row_array();
    }

    public function get_by_cookie($cookie) {
        $sql = "SELECT kode, nama_lengkap, alamat_email, password FROM client WHERE cookie=?";
        $query = $this->db->query($sql, $cookie);
        return $query->row_array();
    }

    public function get_exist_data($table, $data) {
        $query = $this->db->get_where($table, $data);
        return $query->num_rows();
    }

    public function insert_client($data) {
        $this->db->insert('client', $data);
        if ($this->db->affected_rows()) {
            return $this->db->insert_id();
        } else {
            return NULL;
        }
    }

    public function is_token_reset_valid($token) {
        $tkn = substr($token, 0, 30);
        $kode = substr($token, 30);
        
        $sql = "SELECT limit_waktu_reset FROM client WHERE kode=? AND token_reset=?";
        $query = $this->db->query($sql, array($kode, $tkn));        
        $result = $query->row();
        if($result){
            $waktu_reset = $result->limit_waktu_reset;
            $today = date('Y-m-d H:i');
            if(strtotime($today) > strtotime($waktu_reset)){
                return FALSE;
            } else {
                return TRUE;
            }
        } else {
            return FALSE;
        }
    }

    public function insert_data_perusahaan($data) {
        $this->db->insert('data_perusahaan', $data);
        if ($this->db->affected_rows()) {
            return $this->db->insert_id();
        } else {
            return NULL;
        }
    }

    public function insert_data_pemegang_saham_perseorangan($data) {
        $this->db->insert('pemegang_saham_perseorangan', $data);
        return $this->db->affected_rows();
    }

    public function insert_data_pemegang_saham_perusahaan($data) {
        $this->db->insert('pemegang_saham_perusahaan', $data);
        return $this->db->affected_rows();
    }

    public function insert_data_pengurus_perusahaan($data) {
        $this->db->insert('pengurus_perusahaan', $data);
        return $this->db->affected_rows();
    }

    public function get_all_data_client($order = 'nama_lengkap ASC') {
        $sql = "SELECT kode, nama_lengkap, alamat_email, no_telpon, waktu_buat, waktu_ubah FROM client ORDER BY ?";
        $query = $this->db->query($sql, $order);
        return $query->result();
    }

    public function get_data_client($kode_client = NULL, $id_client = NULL) {
        if ($kode_client) {
            $sql = "SELECT kode, nama_lengkap, alamat_email, no_telpon, waktu_buat, waktu_ubah FROM client WHERE kode=?";
            $query = $this->db->query($sql, $kode_client);
        } else {
            $sql = "SELECT kode, nama_lengkap, alamat_email, no_telpon, waktu_buat, waktu_ubah FROM client WHERE id_client=?";
            $query = $this->db->query($sql, $id_client);
        }
        return $query->row();
    }

    public function get_data_perusahaan($kode_client = NULL, $id_perusahaan = NULL) {
        if ($kode_client) {
            $sql = "SELECT kode, nama_perusahaan_1, nama_perusahaan_2, nama_perusahaan_3, tujuan_pendirian, kepemilikan, alamat_domisili, nama_gedung, provinsi, kota_madya, kecamatan, kelurahan, modal_dasar, modal_disetor, nominal_saham, bidang, status, kode_client, waktu_buat, waktu_ubah FROM data_perusahaan WHERE kode_client=?";
            $query = $this->db->query($sql, $kode_client);
        } else {
            $sql = "SELECT kode, nama_perusahaan_1, nama_perusahaan_2, nama_perusahaan_3, tujuan_pendirian, kepemilikan, alamat_domisili, nama_gedung, provinsi, kota_madya, kecamatan, kelurahan, modal_dasar, modal_disetor, nominal_saham, bidang, status, kode_client, waktu_buat, waktu_ubah FROM data_perusahaan WHERE id_data_perusahaan=?";
            $query = $this->db->query($sql, $id_perusahaan);
        }
        return $query->row();
    }

    public function get_data_pemegang_saham_perseorangan($id_data_perusahaan) {
        $sql = "SELECT kode, nama, no_ktp, no_npwp, alamat, persentase_saham FROM pemegang_saham_perseorangan WHERE kode_perusahaan=?";
        $query = $this->db->query($sql, $id_data_perusahaan);
        return $query->result();
    }

    public function get_data_pemegang_saham_perusahaan($id_data_perusahaan) {
        $sql = "SELECT kode, nama, no_akta, tanggal_pendirian, no_sk, no_npwp, persentase_saham FROM pemegang_saham_perusahaan WHERE kode_perusahaan=?";
        $query = $this->db->query($sql, $id_data_perusahaan);
        return $query->result();
    }

    public function get_data_pengurus_perusahaan($id_data_perusahaan) {
        $sql = "SELECT kode, jenis_jabatan, nama_jabatan, nama_pengurus, no_ktp, no_npwp, alamat FROM pengurus_perusahaan WHERE kode_perusahaan=?";
        $query = $this->db->query($sql, $id_data_perusahaan);
        return $query->result();
    }

    public function update_data_client($data) {
        $this->db->update('client', $data, array('kode' => $data['kode']));
        return $this->db->affected_rows();
    }

    public function update_data_perusahaan($data) {
        $this->db->update('data_perusahaan', $data, array('kode' => $data['kode']));
        return $this->db->affected_rows();
    }

    public function delete_data_pemegang_saham_perseorangan($id_data_perusahaan) {
        $del = $this->db->delete('pemegang_saham_perseorangan', array('kode_perusahaan' => $id_data_perusahaan));
        if ($this->db->affected_rows()) {
            return "success";
        } else {
            return "failed";
        }
    }

    public function delete_data_pemegang_saham_perusahaan($id_data_perusahaan) {
        $del = $this->db->delete('pemegang_saham_perusahaan', array('kode_perusahaan' => $id_data_perusahaan));
        if ($this->db->affected_rows()) {
            return "success";
        } else {
            return "failed";
        }
    }

    public function delete_data_pengurus_perusahaan($id_data_perusahaan) {
        $del = $this->db->delete('pengurus_perusahaan', array('kode_perusahaan' => $id_data_perusahaan));
        if ($this->db->affected_rows()) {
            return "success";
        } else {
            return "failed";
        }
    }
    
    public function get_data_file($id_client) {
        $sql = "SELECT nama_file, kode_pemegang_saham, kode_pengurus_perusahaan, tipe FROM file_upload WHERE kode_client=?";
        $query = $this->db->query($sql, $id_client);
        return $query->result();
    }
    
    public function insert_file($data) {
        $this->db->insert('file_upload', $data);
        if ($this->db->affected_rows()) {
            return $this->db->insert_id();
        } else {
            return NULL;
        }
    }

}
