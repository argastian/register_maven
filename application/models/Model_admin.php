<?php

class Model_admin extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function check_login($username) {
        $sql = "SELECT kode, nama, username, email, password FROM admin WHERE username=?";
        $query = $this->db->query($sql, $username);
        return $query->row_array();
    }
    
    public function check_email($email) {
        $sql = "SELECT kode, nama, username, email, password FROM admin WHERE email=?";
        $query = $this->db->query($sql, $email);
        return $query->row_array();
    }
    
    public function get_by_cookie($cookie) {
        $sql = "SELECT kode, nama, username, email, password FROM admin WHERE cookie=?";
        $query = $this->db->query($sql, $cookie);
        return $query->row_array();
    }

    public function get_exist_data($table, $data) {
        $query = $this->db->get_where($table, $data);
        return $query->num_rows();
    }
    
    public function is_token_reset_valid($token) {
        $tkn = substr($token, 0, 30);
        $kode = substr($token, 30);
        
        $sql = "SELECT limit_waktu_reset FROM admin WHERE kode=? AND token_reset=?";
        $query = $this->db->query($sql, array($kode, $tkn));        
        $result = $query->row();
        if($result){
            $waktu_reset = $result->limit_waktu_reset;
            $today = date('Y-m-d H:i');
            if(strtotime($today) > strtotime($waktu_reset)){
                return FALSE;
            } else {
                return TRUE;
            }
        } else {
            return FALSE;
        }
    }

    public function get_admin($id_admin = null) {
        if ($id_admin) {
            $sql = "SELECT kode, nama, username, password, email FROM admin WHERE kode=?";
            $query = $this->db->query($sql, $id_admin);
            return $query->row();
        } else {
            $this->db->select("kode, nama, username, password, email");
            $query = $this->db->get("admin");
            return $query->result();
        }
    }

    public function insert_admin($data) {
        $this->db->insert("admin", $data);
        return $this->db->affected_rows();
    }

    public function update_admin($data) {
        $this->db->update("admin", $data, array("kode" => $data['kode']));
        return $this->db->affected_rows();
    }

    public function delete_admin($id_admin) {
        $del = $this->db->delete("admin", array("kode" => $id_admin));
        if ($this->db->affected_rows()) {
            return "success";
        } else {
            return "failed";
        }
    }

}
