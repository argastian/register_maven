<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Model_config extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function get_config() {
        $sql = "SELECT id_config, api_key_detect_language, link_youtube, faq_profile, faq_nama, faq_alamat, faq_modal, faq_pemegang_saham, faq_pengurus, email_notifikasi_client, alamat_kantor FROM config";
        $query = $this->db->query($sql);
        return $query->row();
    }
    
    public function get_email_config() {
        $sql = "SELECT email_protocol, email_smtp_host, email_smtp_port, email_smtp_user, email_smtp_pass FROM config";
        $query = $this->db->query($sql);
        return $query->row();
    }
    
    public function update_config($data) {
        $this->db->update('config', $data, array('id_config' => 0));
        return $this->db->affected_rows();
    }
}
