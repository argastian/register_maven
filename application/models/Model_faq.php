<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Model_faq extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function get_faq($id_faq = NULL) {
        if ($id_faq) {
            $sql = "SELECT * FROM faq WHERE id_faq=?";
            $query = $this->db->query($sql, $id_faq);
            return $query->row();
        } else {
            $sql = "SELECT * FROM faq";
            $query = $this->db->query($sql);
            return $query->result();
        }
    }

    public function insert_faq($data) {
        $this->db->insert('faq', $data);
        if ($this->db->affected_rows()) {
            return $this->db->insert_id();
        } else {
            return NULL;
        }
    }

    public function update_faq($data) {
        $this->db->update('faq', $data, array('id_faq' => $data['id_faq']));
        return $this->db->affected_rows();
    }

    public function delete_faq($id_faq) {
        $del = $this->db->delete('faq', array('id_faq' => $id_faq));
        if ($this->db->affected_rows()) {
            return "success";
        } else {
            return "failed";
        }
    }

}
