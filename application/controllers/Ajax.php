<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . 'libraries/detectlanguage/detectlanguage.php');

use \DetectLanguage\DetectLanguage;

class Ajax extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->_config = $this->config_app->get_config();
    }

    public function check_language() {
        DetectLanguage::setApiKey($this->_config->api_key_detect_language);
        $string = $this->input->post('data');
        $contain_english = 'no';
        $check_words = array();
        foreach (explode(" ", $string) as $word) {
            if ($this->check_indonesian_library($word) === false) {
                $check_words[] = $word;
            }
        }
        if ($check_words) {
            $result = DetectLanguage::detect($check_words);
            if ($result) {
                foreach ($result as $item) {
                    if ($item[0]->isReliable && $item[0]->language == 'en') {
                        $contain_english = 'yes';
                    }
                }
            }
        }
        $result = array(
            'csrf' => $this->generate_token(),
            'data' => $contain_english
        );
        echo json_encode($result);
    }

    public function check_indonesian_library($searchfor) {
        $file = base_url('assets/library_file/indonesia.txt');
        header('Content-Type: text/plain');
        $contents = file_get_contents($file);
        $pattern = preg_quote(strtolower($searchfor), '/');
        $pattern = "/^.*$pattern.*\$/m";
        if (preg_match_all($pattern, $contents, $matches)) {
            return true;
        } else {
            return false;
        }
    }

    public function get_kota_kab() {
        $id_provinsi = $this->input->post('prov');
        $kota_kab = $this->wilayah->get_kota_kab_by_provinsi($id_provinsi);
        $result = array(
            'csrf' => $this->generate_token(),
            'data' => $kota_kab
        );
        echo json_encode($result);
    }

    public function get_kecamatan() {
        $id_kota_kab = $this->input->post('kota_kab');
        $kecamatan = $this->wilayah->get_kecamatan_by_kota_kab($id_kota_kab);
        $result = array(
            'csrf' => $this->generate_token(),
            'data' => $kecamatan
        );
        echo json_encode($result);
    }

    public function get_kelurahan() {
        $id_kec = $this->input->post('kec');
        $kelurahan = $this->wilayah->get_kelurahan_by_kecamatan($id_kec);
        $result = array(
            'csrf' => $this->generate_token(),
            'data' => $kelurahan
        );
        echo json_encode($result);
    }

    public function get_wilayah() {
        $id_wilayah = $this->input->post('kode');
        $wilayah = $this->wilayah->get_wilayah_by_id($id_wilayah);
        $result = array(
            'csrf' => $this->generate_token(),
            'data' => $wilayah
        );
        echo json_encode($result);
    }

    function unique_email() {
        $email = $this->input->post('email');
        $type = $this->input->post('type');
        $data = array('alamat_email' => $email);
        switch ($type) {
            case 'client':
            default :
                $result = $this->client->get_exist_data('client', $data);
                break;
        }
        $result = array(
            'csrf' => $this->generate_token(),
            'data' => $result
        );
        echo json_encode($result);
    }

    private function generate_token() {
        return $this->security->get_csrf_hash();
    }

}
