<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Dompdf\Dompdf;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

Class Register extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->_config = $this->config_app->get_config();
        $this->_email_config = $this->config_app->get_email_config();
    }

    public function index() {
        $provinsi = $this->wilayah->get_all_provinsi();
        $data = array(
            'title' => 'Registrasi',
            'page' => 'register',
            'css' => array(
                'includes/select2/css/select2',
                'includes/select2/theme/select2-bootstrap',
                'includes/bootstrap-datetimepicker/css/bootstrap-datetimepicker',
                'css/pages/register'
            ),
            'js' => array(
                'includes/select2/js/select2',
                'includes/moment/moment',
                'includes/bootstrap-datetimepicker/js/bootstrap-datetimepicker',
                'includes/pwstrength-bootstrap/pwstrength-bootstrap',
                'includes/jquery-autocomplete/jquery.autocomplete',
                'js/pages/register'
            ),
            'provinsi' => $provinsi,
            'config_app' => $this->_config
        );
        $this->load->view('templates/default', $data);
    }

    public function submit() {
        $params = $this->input->post();
        $status = 'sukses';
        $id_kode_client = '';
        if ($params) {
            $data_client = array(
                'kode' => generate_unique_id(),
                'nama_lengkap' => $params['nama_lengkap'],
                'alamat_email' => $params['alamat_email'],
                'no_telpon' => $params['no_telpon'],
                'password' => password_hash($params['password'], PASSWORD_DEFAULT)
            );
            $id_client = $this->client->insert_client($data_client);
            if ($id_client) {
                $kode_client = $this->client->get_data_client(NULL, $id_client);
                $id_kode_client = $kode_client->kode;
                $data_perusahaan = array(
                    'kode' => generate_unique_id(),
                    'nama_perusahaan_1' => $params['nama_perusahaan_1'],
                    'nama_perusahaan_2' => $params['nama_perusahaan_2'],
                    'nama_perusahaan_3' => $params['nama_perusahaan_3'],
                    'tujuan_pendirian' => $params['tujuan_pendirian'],
                    'kepemilikan' => $params['kepemilikan'],
                    'alamat_domisili' => $params['alamat'][$params['kepemilikan']]['alamat_domisili'],
                    'nama_gedung' => $params['alamat'][$params['kepemilikan']]['nama_gedung'],
                    'provinsi' => $params['alamat'][$params['kepemilikan']]['provinsi'],
                    'kota_madya' => $params['alamat'][$params['kepemilikan']]['kota_madya'],
                    'kecamatan' => $params['alamat'][$params['kepemilikan']]['kecamatan'],
                    'kelurahan' => $params['alamat'][$params['kepemilikan']]['kelurahan'],
                    'modal_dasar' => $params['modal_dasar'],
                    'modal_disetor' => $params['modal_disetor'],
                    'nominal_saham' => $params['nominal_saham'],
                    'kode_client' => $kode_client->kode,
                );
                $id_data_perusahaan = $this->client->insert_data_perusahaan($data_perusahaan);
                if ($id_data_perusahaan) {
                    $kode_perusahaan = $this->client->get_data_perusahaan(NULL, $id_data_perusahaan);
                    if (isset($params['pemegang_saham']['perseorangan'])) {
                        foreach ($params['pemegang_saham']['perseorangan'] as $perseorangan) {
                            if ($perseorangan['nama']) {
                                $data_pemegang_saham = array(
                                    'kode' => generate_unique_id(),
                                    'nama' => $perseorangan['nama'],
                                    'no_ktp' => $perseorangan['no_ktp'],
                                    'no_npwp' => $perseorangan['no_npwp'],
                                    'alamat' => $perseorangan['alamat'],
                                    'persentase_saham' => $perseorangan['persentase_saham'],
                                    'kode_perusahaan' => $kode_perusahaan->kode
                                );
                                $this->client->insert_data_pemegang_saham_perseorangan($data_pemegang_saham);
                            }
                        }
                    }
                    if (isset($params['pemegang_saham']['perusahaan'])) {
                        foreach ($params['pemegang_saham']['perusahaan'] as $perusahaan) {
                            if ($perusahaan['nama']) {
                                $data_pemegang_saham = array(
                                    'kode' => generate_unique_id(),
                                    'nama' => $perusahaan['nama'],
                                    'no_akta' => $perusahaan['no_akta'],
                                    'tanggal_pendirian' => $perusahaan['tanggal_pendirian'],
                                    'no_sk' => $perusahaan['no_sk'],
                                    'no_npwp' => $perusahaan['no_npwp'],
                                    'persentase_saham' => $perusahaan['persentase_saham'],
                                    'kode_perusahaan' => $kode_perusahaan->kode
                                );
                                $this->client->insert_data_pemegang_saham_perusahaan($data_pemegang_saham);
                            }
                        }
                    }
                    if (isset($params['pengurus'])) {
                        foreach ($params['pengurus'] as $pengurus) {
                            $data_pengurus = array(
                                'kode' => generate_unique_id(),
                                'jenis_jabatan' => $pengurus['jenis_jabatan'],
                                'nama_jabatan' => $pengurus['nama_jabatan'],
                                'nama_pengurus' => $pengurus['nama_pengurus'],
                                'no_ktp' => $pengurus['no_ktp'],
                                'no_npwp' => $pengurus['no_npwp'],
                                'alamat' => $pengurus['alamat'],
                                'kode_perusahaan' => $kode_perusahaan->kode
                            );
                            $this->client->insert_data_pengurus_perusahaan($data_pengurus);
                        }
                    }
                }
                if ($id_kode_client) {
                    $this->send_email_admin($id_kode_client);
                    $this->send_email_client($id_kode_client);
                }
            } else {
                $status = 'error';
            }
        }
        $result = array(
            'csrf' => $this->generate_token(),
            'status' => $status
        );
        echo json_encode($result);
    }

    public function send_email_admin($kode_client) {
        $this->load->library('email');
        if (isset($this->_email_config->email_protocol) && $this->_email_config->email_protocol && isset($this->_email_config->email_smtp_host) && $this->_email_config->email_smtp_host && isset($this->_email_config->email_smtp_port) && $this->_email_config->email_smtp_port && isset($this->_email_config->email_smtp_user) && $this->_email_config->email_smtp_user && isset($this->_email_config->email_smtp_pass) && $this->_email_config->email_smtp_pass) {
            $config = array(
                'protocol' => $this->_email_config->email_protocol,
                'smtp_host' => $this->_email_config->email_smtp_host,
                'smtp_port' => $this->_email_config->email_smtp_port,
                'smtp_user' => $this->_email_config->email_smtp_user,
                'smtp_pass' => $this->_email_config->email_smtp_pass,
                'mailtype' => 'html',
                'charset' => 'utf-8'
            );
            $this->email->initialize($config);
        }
        $msg = $this->generate_data_client($kode_client);
        if ($this->_config->email_notifikasi_client) {
            $this->email->from('no-reply@maven.id', 'Registrasi Maven');
            $this->email->to($this->_config->email_notifikasi_client);
            $this->email->subject('Informasi Client Baru');
            $this->email->set_mailtype("html");
            $this->email->message($msg);
            $this->email->send();
        }
    }

    public function send_email_client($kode_client) {
        $this->load->library('email');
        if (isset($this->_email_config->email_protocol) && $this->_email_config->email_protocol && isset($this->_email_config->email_smtp_host) && $this->_email_config->email_smtp_host && isset($this->_email_config->email_smtp_port) && $this->_email_config->email_smtp_port && isset($this->_email_config->email_smtp_user) && $this->_email_config->email_smtp_user && isset($this->_email_config->email_smtp_pass) && $this->_email_config->email_smtp_pass) {
            $config = array(
                'protocol' => $this->_email_config->email_protocol,
                'smtp_host' => $this->_email_config->email_smtp_host,
                'smtp_port' => $this->_email_config->email_smtp_port,
                'smtp_user' => $this->_email_config->email_smtp_user,
                'smtp_pass' => $this->_email_config->email_smtp_pass,
                'mailtype' => 'html',
                'charset' => 'utf-8'
            );
            $this->email->initialize($config);
        }
        $client = $this->client->get_data_client($kode_client);
        $detail_data = $this->generate_data_client($kode_client);

        $dompdf = new Dompdf();
        $dompdf->loadHtml($detail_data);
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();
        $output = $dompdf->output();
        if (!file_exists('doc/' . $client->kode)) {
            mkdir('doc/' . $client->kode, 0777, true);
        }
        file_put_contents('doc/' . $client->kode . '/registrasi_maven_' . str_replace(" ", "_", strtolower($client->nama_lengkap)) . '.pdf', $output);

        $msg = '<p>
            Terima kasih telah melengkapi data data pendirian perusahaan Anda, saat ini data Anda
            akan di proses oleh tim kami. Tim kami akan menghubungi Anda dalam waktu dekat.
        </p>
        <p>
            Data yang telah Anda masukan kami sertakan dalam lampiran email ini. Untuk mengubah atau menambah data Anda, bisa login di <a href="' . site_url('client') . '" target="_BLANK">sini</a>.
        </p>
        <p>Terima kasih.</p>';
        $this->email->from('no-reply@maven.id', 'Registrasi Maven');
        $this->email->to($client->alamat_email);
        $this->email->subject('Informasi Registrasi Maven');
        $this->email->set_mailtype("html");
        $this->email->message($msg);
        $this->email->attach(base_url('doc/' . $client->kode . '/registrasi_maven_' . str_replace(" ", "_", strtolower($client->nama_lengkap)) . '.pdf'));
        $this->email->send();
    }

    private function generate_data_client($kode_client) {
        $client = $this->client->get_data_client($kode_client);
        $perusahaan = isset($client->kode) ? $this->client->get_data_perusahaan($client->kode) : '';
        $provinsi = isset($perusahaan->provinsi) ? $this->wilayah->get_wilayah_by_id($perusahaan->provinsi) : '';
        $kota_madya = isset($perusahaan->kota_madya) ? $this->wilayah->get_wilayah_by_id($perusahaan->kota_madya) : '';
        $kecamatan = isset($perusahaan->kecamatan) ? $this->wilayah->get_wilayah_by_id($perusahaan->kecamatan) : '';
        $kelurahan = isset($perusahaan->kelurahan) ? $this->wilayah->get_wilayah_by_id($perusahaan->kelurahan) : '';
        $jumlah_saham = '';
        if (isset($perusahaan->modal_disetor) && isset($perusahaan->nominal_saham)) {
            if ($perusahaan->modal_disetor != "" && $perusahaan->modal_disetor != NULL && $perusahaan->nominal_saham != "" && $perusahaan->nominal_saham != NULL && $perusahaan->nominal_saham > 0) {
                $jumlah_saham = round($perusahaan->modal_disetor / $perusahaan->nominal_saham);
            }
        }
        $saham_perseorangan = '';
        $saham_perusahaan = '';
        $pengurus = '';
        if (isset($perusahaan->kode)) {
            $no = 1;
            $pemegang_saham_perseorangan = $this->client->get_data_pemegang_saham_perseorangan($perusahaan->kode);
            if ($pemegang_saham_perseorangan) {
                foreach ($pemegang_saham_perseorangan as $key => $item) {
                    $saham_perseorangan .= '<table border="1" cellpadding="7" cellspacing="0">
                        <tbody>
                            <tr>
                                <td colspan="2" align="center"><strong>Pemegang Saham ' . $no . '</strong></td>
                            </tr>
                            <tr>
                                <td><strong>Jenis Pemegang Saham</strong></td>
                                <td>Perseorangan</td>
                            </tr>
                            <tr>
                                <td><strong>Nama Lengkap</strong></td>
                                <td>' . html_escape($item->nama) . '</td>
                            </tr>
                            <tr>
                                <td><strong>Alamat</strong></td>
                                <td>' . html_escape($item->alamat) . '</td>
                            </tr>
                            <tr>
                                <td><strong>No KTP</strong></td>
                                <td>' . html_escape($item->no_ktp) . '</td>
                            </tr>
                            <tr>
                                <td><strong>No NPWP</strong></td>
                                <td>' . html_escape($item->no_npwp) . '</td>
                            </tr>
                            <tr>
                                <td><strong>Jumlah Persentase Saham</strong></td>
                                <td>' . html_escape($item->persentase_saham) . '%</td>
                            </tr>
                        </tbody>
                    </table>';
                    $no++;
                }
            }
            $pemegang_saham_perusahaan = $this->client->get_data_pemegang_saham_perusahaan($perusahaan->kode);
            if ($pemegang_saham_perusahaan) {
                foreach ($pemegang_saham_perusahaan as $key => $item) {
                    $saham_perusahaan .= '<table border="1" cellpadding="7" cellspacing="0">
                        <tbody>
                            <tr>
                                <td colspan="2" align="center"><strong>Pemegang Saham ' . $no . '</strong></td>
                            </tr>
                            <tr>
                                <td><strong>Jenis Pemegang Saham</strong></td>
                                <td>Perusahaan</td>
                            </tr>
                            <tr>
                                <td><strong>Nama PT</strong></td>
                                <td>' . html_escape($item->nama) . '</td>
                            </tr>
                            <tr>
                                <td><strong>No Akta Pendirian</strong></td>
                                <td>' . html_escape($item->no_akta) . '</td>
                            </tr>
                            <tr>
                                <td><strong>Tanggal Pendirian</strong></td>
                                <td>' . html_escape($item->tanggal_pendirian) . '</td>
                            </tr>
                            <tr>
                                <td><strong>No SK Menkumham</strong></td>
                                <td>' . html_escape($item->no_sk) . '</td>
                            </tr>
                            <tr>
                                <td><strong>No NPWP Perusahaan</strong></td>
                                <td>' . html_escape($item->no_npwp) . '</td>
                            </tr>
                            <tr>
                                <td><strong>Jumlah Persentase Saham</strong></td>
                                <td>' . html_escape($item->persentase_saham) . '%</td>
                            </tr>
                        </tbody>
                    </table>';
                    $no++;
                }
            }
            $no = 1;
            $pengurus_perusahaan = $this->client->get_data_pengurus_perusahaan($perusahaan->kode);
            if ($pengurus_perusahaan) {
                foreach ($pengurus_perusahaan as $key => $item) {
                    $pengurus .= '<table border="1" cellpadding="7" cellspacing="0">
                        <tbody>
                            <tr>
                                <td colspan="2" align="center"><strong>Pengurus Perusahaan ' . $no . '</strong></td>
                            </tr>
                            <tr>
                                <td><strong>Jenis Jabatan</strong></td>
                                <td>Dewan ' . ucfirst(html_escape($item->jenis_jabatan)) . '</td>
                            </tr>
                            <tr>
                                <td><strong>Nama Jabatan</strong></td>
                                <td>' . html_escape($item->nama_jabatan) . '</td>
                            </tr>
                            <tr>
                                <td><strong>Nama Pengurus</strong></td>
                                <td>' . html_escape($item->nama_pengurus) . '</td>
                            </tr>
                            <tr>
                                <td><strong>Alamat</strong></td>
                                <td>' . html_escape($item->alamat) . '</td>
                            </tr>
                            <tr>
                                <td><strong>KTP</strong></td>
                                <td>' . html_escape($item->no_ktp) . '</td>
                            </tr>
                            <tr>
                                <td><strong>NPWP</strong></td>
                                <td>' . html_escape($item->no_npwp) . '</td>
                            </tr>
                        </tbody>
                    </table>';
                    $no++;
                }
            }
        }
        $table_client = '<style>   
                    .table-profil tbody tr td{
                        border: 0px !important;
                        font-size: 16px;
                    }
                    table {
                        border-collapse: collapse;
                        border-spacing: 0;
                        width: 100%;
                        max-width: 100%;
                        margin-bottom: 20px;
                    }
                    table td{
                        padding: 4px;
                        font-size: 14px;
                        width: 50%;
                    }
                </style>
                <table class="table-profil" cellpadding="7" cellspacing="0">
                    <tbody>
                        <tr>
                            <td><strong>Nama Lengkap</strong></td>
                            <td id="col_nama_lengkap">' . (isset($client->nama_lengkap) ? html_escape($client->nama_lengkap) : '') . '</td>
                        </tr>
                        <tr>
                            <td><strong>Alamat Email</strong></td>
                            <td id="col_alamat_email">' . (isset($client->alamat_email) ? html_escape($client->alamat_email) : '') . '</td>
                        </tr>
                        <tr>
                            <td><strong>No Telpon</strong></td>
                            <td id="col_no_telpon">' . (isset($client->no_telpon) ? html_escape($client->no_telpon) : '') . '</td>
                        </tr>
                        <tr>
                            <td><strong>Waktu Registrasi</strong></td>
                            <td id="col_no_telpon">' . (isset($perusahaan->waktu_buat) ? display_date_time($perusahaan->waktu_buat) : '') . '</td>
                        </tr>
                    </tbody>
                </table>
                <h4>Nama &amp; Tujuan Perusahaan</h4>
                <table border="1" cellpadding="7" cellspacing="0">        
                    <tbody><tr>
                            <td><strong>Nama 1</strong></td>
                            <td id="col_nama_perusahaan_1">' . (isset($perusahaan->nama_perusahaan_1) ? html_escape($perusahaan->nama_perusahaan_1) : '') . '</td>
                        </tr>
                        <tr>
                            <td><strong>Nama 2</strong></td>
                            <td id="col_nama_perusahaan_2">' . (isset($perusahaan->nama_perusahaan_1) ? html_escape($perusahaan->nama_perusahaan_2) : '') . '</td>
                        </tr>
                        <tr>
                            <td><strong>Nama 3</strong></td>
                            <td id="col_nama_perusahaan_3">' . (isset($perusahaan->nama_perusahaan_1) ? html_escape($perusahaan->nama_perusahaan_3) : '') . '</td>
                        </tr>
                        <tr>
                            <td><strong>Tujuan Perusahaan</strong></td>
                            <td id="col_tujuan_pendirian">' . (isset($perusahaan->tujuan_pendirian) ? html_escape($perusahaan->tujuan_pendirian) : '') . '</td>
                        </tr>
                    </tbody>
                </table>
                <h4>Alamat Domisili</h4>
                <table border="1" cellpadding="7" cellspacing="0">
                    <tbody>
                        <tr>
                            <td colspan="2" align="center"><strong>' . (isset($perusahaan->kepemilikan) ? html_escape(ucwords(str_replace("_", " ", $perusahaan->kepemilikan))) : '&nbsp;') . '</strong></td>
                        </tr>
                        <tr>
                            <td><strong>Alamat Domisili</strong></td>
                            <td id="col_alamat_domisili_vo">' . (isset($perusahaan->alamat_domisili) ? html_escape($perusahaan->alamat_domisili) : '') . '</td>
                        </tr>
                        <tr>
                            <td><strong>Nama Gedung</strong></td>
                            <td id="col_nama_gedung_vo">' . (isset($perusahaan->nama_gedung) ? html_escape($perusahaan->nama_gedung) : '') . '</td>
                        </tr>
                        <tr>
                            <td><strong>Provinsi</strong></td>
                            <td id="col_provinsi_vo">' . (isset($provinsi->nama) ? format_nama_wilayah(html_escape($provinsi->nama)) : '') . '</td>
                        </tr>
                        <tr>
                            <td><strong>Kota Madya</strong></td>
                            <td id="col_kota_madya_vo">' . (isset($kota_madya->nama) ? format_nama_wilayah(html_escape($kota_madya->nama)) : '') . '</td>
                        </tr>
                        <tr>
                            <td><strong>Kecamatan</strong></td>
                            <td id="col_kecamatan_vo">' . (isset($kecamatan->nama) ? format_nama_wilayah(html_escape($kecamatan->nama)) : '') . '</td>
                        </tr>
                        <tr>
                            <td><strong>Kelurahan</strong></td>
                            <td id="col_kelurahan_vo">' . (isset($kelurahan->nama) ? format_nama_wilayah(html_escape($kelurahan->nama)) : '') . '</td>
                        </tr>
                    </tbody>
                </table>
                <h4>Modal</h4>
                <table border="1" cellpadding="7" cellspacing="0">
                    <tbody>
                        <tr>
                            <td><strong>Modal Dasar</strong></td>
                            <td id="col_modal_dasar" class="text-right">Rp ' . (isset($perusahaan->modal_dasar) ? display_number($perusahaan->modal_dasar) : '0') . '</td>
                        </tr>
                        <tr>
                            <td><strong>Modal Disetor &amp; Ditempatkan</strong></td>
                            <td id="col_modal_disetor" class="text-right">Rp ' . (isset($perusahaan->modal_disetor) ? display_number($perusahaan->modal_disetor) : '0') . '</td>
                        </tr>
                        <tr>
                            <td><strong>Nominal Saham/Lembar</strong></td>
                            <td id="col_nominal_saham" class="text-right">Rp ' . (isset($perusahaan->nominal_saham) ? display_number($perusahaan->nominal_saham) : '0') . '</td>
                        </tr>
                        <tr>
                            <td><strong>Jumlah Saham</strong></td>
                            <td id="col_jumlah_saham" class="text-right">' . $jumlah_saham . '</td>
                        </tr>
                    </tbody>
                </table>
                <h4>Pemegang Saham</h4>
                <div id="table-pemegang-saham">
                ' . $saham_perseorangan . $saham_perusahaan . '
                </div>
                <h4>Pengurus Perusahaan</h4>
                <div id="table-pengurus">
                ' . $pengurus . '
                </div>';
        return $table_client;
    }

    public function generate_excel_client($kode_client) {
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        $spreadsheet = $reader->load("assets/template/template_data_registrasi.xlsx");
        $sheet = $spreadsheet->getActiveSheet();

        $client = $this->client->get_data_client($kode_client);
        $perusahaan = isset($client->kode) ? $this->client->get_data_perusahaan($client->kode) : '';
        $provinsi = isset($perusahaan->provinsi) ? $this->wilayah->get_wilayah_by_id($perusahaan->provinsi) : '';
        $kota_madya = isset($perusahaan->kota_madya) ? $this->wilayah->get_wilayah_by_id($perusahaan->kota_madya) : '';
        $kecamatan = isset($perusahaan->kecamatan) ? $this->wilayah->get_wilayah_by_id($perusahaan->kecamatan) : '';
        $kelurahan = isset($perusahaan->kelurahan) ? $this->wilayah->get_wilayah_by_id($perusahaan->kelurahan) : '';
        $jumlah_saham = '';
        if (isset($perusahaan->modal_disetor) && isset($perusahaan->nominal_saham)) {
            if ($perusahaan->modal_disetor != "" && $perusahaan->modal_disetor != NULL && $perusahaan->nominal_saham != "" && $perusahaan->nominal_saham != NULL && $perusahaan->nominal_saham > 0) {
                $jumlah_saham = round($perusahaan->modal_disetor / $perusahaan->nominal_saham);
            }
        }
        $styleTitle = array(
            'font' => array(
                'bold' => true
            )
        );

        $styleTitle = array(
            'font' => array(
                'bold' => true,
            ),
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ),
            'borders' => array(
                'top' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ),
                'left' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ),
                'right' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ),
                'bottom' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                )
            )
        );

        $styleTitleLeft = array(
            'font' => array(
                'bold' => true,
            ),
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
            ),
            'borders' => array(
                'top' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ),
                'left' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ),
                'right' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ),
                'bottom' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                )
            )
        );

        $styleOther = array(
            'font' => array(
                'bold' => false,
                'size' => 10.5
            ),
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
            ),
            'borders' => array(
                'top' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ),
                'left' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ),
                'right' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ),
                'bottom' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                )
            )
        );

        $sheet->setCellValue('B1', (isset($client->nama_lengkap) ? html_escape($client->nama_lengkap) : ''));
        $sheet->setCellValue('B2', (isset($client->alamat_email) ? html_escape($client->alamat_email) : ''));
        $sheet->setCellValue('B3', (isset($client->no_telpon) ? html_escape($client->no_telpon) : ''));
        $sheet->setCellValue('B4', (isset($perusahaan->waktu_buat) ? display_date_time($perusahaan->waktu_buat) : ''));

        $sheet->setCellValue('B8', (isset($perusahaan->nama_perusahaan_1) ? html_escape($perusahaan->nama_perusahaan_1) : ''));
        $sheet->setCellValue('B9', (isset($perusahaan->nama_perusahaan_2) ? html_escape($perusahaan->nama_perusahaan_2) : ''));
        $sheet->setCellValue('B10', (isset($perusahaan->nama_perusahaan_3) ? html_escape($perusahaan->nama_perusahaan_3) : ''));
        $sheet->setCellValue('B11', (isset($perusahaan->tujuan_pendirian) ? html_escape($perusahaan->tujuan_pendirian) : ''));

        $sheet->setCellValue('A15', (isset($perusahaan->kepemilikan) ? html_escape(ucwords(str_replace("_", " ", $perusahaan->kepemilikan))) : '&nbsp;'));
        $sheet->setCellValue('B16', (isset($perusahaan->alamat_domisili) ? html_escape($perusahaan->alamat_domisili) : ''));
        $sheet->setCellValue('B17', (isset($perusahaan->nama_gedung) ? html_escape($perusahaan->nama_gedung) : ''));
        $sheet->setCellValue('B18', (isset($provinsi->nama) ? format_nama_wilayah(html_escape($provinsi->nama)) : ''));
        $sheet->setCellValue('B19', (isset($kota_madya->nama) ? format_nama_wilayah(html_escape($kota_madya->nama)) : ''));
        $sheet->setCellValue('B20', (isset($kecamatan->nama) ? format_nama_wilayah(html_escape($kecamatan->nama)) : ''));
        $sheet->setCellValue('B21', (isset($kelurahan->nama) ? format_nama_wilayah(html_escape($kelurahan->nama)) : ''));

        $sheet->setCellValue('B25', 'Rp ' . (isset($perusahaan->modal_dasar) ? display_number($perusahaan->modal_dasar) : '0'));
        $sheet->setCellValue('B26', 'Rp ' . (isset($perusahaan->modal_disetor) ? display_number($perusahaan->modal_disetor) : '0'));
        $sheet->setCellValue('B27', 'Rp ' . (isset($perusahaan->nominal_saham) ? display_number($perusahaan->nominal_saham) : '0'));
        $sheet->setCellValue('B28', $jumlah_saham);

        $row_pemegang_saham = 31;
        $new_row_pemegang_saham = 40;
        $row_pengurus_perusahaan = 41;

        if (isset($perusahaan->kode)) {
            $no = 1;
            $pemegang_saham_perseorangan = $this->client->get_data_pemegang_saham_perseorangan($perusahaan->kode);
            if ($pemegang_saham_perseorangan) {
                foreach ($pemegang_saham_perseorangan as $key => $item) {
                    $baris = array();
                    for ($i = 0; $i <= 6; $i++) {
                        $row_pemegang_saham = $row_pemegang_saham + 1;
                        $baris[$i] = $row_pemegang_saham;
                    }
                    $sheet->setCellValue('A' . $baris[1], 'Jenis Pemegang Saham');
                    $sheet->setCellValue('A' . $baris[2], 'Nama Lengkap');
                    $sheet->setCellValue('A' . $baris[3], 'Alamat');
                    $sheet->setCellValue('A' . $baris[4], 'No KTP');
                    $sheet->setCellValue('A' . $baris[5], 'No NPWP');
                    $sheet->setCellValue('A' . $baris[6], 'Jumlah Persentase Saham');
                    $sheet->setCellValue('A' . $baris[0], 'Pemegang Saham ' . $no);
                    $sheet->mergeCells('A' . $baris[0] . ':B' . $baris[0]);
                    $sheet->getStyle('A' . $baris[0] . ':B' . $baris[0])->applyFromArray($styleTitle);
                    $sheet->setCellValue('B' . $baris[1], 'Perseorangan');
                    $sheet->setCellValue('B' . $baris[2], html_escape($item->nama));
                    $sheet->setCellValue('B' . $baris[3], html_escape($item->alamat));
                    $sheet->setCellValue('B' . $baris[4], html_escape($item->no_ktp));
                    $sheet->setCellValue('B' . $baris[5], html_escape($item->no_npwp));
                    $sheet->setCellValue('B' . $baris[6], html_escape($item->persentase_saham));
//                    echo 'new row '.$new_row_pemegang_saham.'<br>';
                    $sheet->insertNewRowBefore($new_row_pemegang_saham, 7);
                    $new_row_pemegang_saham = $new_row_pemegang_saham + 7;
                    $row_pengurus_perusahaan = $baris[6] + 2;
                    $no++;
                }
            }

            $pemegang_saham_perusahaan = $this->client->get_data_pemegang_saham_perusahaan($perusahaan->kode);
            if ($pemegang_saham_perusahaan) {
                foreach ($pemegang_saham_perusahaan as $key => $item) {
                    $baris = array();
                    for ($i = 0; $i <= 7; $i++) {
                        $row_pemegang_saham = $row_pemegang_saham + 1;
                        $baris[$i] = $row_pemegang_saham;
                    }
                    $sheet->setCellValue('A' . $baris[1], 'Jenis Pemegang Saham');
                    $sheet->setCellValue('A' . $baris[2], 'Nama PT');
                    $sheet->setCellValue('A' . $baris[3], 'No Akta Pendirian');
                    $sheet->setCellValue('A' . $baris[4], 'Tanggal Pendirian');
                    $sheet->setCellValue('A' . $baris[5], 'No SK Menkumham');
                    $sheet->setCellValue('A' . $baris[6], 'No NPWP Perusahaan');
                    $sheet->setCellValue('A' . $baris[7], 'Jumlah Persentase Saham');
                    $sheet->setCellValue('A' . $baris[0], 'Pemegang Saham ' . $no);
                    $sheet->mergeCells('A' . $baris[0] . ':B' . $baris[0]);
                    $sheet->getStyle('A' . $baris[0] . ':B' . $baris[0])->applyFromArray($styleTitle);
                    $sheet->setCellValue('B' . $baris[1], 'Perusahaan');
                    $sheet->setCellValue('B' . $baris[2], html_escape($item->nama));
                    $sheet->setCellValue('B' . $baris[3], html_escape($item->no_akta));
                    $sheet->setCellValue('B' . $baris[4], html_escape($item->tanggal_pendirian));
                    $sheet->setCellValue('B' . $baris[5], html_escape($item->no_sk));
                    $sheet->setCellValue('B' . $baris[6], html_escape($item->no_npwp));
                    $sheet->setCellValue('B' . $baris[7], html_escape($item->persentase_saham));
                    $sheet->insertNewRowBefore($new_row_pemegang_saham, 8);
                    $new_row_pemegang_saham = $new_row_pemegang_saham + 8;
                    $row_pengurus_perusahaan = $baris[7] + 2;
                    $no++;
                }
            }
            $no = 1;
            $pengurus_perusahaan = $this->client->get_data_pengurus_perusahaan($perusahaan->kode);
            if ($pengurus_perusahaan) {
                $sheet->setCellValue('A' . $row_pengurus_perusahaan, 'Pengurus Perusahaan');
                $row_pengurus_perusahaan = $row_pengurus_perusahaan + 1;
                foreach ($pengurus_perusahaan as $key => $item) {
                    $baris = array();
                    for ($i = 0; $i <= 6; $i++) {
                        $row_pengurus_perusahaan = $row_pengurus_perusahaan + 1;
                        $baris[$i] = $row_pengurus_perusahaan;
                    }
                    $sheet->setCellValue('A' . $baris[1], 'Jenis Jabatan');
                    $sheet->setCellValue('A' . $baris[2], 'Nama Jabatan');
                    $sheet->setCellValue('A' . $baris[3], 'Nama Pengurus');
                    $sheet->setCellValue('A' . $baris[4], 'Alamat');
                    $sheet->setCellValue('A' . $baris[5], 'KTP');
                    $sheet->setCellValue('A' . $baris[6], 'NPWP');
                    $sheet->setCellValue('A' . $baris[0], 'Pengurus_perusahaan ' . $no);
                    $sheet->mergeCells('A' . $baris[0] . ':B' . $baris[0]);
                    $sheet->getStyle('A' . $baris[0] . ':B' . $baris[0])->applyFromArray($styleTitle);
                    $sheet->setCellValue('B' . $baris[1], html_escape($item->jenis_jabatan));
                    $sheet->setCellValue('B' . $baris[2], html_escape($item->nama_jabatan));
                    $sheet->setCellValue('B' . $baris[3], html_escape($item->nama_pengurus));
                    $sheet->setCellValue('B' . $baris[4], html_escape($item->alamat));
                    $sheet->setCellValue('B' . $baris[5], html_escape($item->no_ktp));
                    $sheet->setCellValue('B' . $baris[6], html_escape($item->no_npwp));
                    $no++;
                }
            }
        }

        $writer = new Xlsx($spreadsheet);
        $writer->save('hello world.xlsx');
    }

    private function generate_token() {
        return $this->security->get_csrf_hash();
    }

}
