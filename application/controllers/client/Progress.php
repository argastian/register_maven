<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Progress extends CI_Controller {

    public function __construct() {
        parent::__construct();

        if (!$this->session->has_userdata("MavenClient")) {
            redirect('client');
        }
    }

    public function index() {
        $login_data = $this->session->userdata("MavenClient");
        session_write_close();
        $perusahaan = $this->client->get_data_perusahaan($login_data['kode']);
        $data = array(
            'title' => 'Progress Keseluruhan',
            'page' => 'client/progress',
            'css' => array(
                'css/pages/client/progress'
            ),
            'js' => array(
                'js/pages/client/progress'
            ),
            'login_data' => $login_data,
            'perusahaan' => $perusahaan
        );
        $this->load->view('templates/client', $data);
    }

}
