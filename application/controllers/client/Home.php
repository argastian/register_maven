<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->has_userdata("MavenClient")) {
            redirect('client');
        }

        $this->_config = $this->config_app->get_config();
    }

    public function index() {
        $login_data = $this->session->userdata("MavenClient");
        $client = $this->client->get_data_client($login_data['kode']);
        $perusahaan = $this->client->get_data_perusahaan($login_data['kode']);
        $pemegang_saham_perseorangan = '';
        $pemegang_saham_perusahaan = '';
        $pengurus_perusahaan = '';
        if (isset($perusahaan->kode)) {
            $pemegang_saham_perseorangan = $this->client->get_data_pemegang_saham_perseorangan($perusahaan->kode);
            $pemegang_saham_perusahaan = $this->client->get_data_pemegang_saham_perusahaan($perusahaan->kode);
            $pengurus_perusahaan = $this->client->get_data_pengurus_perusahaan($perusahaan->kode);
        }
        $provinsi = $this->wilayah->get_all_provinsi();
        $kota_madya = isset($perusahaan->kota_madya) ? $this->wilayah->get_kota_kab_by_provinsi($perusahaan->provinsi) : '';
        $kecamatan = isset($perusahaan->kecamatan) ? $this->wilayah->get_kecamatan_by_kota_kab($perusahaan->kota_madya) : '';
        $kelurahan = isset($perusahaan->kelurahan) ? $this->wilayah->get_kelurahan_by_kecamatan($perusahaan->kecamatan) : '';
        $faq = $this->faq->get_faq();
        $data = array(
            'title' => 'Client - Data Diri',
            'page' => 'client/home',
            'css' => array(
                'includes/select2/css/select2',
                'includes/select2/theme/select2-bootstrap',
                'includes/bootstrap-datetimepicker/css/bootstrap-datetimepicker',
                'css/pages/client/home'
            ),
            'js' => array(
                'includes/select2/js/select2',
                'includes/moment/moment',
                'includes/bootstrap-datetimepicker/js/bootstrap-datetimepicker',
                'includes/pwstrength-bootstrap/pwstrength-bootstrap',
                'includes/jquery-autocomplete/jquery.autocomplete',
                'js/pages/client',
                'js/pages/client/home'
            ),
            'login_data' => $login_data,
            'faq' => $faq,
            'client' => $client,
            'perusahaan' => $perusahaan,
            'provinsi' => $provinsi,
            'kota_madya' => $kota_madya,
            'kecamatan' => $kecamatan,
            'kelurahan' => $kelurahan,
            'pemegang_saham_perseorangan' => $pemegang_saham_perseorangan,
            'pemegang_saham_perusahaan' => $pemegang_saham_perusahaan,
            'pengurus_perusahaan' => $pengurus_perusahaan
        );
        $this->load->view('templates/client', $data);
    }
    
    public function submit_nama() {
        $params = $this->input->post();
        if ($params) {
            $data_perusahaan = array(
                'kode' => $params['id_perusahaan'],
                'nama_perusahaan_1' => $params['nama_perusahaan_1'],
                'nama_perusahaan_2' => $params['nama_perusahaan_2'],
                'nama_perusahaan_3' => $params['nama_perusahaan_3'],
                'tujuan_pendirian' => $params['tujuan_pendirian'],
                'waktu_ubah' => date('Y-m-d H:i:s')
            );
            $update = $this->client->update_data_perusahaan($data_perusahaan);
            if ($update > 0) {
                $this->session->set_flashdata('msg', build_success_message('Ubah nama & tujuan perusahaan berhasil'));
                redirect('client/home');
            } else {
                $this->session->set_flashdata('msg', build_error_message('Tidak ada data nama & tujuan perusahaan yang diubah'));
                redirect('client/home');
            }
        }
    }

    public function submit_alamat() {
        $params = $this->input->post();
        if ($params) {
            $data_perusahaan = array(
                'kode' => $params['id_perusahaan'],
                'kepemilikan' => $params['alamat']['kepemilikan'],
                'alamat_domisili' => $params['alamat']['alamat_domisili'],
                'nama_gedung' => $params['alamat']['nama_gedung'],
                'provinsi' => $params['alamat']['provinsi'],
                'kota_madya' => $params['alamat']['kota_madya'],
                'kecamatan' => $params['alamat']['kecamatan'],
                'kelurahan' => $params['alamat']['kelurahan'],
                'waktu_ubah' => date('Y-m-d H:i:s')
            );
            $update = $this->client->update_data_perusahaan($data_perusahaan);
            if ($update > 0) {
                $this->session->set_flashdata('msg', build_success_message('Ubah alamat domisili berhasil'));
                redirect('client/home');
            } else {
                $this->session->set_flashdata('msg', build_error_message('Tidak ada data alamat domisili yang diubah'));
                redirect('client/home');
            }
        }
    }

    public function submit_modal() {
        $params = $this->input->post();
        if ($params) {
            $data_perusahaan = array(
                'kode' => $params['id_perusahaan'],
                'modal_dasar' => $params['modal_dasar'],
                'modal_disetor' => $params['modal_disetor'],
                'nominal_saham' => $params['nominal_saham'],
                'waktu_ubah' => date('Y-m-d H:i:s')
            );
            $update = $this->client->update_data_perusahaan($data_perusahaan);
            if ($update > 0) {
                $this->session->set_flashdata('msg', build_success_message('Ubah modal berhasil'));
                redirect('client/home');
            } else {
                $this->session->set_flashdata('msg', build_error_message('Tidak ada data modal yang diubah'));
                redirect('client/home');
            }
        }
    }

    public function submit_bidang() {
        $params = $this->input->post();
        if ($params) {
            $data_perusahaan = array(
                'kode' => $params['id_perusahaan'],
                'bidang' => $params['bidang'],
                'waktu_ubah' => date('Y-m-d H:i:s')
            );
            $update = $this->client->update_data_perusahaan($data_perusahaan);
            if ($update > 0) {
                $this->session->set_flashdata('msg', build_success_message('Ubah bidang usaha berhasil'));
                redirect('client/home');
            } else {
                $this->session->set_flashdata('msg', build_error_message('Tidak ada data bidang usaha yang diubah'));
                redirect('client/home');
            }
        }
    }

    public function submit_pemegang_saham() {
        $params = $this->input->post();
        if ($params) {
            $this->client->delete_data_pemegang_saham_perseorangan($params['id_perusahaan']);
            if (isset($params['pemegang_saham']['perseorangan'])) {
                $this->client->delete_data_pemegang_saham_perseorangan($params['id_perusahaan']);
                foreach ($params['pemegang_saham']['perseorangan'] as $perseorangan) {
                    if ($perseorangan['nama']) {
                        $data_pemegang_saham = array(
                            'kode' => generate_unique_id(),
                            'nama' => $perseorangan['nama'],
                            'no_ktp' => $perseorangan['no_ktp'],
                            'no_npwp' => $perseorangan['no_npwp'],
                            'alamat' => $perseorangan['alamat'],
                            'persentase_saham' => $perseorangan['persentase_saham'],
                            'kode_perusahaan' => $params['id_perusahaan']
                        );
                        $this->client->insert_data_pemegang_saham_perseorangan($data_pemegang_saham);
                    }
                }
                $this->session->set_flashdata('msg', build_success_message('Ubah pemegang saham berhasil'));
                redirect('client/home');
            }
            if (isset($params['pemegang_saham']['perusahaan'])) {
                $this->client->delete_data_pemegang_saham_perusahaan($params['id_perusahaan']);
                foreach ($params['pemegang_saham']['perusahaan'] as $perusahaan) {
                    if ($perusahaan['nama']) {
                        $data_pemegang_saham = array(
                            'kode' => generate_unique_id(),
                            'nama' => $perusahaan['nama'],
                            'no_akta' => $perusahaan['no_akta'],
                            'tanggal_pendirian' => $perusahaan['tanggal_pendirian'],
                            'no_sk' => $perusahaan['no_sk'],
                            'no_npwp' => $perusahaan['no_npwp'],
                            'persentase_saham' => $perusahaan['persentase_saham'],
                            'kode_perusahaan' => $params['id_perusahaan']
                        );
                        $this->client->insert_data_pemegang_saham_perusahaan($data_pemegang_saham);
                    }
                }
                $this->session->set_flashdata('msg', build_success_message('Ubah pemegang saham berhasil'));
                redirect('client/home');
            }
        }
    }

    public function submit_pengurus_perusahaan() {
        $params = $this->input->post();
        if ($params) {
            if (isset($params['pengurus'])) {
                $this->client->delete_data_pengurus_perusahaan($params['id_perusahaan']);
                foreach ($params['pengurus'] as $pengurus) {
                    $data_pengurus = array(
                        'kode' => generate_unique_id(),
                        'jenis_jabatan' => $pengurus['jenis_jabatan'],
                        'nama_jabatan' => $pengurus['nama_jabatan'],
                        'nama_pengurus' => $pengurus['nama_pengurus'],
                        'no_ktp' => $pengurus['no_ktp'],
                        'no_npwp' => $pengurus['no_npwp'],
                        'alamat' => $pengurus['alamat'],
                        'kode_perusahaan' => $params['id_perusahaan']
                    );
                    $this->client->insert_data_pengurus_perusahaan($data_pengurus);
                }
                $this->session->set_flashdata('msg', build_success_message('Ubah pengurus perusahaan berhasil'));
                redirect('client/home');
            }
        }
    }
}
