<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dokumen extends CI_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->has_userdata("MavenClient")) {
            redirect('client');
        }

        $this->_config = $this->config_app->get_config();
    }

    public function index() {
        $login_data = $this->session->userdata("MavenClient");
        $perusahaan = $this->client->get_data_perusahaan($login_data['kode']);
        $file_dokumen = $this->client->get_data_file($login_data['kode']);
        $pemegang_saham_perseorangan = '';
        $pengurus_perusahaan = '';
        if (isset($perusahaan->kode)) {
            $pemegang_saham_perseorangan = $this->client->get_data_pemegang_saham_perseorangan($perusahaan->kode);
            $pengurus_perusahaan = $this->client->get_data_pengurus_perusahaan($perusahaan->kode);
        }
        $faq = $this->faq->get_faq();
        $data = array(
            'title' => 'Client - Data Dokumen',
            'page' => 'client/dokumen',
            'css' => array(
                'css/pages/client/dokumen'
            ),
            'js' => array(
                'js/pages/client',
                'js/pages/client/dokumen'
            ),
            'login_data' => $login_data,
            'pemegang_saham_perseorangan' => $pemegang_saham_perseorangan,
            'pengurus_perusahaan' => $pengurus_perusahaan,
            'faq' => $faq,
            'config_app' => $this->_config,
            'file_dokumen' => $file_dokumen
        );
        $this->load->view('templates/client', $data);
    }

    public function template($type) {
        switch ($type) {
            case 'setoran_modal':
                redirect(base_url('assets/template/[MAVEN]%20Template%20Pernyataan%20Setor%20Modal.doc'));
                break;
            case 'pernyataan_keabsahan':
                redirect(base_url('assets/template/[MAVEN]%20Template%20Pernyataan%20Keabsahan%20Pendirian%20PT.docx'));
                break;
            case 'pernyataan_domisili':
                redirect(base_url('assets/template/[MAVEN]%20Template%20Pernyataan%20Domisili%20Perseroan.docx'));
                break;
            case 'kuasa_hadap':
                redirect(base_url('assets/template/[MAVEN]%20Template%20Surat%20Kuasa%20Hadap.docx'));
                break;
            default :
                redirect('client/dokumen');
                break;
        }
    }

    public function submit_file_organ() {
        $this->load->library('upload');
        $config['allowed_types'] = 'gif|jpg|png|jpeg|pdf';
        $config['max_size'] = 2048;
        $config['max_width'] = 0;
        $config['max_height'] = 0;
        $error = array();
        $id_client = $this->input->post('id_client');
        if (!file_exists('doc/' . $id_client)) {
            mkdir('doc/' . $id_client, 0777, true);
        }
        $id_pemegang_saham = array();
        $id_pengurus_perusahaan = array();
        $type_file_pemegang_saham = array('ktp_pemegang_saham', 'npwp_pemegang_saham', 'kk_pemegang_saham');
        $type_file_pengurus_perusahaan = array('ktp_pengurus_perusahaan', 'npwp_pengurus_perusahaan', 'kk_pengurus_perusahaan');
        foreach ($type_file_pemegang_saham as $type) {
            foreach ($_FILES[$type]['name'] as $key => $val) {
                if (!in_array($key, $id_pemegang_saham)) {
                    $id_pemegang_saham[] = $key;
                }
            }
        }
        foreach ($type_file_pengurus_perusahaan as $type) {
            foreach ($_FILES[$type]['name'] as $key => $val) {
                if (!in_array($key, $id_pengurus_perusahaan)) {
                    $id_pengurus_perusahaan[] = $key;
                }
            }
        }
        foreach ($id_pemegang_saham as $id) {
            foreach ($type_file_pemegang_saham as $type) {
                if (isset($_FILES[$type]['name'][$id]) && $_FILES[$type]['name'][$id] != "") {
                    if (!file_exists('doc/' . $id_client . '/' . $id)) {
                        mkdir('doc/' . $id_client . '/' . $id, 0777, true);
                    }
                    $config['upload_path'] = './doc/' . $id_client . '/' . $id . '/';
                    $this->upload->initialize($config);
                    $_FILES['userfile']['name'] = $_FILES[$type]['name'][$id];
                    $_FILES['userfile']['type'] = $_FILES[$type]['type'][$id];
                    $_FILES['userfile']['tmp_name'] = $_FILES[$type]['tmp_name'][$id];
                    $_FILES['userfile']['error'] = $_FILES[$type]['error'][$id];
                    $_FILES['userfile']['size'] = $_FILES[$type]['size'][$id];
                    if (!$this->upload->do_upload('userfile')) {
                        $error[] = $this->upload->display_errors();
                    } else {
                        $data_file = array(
                            'nama_file' => './doc/' . $id_client . '/' . $id . '/' . $this->upload->data("file_name"),
                            'kode_client' => $id_client,
                            'kode_pemegang_saham' => $id,
                            'kode_pengurus_perusahaan' => NULL,
                            'tipe' => $type
                        );
                        $upload = $this->client->insert_file($data_file);
                        if (!$upload) {
                            $error[] = $this->upload->data("file_name") . ' gagal diupload';
                        }
                    }
                }
            }
        }
        foreach ($id_pengurus_perusahaan as $id) {
            foreach ($type_file_pengurus_perusahaan as $type) {
                if (isset($_FILES[$type]['name'][$id]) && $_FILES[$type]['name'][$id] != "") {
                    if (!file_exists('doc/' . $id_client . '/' . $id)) {
                        mkdir('doc/' . $id_client . '/' . $id, 0777, true);
                    }
                    $config['upload_path'] = './doc/' . $id_client . '/' . $id . '/';
                    $this->upload->initialize($config);
                    $_FILES['userfile']['name'] = $_FILES[$type]['name'][$id];
                    $_FILES['userfile']['type'] = $_FILES[$type]['type'][$id];
                    $_FILES['userfile']['tmp_name'] = $_FILES[$type]['tmp_name'][$id];
                    $_FILES['userfile']['error'] = $_FILES[$type]['error'][$id];
                    $_FILES['userfile']['size'] = $_FILES[$type]['size'][$id];
                    if (!$this->upload->do_upload('userfile')) {
                        $error[] = $this->upload->display_errors();
                    } else {
                        $data_file = array(
                            'nama_file' => './doc/' . $id_client . '/' . $id . '/' . $this->upload->data("file_name"),
                            'kode_client' => $id_client,
                            'kode_pemegang_saham' => NULL,
                            'kode_pengurus_perusahaan' => $id,
                            'tipe' => $type
                        );
                        $upload = $this->client->insert_file($data_file);
                        if (!$upload) {
                            $error[] = $this->upload->data("file_name") . ' gagal diupload';
                        }
                    }
                }
            }
        }
        if (count($error) == 0) {
            $this->session->set_flashdata('msg', build_success_message('Semua dokumen berhasil diupload'));
            redirect('client/dokumen');
        } else {
            $this->session->set_flashdata('msg', build_error_message(implode('<br>', $error)));
            redirect('client/dokumen');
        }
    }

    public function submit_file_domisili() {
        $this->load->library('upload');
        $config['allowed_types'] = 'gif|jpg|png|jpeg|pdf';
        $config['max_size'] = 2048;
        $config['max_width'] = 0;
        $config['max_height'] = 0;
        $id_client = $this->input->post('id_client');
        if (!file_exists('doc/' . $id_client)) {
            mkdir('doc/' . $id_client, 0777, true);
        }
        $config['upload_path'] = './doc/' . $id_client . '/';
        $this->upload->initialize($config);
        $error = array();
        if (isset($_FILES['domisili_gedung']['name']) && $_FILES['domisili_gedung']['name'] != "") {
            if (!$this->upload->do_upload('domisili_gedung')) {
                $error[] = $this->upload->display_errors();
            } else {
                $data_file = array(
                    'nama_file' => './doc/' . $id_client . '/' . $this->upload->data("file_name"),
                    'kode_client' => $id_client,
                    'kode_pemegang_saham' => NULL,
                    'kode_pengurus_perusahaan' => NULL,
                    'tipe' => 'domisili_gedung'
                );
                $upload = $this->client->insert_file($data_file);
                if (!$upload) {
                    $error[] = $this->upload->data("raw_name") . ' gagal diupload';
                }
            }
        }
        if (isset($_FILES['kontrak_virtual_office']['name']) && $_FILES['kontrak_virtual_office']['name'] != "") {
            if (!$this->upload->do_upload('kontrak_virtual_office')) {
                $error[] = $this->upload->display_errors();
            } else {
                $data_file = array(
                    'nama_file' => './doc/' . $id_client . '/' . $this->upload->data("file_name"),
                    'kode_client' => $id_client,
                    'kode_pemegang_saham' => NULL,
                    'kode_pengurus_perusahaan' => NULL,
                    'tipe' => 'kontrak_virtual_office'
                );
                $upload = $this->client->insert_file($data_file);
                if (!$upload) {
                    $error[] = $this->upload->data("raw_name") . ' gagal diupload';
                }
            }
        }
        if (count($error) == 0) {
            $this->session->set_flashdata('msg', build_success_message('Semua dokumen berhasil diupload'));
            redirect('client/dokumen');
        } else {
            $this->session->set_flashdata('msg', build_error_message(implode('<br>', $error)));
            redirect('client/dokumen');
        }
    }
    
    public function submit_file_lain() {
        $this->load->library('upload');
        $config['allowed_types'] = 'gif|jpg|png|jpeg|pdf';
        $config['max_size'] = 2048;
        $config['max_width'] = 0;
        $config['max_height'] = 0;
        $id_client = $this->input->post('id_client');
        if (!file_exists('doc/' . $id_client)) {
            mkdir('doc/' . $id_client, 0777, true);
        }
        $config['upload_path'] = './doc/' . $id_client . '/';
        $this->upload->initialize($config);
        $error = array();
        if (isset($_FILES['setoran_modal']['name']) && $_FILES['setoran_modal']['name'] != "") {
            if (!$this->upload->do_upload('setoran_modal')) {
                $error[] = $this->upload->display_errors();
            } else {
                $data_file = array(
                    'nama_file' => './doc/' . $id_client . '/' . $this->upload->data("file_name"),
                    'kode_client' => $id_client,
                    'kode_pemegang_saham' => NULL,
                    'kode_pengurus_perusahaan' => NULL,
                    'tipe' => 'setoran_modal'
                );
                $upload = $this->client->insert_file($data_file);
                if (!$upload) {
                    $error[] = $this->upload->data("raw_name") . ' gagal diupload';
                }
            }
        }
        if (isset($_FILES['pernyataan_keabsahan']['name']) && $_FILES['pernyataan_keabsahan']['name'] != "") {
            if (!$this->upload->do_upload('pernyataan_keabsahan')) {
                $error[] = $this->upload->display_errors();
            } else {
                $data_file = array(
                    'nama_file' => './doc/' . $id_client . '/' . $this->upload->data("file_name"),
                    'kode_client' => $id_client,
                    'kode_pemegang_saham' => NULL,
                    'kode_pengurus_perusahaan' => NULL,
                    'tipe' => 'pernyataan_keabsahan'
                );
                $upload = $this->client->insert_file($data_file);
                if (!$upload) {
                    $error[] = $this->upload->data("raw_name") . ' gagal diupload';
                }
            }
        }
        if (isset($_FILES['pernyataan_domisili']['name']) && $_FILES['pernyataan_domisili']['name'] != "") {
            if (!$this->upload->do_upload('pernyataan_domisili')) {
                $error[] = $this->upload->display_errors();
            } else {
                $data_file = array(
                    'nama_file' => './doc/' . $id_client . '/' . $this->upload->data("file_name"),
                    'kode_client' => $id_client,
                    'kode_pemegang_saham' => NULL,
                    'kode_pengurus_perusahaan' => NULL,
                    'tipe' => 'pernyataan_domisili'
                );
                $upload = $this->client->insert_file($data_file);
                if (!$upload) {
                    $error[] = $this->upload->data("raw_name") . ' gagal diupload';
                }
            }
        }
        if (isset($_FILES['kuasa_hadap']['name']) && $_FILES['kuasa_hadap']['name'] != "") {
            if (!$this->upload->do_upload('kuasa_hadap')) {
                $error[] = $this->upload->display_errors();
            } else {
                $data_file = array(
                    'nama_file' => './doc/' . $id_client . '/' . $this->upload->data("file_name"),
                    'kode_client' => $id_client,
                    'kode_pemegang_saham' => NULL,
                    'kode_pengurus_perusahaan' => NULL,
                    'tipe' => 'kuasa_hadap'
                );
                $upload = $this->client->insert_file($data_file);
                if (!$upload) {
                    $error[] = $this->upload->data("raw_name") . ' gagal diupload';
                }
            }
        }
        if (count($error) == 0) {
            $this->session->set_flashdata('msg', build_success_message('Semua dokumen berhasil diupload'));
            redirect('client/dokumen');
        } else {
            $this->session->set_flashdata('msg', build_error_message(implode('<br>', $error)));
            redirect('client/dokumen');
        }
    }

}
