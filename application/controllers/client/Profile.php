<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

    public function __construct() {
        parent::__construct();

        if (!$this->session->has_userdata("MavenClient")) {
            redirect('client');
        }
    }

    public function index() {
        $login_data = $this->session->userdata("MavenClient");
        $id_client = $login_data['kode'];
        session_write_close();
        $profile = $this->client->get_data_client($id_client);
        $data = array(
            'title' => 'Profile',
            'page' => 'client/profile',
            'css' => array(),
            'js' => array(),
            'profile' => $profile,
            'login_data' => $login_data
        );
        $this->load->view('templates/client', $data);
    }

    public function submit() {
        $this->form_validation->set_rules('nama_lengkap', 'Nama', 'trim|required', array('trim' => 'Nama harus diisi', 'required' => 'Nama harus diisi'));
        $this->form_validation->set_rules('no_telpon', 'Telepon', 'trim|required', array('trim' => 'Telepon harus diisi', 'required' => 'Telepon harus diisi'));
        $this->form_validation->set_rules('alamat_email', 'Email', 'required|callback__is_unique_edit[client.alamat_email]', array('required' => 'Email harus diisi', '_is_unique_edit' => 'Email sudah terdaftar'));
        if ($this->input->post("password")) {
            $this->form_validation->set_rules('password', 'Password', 'required|min_length[6]', array('required' => 'Password harus diisi', 'min_length' => 'Password minimal 6 karakter'));
            $this->form_validation->set_rules('retype_password', 'Konfirmasi Password', 'required|matches[password]', array('required' => 'Konfirmasi password harus diisi', 'matches' => 'Konfirmasi password tidak sesuai'));
        }

        $this->form_validation->set_error_delimiters('', '<br>');
        $form_data = $this->input->post();
        if ($this->form_validation->run() === FALSE) {
            $this->session->set_flashdata('msg', build_error_message(validation_errors()));
            redirect('client/profile');
        } else {
            if ($form_data['password']) {
                $form_data['password'] = password_hash($form_data['password'], PASSWORD_DEFAULT);
            } else {
                unset($form_data['password']);
            }
            unset($form_data['retype_password']);
            $update = $this->client->update_data_client($form_data);
            if ($update > 0) {
                $data_client = $this->client->check_login($form_data["alamat_email"]);
                $this->session->set_userdata(array("MavenClient" => $data_client));
                $this->session->set_flashdata('msg', build_success_message('Ubah profile berhasil'));
                redirect('client/profile');
            } else {
                $this->session->set_flashdata('msg', build_error_message('Tidak ada data profile yang diubah'));
                redirect('client/profile');
            }
        }
    }

    function _is_unique_edit($value, $field) {
        $login_data = $this->session->userdata("MavenClient");
        $id_client = $login_data['kode'];
        $field = explode(".", $field);
        $data = array($field[1] => $value, "kode !=" => $id_client);
        $data = $this->client->get_exist_data($field[0], $data);
        if ($data < 1) {
            return true;
        } else {
            return false;
        }
    }

}
