<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('Cookie'));
    }

    public function index() {
        $login_data = $this->session->userdata("MavenClient");
        $update_data = array(
            'kode' => $login_data['kode'],
            'cookie' => NULL
        );
        $this->client->update_data_client($update_data);
        delete_cookie('ClientCookie');
        $this->session->sess_destroy();
        redirect("client");
    }

}
