<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Reset extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('security');
        $this->_email_config = $this->config_app->get_email_config();
    }

    public function index() {
        $data = array(
            'title' => 'Reset Password',
            'page' => 'client/reset/reset_form',
            'css' => array(
                'css/pages/client/reset/reset_form'
            ),
            'js' => array(
                'js/pages/client/reset/reset_form'
            )
        );
        $this->load->view('templates/default', $data);
    }
    
    public function token($token_reset) {
        $token = $this->base64url_decode($token_reset);
        $token_valid = false;
        if ($token) {
            $cleanToken = $this->security->xss_clean($token);
            $token_valid = $this->client->is_token_reset_valid($cleanToken);
        }
        if (!$token || !$token_valid) {
            $this->session->set_flashdata('msg', build_error_message('Token tidak valid!'));
            redirect('client/reset');
        }
        $data = array(
            'title' => 'Password Baru',
            'page' => 'client/reset/new_pass',
            'css' => array(
                'css/pages/client/reset/new_pass'
            ),
            'js' => array(
                'js/pages/client/reset/new_pass'
            ),
            'token' => $token_reset
        );
        $this->load->view('templates/default', $data);
    }
    
    public function success() {
        $data = array(
            'title' => 'Email Terkirim',
            'page' => 'client/reset/success',
            'css' => array(
                'css/pages/client/reset/reset_form'
            )
        );
        $this->load->view('templates/default', $data);
    }

    public function check_email() {
        $email = $this->input->post('email');
        if ($email) {
            $result = $this->client->check_login($email);
            if ($result) {
                $date = date('Y-m-d H:i', strtotime('+2 hour', strtotime(date('Y-m-d H:i'))));
                $token = substr(sha1(rand() . $date . $result['kode']), 0, 30);
                $data = array(
                    'token_reset' => $token,
                    'kode' => $result['kode'],
                    'limit_waktu_reset' => $date
                );
                $set_token = $this->client->update_data_client($data);
                if ($set_token) {
                    $this->load->library('email');
                    if (isset($this->_email_config->email_protocol) && $this->_email_config->email_protocol && isset($this->_email_config->email_smtp_host) && $this->_email_config->email_smtp_host && isset($this->_email_config->email_smtp_port) && $this->_email_config->email_smtp_port && isset($this->_email_config->email_smtp_user) && $this->_email_config->email_smtp_user && isset($this->_email_config->email_smtp_pass) && $this->_email_config->email_smtp_pass) {
                        $config = array(
                            'protocol' => $this->_email_config->email_protocol,
                            'smtp_host' => $this->_email_config->email_smtp_host,
                            'smtp_port' => $this->_email_config->email_smtp_port,
                            'smtp_user' => $this->_email_config->email_smtp_user,
                            'smtp_pass' => $this->_email_config->email_smtp_pass,
                            'mailtype' => 'html',
                            'charset' => 'utf-8'
                        );
                        $this->email->initialize($config);
                    }
                    $msg = '<html>
                                <body>
                                    <table align="left" width="100%" style="border-collapse:collapse;border-spacing:0;margin:0;padding:0">
                                        <tbody>
                                            <tr>
                                                <td style="border-collapse:collapse;border-spacing:0;font-family:\'Arial\',sans-serif;line-height:1.5;margin:0;padding:0">
                                                    <p style="font-family:\'Arial\',sans-serif;font-size:14px;line-height:1.5;margin:0;padding:0">Halo ' . $result['nama_lengkap'] . ',</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="20" style="border-collapse:collapse;border-spacing:0;font-family:\'Arial\',sans-serif;line-height:1.5;margin:0;padding:0"></td>
                                            </tr>
                                            <tr>
                                                <td style="border-collapse:collapse;border-spacing:0;font-family:\'Arial\',sans-serif;line-height:1.5;margin:0;padding:0">
                                                    <p style="font-family:\'Arial\',sans-serif;font-size:14px;line-height:1.5;margin:0;padding:0">Kami telah menerima permintaan Anda untuk melakukan reset password. Silahkan klik tombol berikut untuk melanjutkan.</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="20" style="border-collapse:collapse;border-spacing:0;font-family:\'Arial\',sans-serif;line-height:1.5;margin:0;padding:0"></td>
                                            </tr>
                                            <tr>
                                                <td style="border-collapse:collapse;border-spacing:0;font-family:\'Arial\',sans-serif;line-height:1.5;margin:0;padding:0;text-align:center" align="center">
                                                    <a href="' . site_url('client/reset/token/' . $this->base64url_encode($token . $result['kode'])) . '" style="background:#2C374B;border:1px solid #2C374B;border-radius:10px;box-sizing:border-box;color:#ffffff!important;display:inline-block;font-family:\'Arial\',sans-serif;font-size:13px;font-weight:bold;line-height:1.5;margin:0;padding:12px 7px;text-align:center;text-decoration:none;vertical-align:middle;width:75%;" target="_blank">
                                                        RESET PASSWORD
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="20" style="border-collapse:collapse;border-spacing:0;font-family:\'Arial\',sans-serif;line-height:1.5;margin:0;padding:0"></td>
                                            </tr>
                                            <tr>
                                                <td style="border-collapse:collapse;border-spacing:0;font-family:\'Arial\',sans-serif;line-height:1.5;margin:0;padding:0">
                                                    <p style="font-family:\'Arial\',sans-serif;font-size:14px;line-height:1.5;margin:0;padding:0">Mohon abaikan e-mail ini jika Anda merasa tidak pernah meminta untuk melakukan reset password.</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="20" style="border-collapse:collapse;border-spacing:0;font-family:\'Arial\',sans-serif;line-height:1.5;margin:0;padding:0"></td>
                                            </tr>
                                            <tr>
                                                <td style="border-collapse:collapse;border-spacing:0;font-family:\'Arial\',sans-serif;line-height:1.5;margin:0;padding:0">
                                                    <p style="font-family:\'Arial\',sans-serif;font-size:14px;line-height:1.5;margin:0;padding:0">Terima Kasih.</p>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </body>
                            </html>';
                    $this->email->from('no-reply@maven.id', 'Registrasi Maven');
                    $this->email->to($result['alamat_email']);
                    $this->email->subject('Informasi Reset Password');
                    $this->email->set_mailtype("html");
                    $this->email->message($msg);
                    $this->email->send();
                    echo "sukses";
                } else {
                    $this->session->set_flashdata('msg', build_error_message('Terjadi kesalahan. Coba lagi!'));
                    echo "error";
                }
            } else {
                $this->session->set_flashdata('msg', build_error_message('Email tidak ditemukan!'));
                echo "invalid";
            }
        } else {
            $this->session->set_flashdata('msg', build_error_message('Email harus diisi!'));
            echo "invalid";
        }
    }

    public function submit() {
        $this->form_validation->set_rules('password', 'Password', 'required|min_length[6]', array('required' => 'Password harus diisi', 'min_length' => 'Password minimal 6 karakter'));
        $this->form_validation->set_rules('retype_password', 'Konfirmasi Password', 'required|matches[password]', array('required' => 'Password harus diisi', 'matches' => 'Konfirmasi Password tidak sesuai'));
        $this->form_validation->set_rules('token', 'Token', 'required', array('required' => 'Token tidak valid'));
        $this->form_validation->set_error_delimiters('', '<br>');
        $token_reset = $this->base64url_decode($this->input->post('token'));
        $password = $this->input->post('password');
        if ($this->form_validation->run() === FALSE) {
            $this->session->set_flashdata('msg', build_error_message(validation_errors()));
            echo "gagal";
        } else {
            $cleanToken = $this->security->xss_clean($token_reset);
            $token_valid = $this->client->is_token_reset_valid($cleanToken);
            if ($token_valid) {
                $kode = substr($token_reset, 30);
                $form_update = array(
                    'kode' => $kode,
                    'token_reset' => NULL,
                    'limit_waktu_reset' => NULL,
                    'password' => password_hash($password, PASSWORD_DEFAULT)
                );
                $update = $this->client->update_data_client($form_update);
                if ($update > 0) {
                    $this->session->set_flashdata('msg', build_success_message('Password anda sudah diperbaharui. Silahkan login'));
                    echo "sukses";
                } else {
                    $this->session->set_flashdata('msg', build_error_message('Password gagal diperbaharui'));
                    echo "gagal";
                }
            } else {
                $this->session->set_flashdata('msg', build_error_message('Token tidak valid!'));
                echo "invalid";
            }
        }
    }

    public function base64url_encode($data) {
        return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
    }

    public function base64url_decode($data) {
        return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
    }

}
