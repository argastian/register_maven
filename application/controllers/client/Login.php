<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper(array('Cookie', 'String'));
        if ($this->session->has_userdata("MavenClient")) {
            redirect('client/home');
        }
    }

    public function index() {
        $cookie = get_cookie('ClientCookie');
        if ($cookie <> '') {
            $row = $this->client->get_by_cookie($cookie);
            if ($row) {
                $key = random_string('alnum', 64);
                set_cookie('ClientCookie', $key, 3600 * 24 * 14);
                $update_data = array(
                    'kode' => $row['kode'],
                    'cookie' => $key
                );
                $this->client->update_data_client($update_data);
                $this->session->set_userdata(array("MavenClient" => $row));
                redirect('client/home');
            } else {
                $data = array(
                    'title' => 'Login',
                    'page' => 'client/login',
                    'css' => array(
                        'css/pages/client/login'
                    ),
                    'js' => array(
                        'js/pages/client/login'
                    )
                );
                $this->load->view('templates/default', $data);
            }
        } else {
            $data = array(
                'title' => 'Login',
                'page' => 'client/login',
                'css' => array(
                    'css/pages/client/login'
                ),
                'js' => array(
                    'js/pages/client/login'
                )
            );
            $this->load->view('templates/default', $data);
        }
    }

    public function authentication() {
        $this->form_validation->set_rules('email', 'Email', 'trim|required', array('trim' => 'Email harus diisi', 'required' => 'Email harus diisi'));
        $this->form_validation->set_rules('password', 'Password', 'trim|required', array('trim' => 'Password harus diisi', 'required' => 'Password harus diisi'));
        $this->form_validation->set_error_delimiters('', '<br>');
        $form_data = $this->input->post();
        if ($this->form_validation->run() === FALSE) {
            $this->session->set_flashdata('msg', build_error_message(validation_errors()));
        } else {
            $check = $this->client->check_login($this->input->post("email"));
            if ($check) {
                if (password_verify($this->input->post("password"), $check['password'])) {
                    if ($this->input->post("ingat")) {
                        $key = random_string('alnum', 64);
                        set_cookie('ClientCookie', $key, 3600 * 24 * 14);
                        $update_data = array(
                            'kode' => $check['kode'],
                            'cookie' => $key
                        );
                        $this->client->update_data_client($update_data);
                    }
                    $this->session->set_userdata(array("MavenClient" => $check));
                    echo "valid";
                } else {
                    $this->session->set_flashdata('msg', build_error_message('Email atau password salah!'));
                    echo "invalid";
                }
            } else {
                $this->session->set_flashdata('msg', build_error_message('Email atau password salah!'));
                echo "invalid";
            }
        }
    }

}
