<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Faq extends CI_Controller {

    public function __construct() {
        parent::__construct();

        if (!$this->session->has_userdata("loginMaven")) {
            redirect('administrator');
        }
    }

    public function index() {
        $login_data = $this->session->userdata("loginMaven");
        $faq = $this->faq->get_faq();
        $data = array(
            'title' => 'FAQ',
            'page' => 'administrator/faq/list',
            'css' => array(
                'includes/datatables/datatables',
                'includes/datatables/css/dataTables.bootstrap',
                'css/pages/administrator/faq/list'
            ),
            'js' => array(
                'includes/datatables/datatables',
                'includes/datatables/js/dataTables.bootstrap',
                'js/pages/administrator/faq/list'
            ),
            'faq' => $faq,
            'login_data' => $login_data
        );
        $this->load->view('templates/administrator', $data);
    }

    public function kelola($id_faq = NULL) {
        $login_data = $this->session->userdata("loginMaven");
        $faq = NULL;
        $title = 'Tambah FAQ';
        if ($id_faq) {
            $title = 'Ubah FAQ';
            $faq = $this->faq->get_faq($id_faq);
        }
        $data = array(
            'title' => $title,
            'page' => 'administrator/faq/kelola',
            'css' => array(
                'css/pages/administrator/faq/kelola'
            ),
            'js' => array(),
            'faq' => $faq,
            'login_data' => $login_data
        );
        $this->load->view('templates/administrator', $data);
    }

    public function submit() {
        $this->form_validation->set_rules('tanya', 'Pertanyaan', 'trim|required', array('trim' => 'Pertanyaan harus diisi', 'required' => 'Pertanyaan harus diisi'));
        $this->form_validation->set_rules('jawab', 'Jawaban', 'trim|required', array('trim' => 'Jawaban harus diisi', 'required' => 'Jawaban harus diisi'));
        $this->form_validation->set_error_delimiters('', '<br>');
        $form_data = $this->input->post();
        $redirest_url = ($form_data['id_faq']) ? 'administrator/kelola-faq/' . $form_data['id_faq'] : 'administrator/tambah-faq';
        if ($this->form_validation->run() === FALSE) {
            $this->session->set_flashdata('msg', build_error_message(validation_errors()));
            redirect($redirest_url);
        } else {
            if ($form_data['id_faq']) {
                $update = $this->faq->update_faq($form_data);
                if ($update > 0) {
                    $this->session->set_flashdata('msg', build_success_message('Ubah FAQ berhasil'));
                    redirect('administrator/faq');
                } else {
                    $error = $this->db->error();
                    $this->session->set_flashdata('msg', build_error_message((isset($error['message']) && $error['message']) ? $error['message'] : 'Tidak ada data yang berubah!'));
                    redirect($redirest_url);
                }
            } else {
                unset($form_data['id_faq']);
                $insert = $this->faq->insert_faq($form_data);
                if ($insert) {
                    $this->session->set_flashdata('msg', build_success_message('Tambah FAQ berhasil'));
                    redirect('administrator/faq');
                } else {
                    $error = $this->db->error();
                    $this->session->set_flashdata('msg', build_error_message((isset($error['message']) && $error['message']) ? $error['message'] : 'Tambah FAQ gagal!'));
                    redirect($redirest_url);
                }
            }
        }
    }

    public function delete() {
        $id_faq = $this->input->post('id_faq');
        if ($id_faq) {
            $delete = $this->faq->delete_faq($id_faq);
            if($delete == 'success') {
                $this->session->set_flashdata('msg', build_success_message('Item FAQ berhasil dihapus'));
            } else {
                $this->session->set_flashdata('msg', build_error_message('Item FAQ gagal dihapus!'));
            }
            echo $delete;
        } else {
            $this->session->set_flashdata('msg', build_error_message('Item FAQ tidak ditemukan!'));
            echo "kosong";
        }
    }

}
