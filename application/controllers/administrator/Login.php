<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper(array('Cookie', 'String'));
        if ($this->session->has_userdata("loginMaven")) {
            redirect('administrator/home');
        }
    }

    public function index() {
        $cookie = get_cookie('MavenCookie');
        if ($cookie <> '') {
            $row = $this->admin->get_by_cookie($cookie);
            if ($row) {
                $key = random_string('alnum', 64);
                set_cookie('MavenCookie', $key, 3600 * 24 * 14);
                $update_data = array(
                    'kode' => $row['kode'],
                    'cookie' => $key
                );
                $this->client->update_data_client($update_data);
                $this->session->set_userdata(array("MavenClient" => $row));
                redirect('administrator/home');
            } else {
                $data = array(
                    'title' => 'Login',
                    'page' => 'administrator/login',
                    'css' => array(
                        'css/pages/administrator/login'
                    ),
                    'js' => array(
                        'js/pages/administrator/login'
                    )
                );
                $this->load->view('templates/default', $data);
            }
        } else {
            $data = array(
                'title' => 'Login',
                'page' => 'administrator/login',
                'css' => array(
                    'css/pages/administrator/login'
                ),
                'js' => array(
                    'js/pages/administrator/login'
                )
            );
            $this->load->view('templates/default', $data);
        }
    }

    public function authentication() {
        $this->form_validation->set_rules('username', 'Username', 'trim|required', array('trim' => 'Username harus diisi', 'required' => 'Username harus diisi'));
        $this->form_validation->set_rules('password', 'Password', 'trim|required', array('trim' => 'Password harus diisi', 'required' => 'Password harus diisi'));
        $this->form_validation->set_error_delimiters('', '<br>');
        $form_data = $this->input->post();
        if ($this->form_validation->run() === FALSE) {
            $this->session->set_flashdata('msg', build_error_message(validation_errors()));
        } else {
            $check = $this->admin->check_login($this->input->post("username"));
            if ($check) {
                if (password_verify($this->input->post("password"), $check['password'])) {
                    if ($this->input->post("ingat")) {
                        $key = random_string('alnum', 64);
                        set_cookie('ClientCookie', $key, 3600 * 24 * 14);
                        $update_data = array(
                            'kode' => $check['kode'],
                            'cookie' => $key
                        );
                        $this->admin->update_admin($update_data);
                    }
                    $this->session->set_userdata(array("loginMaven" => $check));
                    echo "valid";
                } else {
                    $this->session->set_flashdata('msg', build_error_message('Username atau password salah!'));
                    echo "invalid";
                }
            } else {
                $this->session->set_flashdata('msg', build_error_message('Username atau password salah!'));
                echo "invalid";
            }
        }
    }

}
