<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->has_userdata("loginMaven")) {
            redirect('administrator');
        }
    }

    public function index() {
        $login_data = $this->session->userdata("loginMaven");
        $clients = $this->client->get_all_data_client();
        $data = array(
            'title' => 'Client',
            'page' => 'administrator/client/list',
            'css' => array(
                'includes/datatables/datatables',
                'includes/datatables/css/dataTables.bootstrap'
            ),
            'js' => array(
                'includes/datatables/datatables',
                'includes/datatables/js/dataTables.bootstrap',
                'js/pages/administrator/home'
            ),
            'login_data' => $login_data,
            'clients' => $clients
        );
        $this->load->view('templates/administrator', $data);
    }

    public function detail($kode) {
        if (ctype_alnum($kode)) {
            $login_data = $this->session->userdata("loginMaven");
            $client = $this->client->get_data_client($kode);
            $perusahaan = isset($client->kode) ? $this->client->get_data_perusahaan($client->kode) : '';
            $provinsi = isset($perusahaan->provinsi) ? $this->wilayah->get_wilayah_by_id($perusahaan->provinsi) : '';
            $kota_madya = isset($perusahaan->kota_madya) ? $this->wilayah->get_wilayah_by_id($perusahaan->kota_madya) : '';
            $kecamatan = isset($perusahaan->kecamatan) ? $this->wilayah->get_wilayah_by_id($perusahaan->kecamatan) : '';
            $kelurahan = isset($perusahaan->kelurahan) ? $this->wilayah->get_wilayah_by_id($perusahaan->kelurahan) : '';
            $file_dokumen = $this->client->get_data_file($client->kode);
            $dokumen_pemegang_saham = array();
            $dokumen_pengurus_perusahaan = array();
            $dokumen_lain = array();
            foreach ($file_dokumen as $file) {
                if ($file->kode_pemegang_saham) {
                    $dokumen_pemegang_saham[$file->kode_pemegang_saham][$file->tipe] = str_replace("./doc/", "doc/", $file->nama_file);
                } else if ($file->kode_pengurus_perusahaan) {
                    $dokumen_pengurus_perusahaan[$file->kode_pengurus_perusahaan][$file->tipe] = str_replace("./doc/", "doc/", $file->nama_file);
                } else {
                    $dokumen_lain[$file->tipe] = str_replace("./doc/", "doc/", $file->nama_file);
                }
            }
            $jumlah_saham = '';
            if (isset($perusahaan->modal_disetor) && isset($perusahaan->nominal_saham)) {
                if ($perusahaan->modal_disetor != "" && $perusahaan->modal_disetor != NULL && $perusahaan->nominal_saham != "" && $perusahaan->nominal_saham != NULL && $perusahaan->nominal_saham > 0) {
                    $jumlah_saham = round($perusahaan->modal_disetor / $perusahaan->nominal_saham);
                }
            }
            $saham_perseorangan = '';
            $file_saham_perseorangan = '';
            $saham_perusahaan = '';
            $pengurus = '';
            $file_pengurus = '';
            if (isset($perusahaan->kode)) {
                $no = 1;
                $pemegang_saham_perseorangan = $this->client->get_data_pemegang_saham_perseorangan($perusahaan->kode);
                if ($pemegang_saham_perseorangan) {
                    foreach ($pemegang_saham_perseorangan as $key => $item) {
                        $saham_perseorangan .= '<table border="1" cellpadding="7" cellspacing="0">
                        <tbody>
                            <tr>
                                <td colspan="2" align="center"><strong>Pemegang Saham ' . $no . '</strong></td>
                            </tr>
                            <tr>
                                <td><strong>Jenis Pemegang Saham</strong></td>
                                <td>Perseorangan</td>
                            </tr>
                            <tr>
                                <td><strong>Nama Lengkap</strong></td>
                                <td>' . html_escape($item->nama) . '</td>
                            </tr>
                            <tr>
                                <td><strong>Alamat</strong></td>
                                <td>' . html_escape($item->alamat) . '</td>
                            </tr>
                            <tr>
                                <td><strong>No KTP</strong></td>
                                <td>' . html_escape($item->no_ktp) . '</td>
                            </tr>
                            <tr>
                                <td><strong>No NPWP</strong></td>
                                <td>' . html_escape($item->no_npwp) . '</td>
                            </tr>
                            <tr>
                                <td><strong>Jumlah Persentase Saham</strong></td>
                                <td>' . html_escape($item->persentase_saham) . '%</td>
                            </tr>
                        </tbody>
                    </table>';
                        $file_saham_perseorangan .= '<table border="1" cellpadding="7" cellspacing="0">
                        <tbody>
                            <tr>
                                <td colspan="2" align="center"><strong>Pemegang Saham : ' . html_escape($item->nama) . '</strong></td>
                            </tr>
                            <tr>
                                <td><strong>KTP</strong></td>
                                <td class="text-center">' . (isset($dokumen_pemegang_saham[$item->kode]['ktp_pemegang_saham']) ? '<a href="' . base_url($dokumen_pemegang_saham[$item->kode]['ktp_pemegang_saham']) . '" target="_BLANK" class="btn btn-info btn-xs">DOWNLOAD</a>' : '') . '</td>
                            </tr>
                            <tr>
                                <td><strong>NPWP</strong></td>
                                <td class="text-center">' . (isset($dokumen_pemegang_saham[$item->kode]['npwp_pemegang_saham']) ? '<a href="' . base_url($dokumen_pemegang_saham[$item->kode]['npwp_pemegang_saham']) . '" target="_BLANK" class="btn btn-info btn-xs">DOWNLOAD</a>' : '') . '</td>
                            </tr>
                            <tr>
                                <td><strong>Kartu Keluarga</strong></td>
                                <td class="text-center">' . (isset($dokumen_pemegang_saham[$item->kode]['kk_pemegang_saham']) ? '<a href="' . base_url($dokumen_pemegang_saham[$item->kode]['kk_pemegang_saham']) . '" target="_BLANK" class="btn btn-info btn-xs">DOWNLOAD</a>' : '') . '</td>
                            </tr>
                        </tbody>
                    </table>';
                        $no++;
                    }
                }
                $pemegang_saham_perusahaan = $this->client->get_data_pemegang_saham_perusahaan($perusahaan->kode);
                if ($pemegang_saham_perusahaan) {
                    foreach ($pemegang_saham_perusahaan as $key => $item) {
                        $saham_perusahaan .= '<table border="1" cellpadding="7" cellspacing="0">
                        <tbody>
                            <tr>
                                <td colspan="2" align="center"><strong>Pemegang Saham ' . $no . '</strong></td>
                            </tr>
                            <tr>
                                <td><strong>Jenis Pemegang Saham</strong></td>
                                <td>Perusahaan</td>
                            </tr>
                            <tr>
                                <td><strong>Nama PT</strong></td>
                                <td>' . html_escape($item->nama) . '</td>
                            </tr>
                            <tr>
                                <td><strong>No Akta Pendirian</strong></td>
                                <td>' . html_escape($item->no_akta) . '</td>
                            </tr>
                            <tr>
                                <td><strong>Tanggal Pendirian</strong></td>
                                <td>' . html_escape($item->tanggal_pendirian) . '</td>
                            </tr>
                            <tr>
                                <td><strong>No SK Menkumham</strong></td>
                                <td>' . html_escape($item->no_sk) . '</td>
                            </tr>
                            <tr>
                                <td><strong>No NPWP Perusahaan</strong></td>
                                <td>' . html_escape($item->no_npwp) . '</td>
                            </tr>
                            <tr>
                                <td><strong>Jumlah Persentase Saham</strong></td>
                                <td>' . html_escape($item->persentase_saham) . '%</td>
                            </tr>
                        </tbody>
                    </table>';
                        $no++;
                    }
                }
                $no = 1;
                $pengurus_perusahaan = $this->client->get_data_pengurus_perusahaan($perusahaan->kode);
                if ($pengurus_perusahaan) {
                    foreach ($pengurus_perusahaan as $key => $item) {
                        $pengurus .= '<table border="1" cellpadding="7" cellspacing="0">
                        <tbody>
                            <tr>
                                <td colspan="2" align="center"><strong>Pengurus Perusahaan ' . $no . '</strong></td>
                            </tr>
                            <tr>
                                <td><strong>Jenis Jabatan</strong></td>
                                <td>Dewan ' . ucfirst(html_escape($item->jenis_jabatan)) . '</td>
                            </tr>
                            <tr>
                                <td><strong>Nama Jabatan</strong></td>
                                <td>' . html_escape($item->nama_jabatan) . '</td>
                            </tr>
                            <tr>
                                <td><strong>Nama Pengurus</strong></td>
                                <td>' . html_escape($item->nama_pengurus) . '</td>
                            </tr>
                            <tr>
                                <td><strong>Alamat</strong></td>
                                <td>' . html_escape($item->alamat) . '</td>
                            </tr>
                            <tr>
                                <td><strong>KTP</strong></td>
                                <td>' . html_escape($item->no_ktp) . '</td>
                            </tr>
                            <tr>
                                <td><strong>NPWP</strong></td>
                                <td>' . html_escape($item->no_npwp) . '</td>
                            </tr>
                        </tbody>
                    </table>';
                        $file_pengurus .= '<table border="1" cellpadding="7" cellspacing="0">
                        <tbody>
                            <tr>
                                <td colspan="2" align="center"><strong>Pengurus Perusahaan : ' . html_escape($item->nama_pengurus) . '</strong></td>
                            </tr>
                            <tr>
                                <td><strong>KTP</strong></td>
                                <td class="text-center">' . (isset($dokumen_pengurus_perusahaan[$item->kode]['ktp_pengurus_perusahaan']) ? '<a href="' . base_url($dokumen_pengurus_perusahaan[$item->kode]['ktp_pengurus_perusahaan']) . '" target="_BLANK" class="btn btn-info btn-xs">DOWNLOAD</a>' : '') . '</td>
                            </tr>
                            <tr>
                                <td><strong>NPWP</strong></td>
                                <td class="text-center">' . (isset($dokumen_pengurus_perusahaan[$item->kode]['npwp_pengurus_perusahaan']) ? '<a href="' . base_url($dokumen_pengurus_perusahaan[$item->kode]['npwp_pengurus_perusahaan']) . '" target="_BLANK" class="btn btn-info btn-xs">DOWNLOAD</a>' : '') . '</td>
                            </tr>
                            <tr>
                                <td><strong>Kartu Keluarga</strong></td>
                                <td class="text-center">' . (isset($dokumen_pengurus_perusahaan[$item->kode]['kk_pengurus_perusahaan']) ? '<a href="' . base_url($dokumen_pengurus_perusahaan[$item->kode]['kk_pengurus_perusahaan']) . '" target="_BLANK" class="btn btn-info btn-xs">DOWNLOAD</a>' : '') . '</td>
                            </tr>
                        </tbody>
                    </table>';
                        $no++;
                    }
                }
            }
            $detail_client = '<table class="table-profil" cellpadding="7" cellspacing="0">
                    <tbody>
                        <tr>
                            <td><strong>Nama Lengkap</strong></td>
                            <td id="col_nama_lengkap">' . (isset($client->nama_lengkap) ? html_escape($client->nama_lengkap) : '') . '</td>
                        </tr>
                        <tr>
                            <td><strong>Alamat Email</strong></td>
                            <td id="col_alamat_email">' . (isset($client->alamat_email) ? html_escape($client->alamat_email) : '') . '</td>
                        </tr>
                        <tr>
                            <td><strong>No Telpon</strong></td>
                            <td id="col_no_telpon">' . (isset($client->no_telpon) ? html_escape($client->no_telpon) : '') . '</td>
                        </tr>
                        <tr>
                            <td><strong>Waktu Registrasi</strong></td>
                            <td id="col_no_telpon">' . display_date_time($perusahaan->waktu_buat) . '</td>
                        </tr>
                        <tr>
                            <td><strong>Terakhir Diperbaharui</strong></td>
                            <td id="col_no_telpon">' . ((isset($perusahaan->waktu_ubah) && $perusahaan->waktu_ubah) ? display_date_time($perusahaan->waktu_ubah) : display_date_time($client->waktu_buat)) . '</td>
                        </tr>
                    </tbody>
                </table>
                <div>

                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#data-perusahaan" aria-controls="data-perusahaan" role="tab" data-toggle="tab">Data Perusahaan</a></li>
                        <li role="presentation"><a href="#download-data" aria-controls="download-data" role="tab" data-toggle="tab">Download</a></li>
                        <li role="presentation"><a href="#progress-status" aria-controls="download-data" role="tab" data-toggle="tab">Progress</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="data-perusahaan">
                            <div id="container-detail-client">
                                <h4>Nama &amp; Tujuan Perusahaan</h4>
                                <table border="1" cellpadding="7" cellspacing="0">        
                                    <tbody><tr>
                                            <td><strong>Nama 1</strong></td>
                                            <td id="col_nama_perusahaan_1">' . (isset($perusahaan->nama_perusahaan_1) ? html_escape($perusahaan->nama_perusahaan_1) : '') . '</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Nama 2</strong></td>
                                            <td id="col_nama_perusahaan_2">' . (isset($perusahaan->nama_perusahaan_1) ? html_escape($perusahaan->nama_perusahaan_2) : '') . '</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Nama 3</strong></td>
                                            <td id="col_nama_perusahaan_3">' . (isset($perusahaan->nama_perusahaan_1) ? html_escape($perusahaan->nama_perusahaan_3) : '') . '</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Tujuan Perusahaan</strong></td>
                                            <td id="col_tujuan_pendirian">' . (isset($perusahaan->tujuan_pendirian) ? html_escape($perusahaan->tujuan_pendirian) : '') . '</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Bidang Usaha</strong></td>
                                            <td id="col_bidang_usaha">' . (isset($perusahaan->bidang) ? html_escape($perusahaan->bidang) : '') . '</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <h4>Alamat Domisili</h4>
                                <table border="1" cellpadding="7" cellspacing="0">
                                    <tbody>
                                        <tr>
                                            <td colspan="2" align="center"><strong>' . (isset($perusahaan->kepemilikan) ? html_escape(ucwords(str_replace("_", " ", $perusahaan->kepemilikan))) : '&nbsp;') . '</strong></td>
                                        </tr>
                                        <tr>
                                            <td><strong>Alamat Domisili</strong></td>
                                            <td id="col_alamat_domisili_vo">' . (isset($perusahaan->alamat_domisili) ? html_escape($perusahaan->alamat_domisili) : '') . '</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Nama Gedung</strong></td>
                                            <td id="col_nama_gedung_vo">' . (isset($perusahaan->nama_gedung) ? html_escape($perusahaan->nama_gedung) : '') . '</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Provinsi</strong></td>
                                            <td id="col_provinsi_vo">' . (isset($provinsi->nama) ? format_nama_wilayah(html_escape($provinsi->nama)) : '') . '</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Kota Madya</strong></td>
                                            <td id="col_kota_madya_vo">' . (isset($kota_madya->nama) ? format_nama_wilayah(html_escape($kota_madya->nama)) : '') . '</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Kecamatan</strong></td>
                                            <td id="col_kecamatan_vo">' . (isset($kecamatan->nama) ? format_nama_wilayah(html_escape($kecamatan->nama)) : '') . '</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Kelurahan</strong></td>
                                            <td id="col_kelurahan_vo">' . (isset($kelurahan->nama) ? format_nama_wilayah(html_escape($kelurahan->nama)) : '') . '</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <h4>Modal</h4>
                                <table border="1" cellpadding="7" cellspacing="0">
                                    <tbody>
                                        <tr>
                                            <td><strong>Modal Dasar</strong></td>
                                            <td id="col_modal_dasar" class="text-right">Rp ' . (isset($perusahaan->modal_dasar) ? display_number($perusahaan->modal_dasar) : '0') . '</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Modal Disetor &amp; Ditempatkan</strong></td>
                                            <td id="col_modal_disetor" class="text-right">Rp ' . (isset($perusahaan->modal_disetor) ? display_number($perusahaan->modal_disetor) : '0') . '</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Nominal Saham/Lembar</strong></td>
                                            <td id="col_nominal_saham" class="text-right">Rp ' . (isset($perusahaan->nominal_saham) ? display_number($perusahaan->nominal_saham) : '0') . '</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Jumlah Saham</strong></td>
                                            <td id="col_jumlah_saham" class="text-right">' . $jumlah_saham . '</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <h4>Pemegang Saham</h4>
                                <div id="table-pemegang-saham">
                                ' . $saham_perseorangan . $saham_perusahaan . '
                                </div>
                                <h4>Pengurus Perusahaan</h4>
                                <div id="table-pengurus">
                                ' . $pengurus . '
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="download-data">
                            <div id="container-download">
                                ' . $file_saham_perseorangan . $file_pengurus . '
                                <table border="1" cellpadding="7" cellspacing="0">
                                    <tbody>
                                        <tr>
                                            <td colspan="2" align="center"><strong>Dokumen Lain</strong></td>
                                        </tr>
                                        <tr>
                                            <td><strong>Surat Domisili Gedung</strong></td>
                                            <td class="text-center">' . (isset($dokumen_lain['domisili_gedung']) ? '<a href="' . base_url($dokumen_lain['domisili_gedung']) . '" target="_BLANK" class="btn btn-info btn-xs">DOWNLOAD</a>' : '') . '</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Kontrak Virtual Office</strong></td>
                                            <td class="text-center">' . (isset($dokumen_lain['kontrak_virtual_office']) ? '<a href="' . base_url($dokumen_lain['kontrak_virtual_office']) . '" target="_BLANK" class="btn btn-info btn-xs">DOWNLOAD</a>' : '') . '</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Surat Pernyataan Setoran Modal</strong></td>
                                            <td class="text-center">' . (isset($dokumen_lain['setoran_modal']) ? '<a href="' . base_url($dokumen_lain['setoran_modal']) . '" target="_BLANK" class="btn btn-info btn-xs">DOWNLOAD</a>' : '') . '</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Pernyataan Keabsahan</strong></td>
                                            <td class="text-center">' . (isset($dokumen_lain['pernyataan_keabsahan']) ? '<a href="' . base_url($dokumen_lain['pernyataan_keabsahan']) . '" target="_BLANK" class="btn btn-info btn-xs">DOWNLOAD</a>' : '') . '</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Pernyataan Domisili</strong></td>
                                            <td class="text-center">' . (isset($dokumen_lain['pernyataan_domisili']) ? '<a href="' . base_url($dokumen_lain['pernyataan_domisili']) . '" target="_BLANK" class="btn btn-info btn-xs">DOWNLOAD</a>' : '') . '</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Kuasa Hadap (opsional)</strong></td>
                                            <td class="text-center">' . (isset($dokumen_lain['kuasa_hadap']) ? '<a href="' . base_url($dokumen_lain['kuasa_hadap']) . '" target="_BLANK" class="btn btn-info btn-xs">DOWNLOAD</a>' : '') . '</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>    
                        </div>
                        <div role="tabpanel" class="tab-pane" id="progress-status">
                            ' . form_open(site_url('administrator/home/submit_progress'), array('class' => 'form-horizontal')) . '
                            <input type="hidden" name="kode" value="' . (isset($perusahaan->kode) ? $perusahaan->kode : "") . '">
                            <input type="hidden" name="client" value="' . $client->kode . '">
                            <div class="col-md-24 text-center">
                                <h2><span id="persentase_progress">' . (isset($perusahaan->status) ? $perusahaan->status : '0') . '</span>% Selesai</h2>
                            </div>
                            <br>
                            <div class="form-group">
                                <div class="col-md-24">
                                    <input type="range" min="0" max="100" value="' . (isset($perusahaan->status) ? $perusahaan->status : '0') . '" step="25" class="slider" id="range_progress" name="status">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-24">
                                    <button type="submit" class="btn btn-primary pull-right">SIMPAN</button>
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>';
            $data = array(
                'title' => 'Detail Client',
                'page' => 'administrator/client/detail',
                'css' => array(
                    'css/pages/administrator/client/detail'
                ),
                'js' => array(
                    'js/pages/administrator/client/detail'
                ),
                'login_data' => $login_data,
                'detail_client' => $detail_client
            );
            $this->load->view('templates/administrator', $data);
        } else {
            $this->session->set_flashdata('msg', build_warning_message('Invalid ID'));
            redirect('administrator/home');
        }
    }

    public function submit_progress() {
        $params = $this->input->post();
        if ($params) {
            $data_perusahaan = array(
                'kode' => $params['kode'],
                'status' => $params['status'],
                'waktu_ubah' => date('Y-m-d H:i:s')
            );
            $update = $this->client->update_data_perusahaan($data_perusahaan);
            if ($update > 0) {
                $this->session->set_flashdata('msg', build_success_message('Ubah progress berhasil'));
                redirect('administrator/detail-client/' . $params['client']);
            } else {
                $this->session->set_flashdata('msg', build_error_message('Progress tidak berubah'));
                redirect('administrator/detail-client/' . $params['client']);
            }
        }
    }

}
