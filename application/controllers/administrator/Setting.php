<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends CI_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->has_userdata("loginMaven")) {
            redirect('administrator');
        }
    }

    public function index($tab = 1) {
        $login_data = $this->session->userdata("loginMaven");
        $config_app = $this->config_app->get_config();
        $config_mail = $this->config_app->get_email_config();
        $data = array(
            'title' => 'Login',
            'page' => 'administrator/setting',
            'css' => array(
                'css/pages/administrator/setting'
            ),
            'js' => array(),
            'config_app' => $config_app,
            'config_mail' => $config_mail,
            'login_data' => $login_data,
            'tab' => $tab
        );
        $this->load->view('templates/administrator', $data);
    }

    public function submit_reg() {
        $this->form_validation->set_rules('api_key_detect_language', 'API Key Detect Language', 'trim|required', array('trim' => 'API Key Detect Language harus diisi', 'required' => 'API Key Detect Language harus diisi'));
        $this->form_validation->set_rules('link_youtube', 'URL Video Youtube', 'trim|required', array('trim' => 'URL Video Youtube harus diisi', 'required' => 'URL Video Youtube harus diisi'));
        $this->form_validation->set_error_delimiters('', '<br>');
        $form_data = $this->input->post();
        if ($this->form_validation->run() === FALSE) {
            $this->session->set_flashdata('msg', build_error_message(validation_errors()));
            redirect('administrator/setting');
        } else {
            $update = $this->config_app->update_config($form_data);
            if ($update > 0) {
                $this->session->set_flashdata('msg', build_success_message('Ubah setting berhasil'));
                redirect('administrator/setting');
            } else {
                $error = $this->db->error();
                $this->session->set_flashdata('msg', build_error_message((isset($error['message']) && $error['message']) ? $error['message'] : 'Tidak ada data yang berubah!'));
                redirect('administrator/setting');
            }
        }
    }

    public function submit_mail() {
        $form_data = $this->input->post();
        if($form_data['email_smtp_pass'] == "") {
            unset($form_data['email_smtp_pass']);
        }
        $update = $this->config_app->update_config($form_data);
        if ($update > 0) {
            $this->session->set_flashdata('msg', build_success_message('Ubah setting berhasil'));
            redirect('administrator/setting/2');
        } else {
            $error = $this->db->error();
            $this->session->set_flashdata('msg', build_error_message((isset($error['message']) && $error['message']) ? $error['message'] : 'Tidak ada data yang berubah!'));
            redirect('administrator/setting/2');
        }
    }

}
