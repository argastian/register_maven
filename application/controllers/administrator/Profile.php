<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

    public function __construct() {
        parent::__construct();

        if (!$this->session->has_userdata("loginMaven")) {
            redirect('administrator');
        }
    }

    public function index() {
        $login_data = $this->session->userdata("loginMaven");
        $id_admin = $login_data['kode'];
        session_write_close();
        $profile = $this->admin->get_admin($id_admin);
        $data = array(
            'title' => 'Profile',
            'page' => 'administrator/profile',
            'css' => array(),
            'js' => array(),
            'profile' => $profile,
            'login_data' => $login_data
        );
        $this->load->view('templates/administrator', $data);
    }

    public function submit() {
        $this->form_validation->set_rules('nama', 'Nama', 'trim|required', array('trim' => 'Nama harus diisi', 'required' => 'Nama harus diisi'));
        $this->form_validation->set_rules('username', 'Username', 'required|min_length[6]|max_length[30]|callback__is_unique_edit[admin.username]', array('required' => 'Username harus diisi', 'min_length' => 'Username minimal 6 karakter', 'max_length' => 'Username maksimal 30 karakter', '_is_unique_edit' => 'Username sudah ada'));
        $this->form_validation->set_rules('email', 'Email', 'required|callback__is_unique_edit[admin.email]', array('required' => 'Email harus diisi', '_is_unique_edit' => 'Email sudah ada'));
        if ($this->input->post("password")) {
            $this->form_validation->set_rules('password', 'Password', 'required|min_length[6]', array('required' => 'Password harus diisi', 'min_length' => 'Password minimal 6 karakter'));
            $this->form_validation->set_rules('retype_password', 'Konfirmasi Password', 'required|matches[password]', array('required' => 'Konfirmasi password harus diisi', 'matches' => 'Konfirmasi password tidak sesuai'));
        }

        $this->form_validation->set_error_delimiters('', '<br>');
        $form_data = $this->input->post();
        if ($this->form_validation->run() === FALSE) {
            $this->session->set_flashdata('msg', build_error_message(validation_errors()));
            redirect('administrator/profile');
        } else {
            if ($form_data['password']) {
                $form_data['password'] = password_hash($form_data['password'], PASSWORD_DEFAULT);
            } else {
                unset($form_data['password']);
            }
            unset($form_data['retype_password']);
            $update = $this->admin->update_admin($form_data);
            if ($update > 0) {
                $data_admin = $this->admin->check_login($form_data["username"]);
                $this->session->set_userdata(array("loginMaven" => $data_admin));
                $this->session->set_flashdata('msg', build_success_message('Ubah profile berhasil'));
                redirect('administrator/profile');
            } else {
                $this->session->set_flashdata('msg', build_error_message('Tidak ada data profile yang diubah'));
                redirect('administrator/profile');
            }
        }
    }

    function _is_unique_edit($value, $field) {
        $login_data = $this->session->userdata("loginMaven");
        $id_admin = $login_data['kode'];
        $field = explode(".", $field);
        $data = array($field[1] => $value, "kode !=" => $id_admin);
        $data = $this->admin->get_exist_data($field[0], $data);
        if ($data < 1) {
            return true;
        } else {
            return false;
        }
    }

}
