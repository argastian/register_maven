<?php
$this->load->view('templates/parts/header');
$lokasi = $this->uri->segment(2);
?>
<link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/css/pages/client.min.css?v=' . md5_file('assets/css/pages/client.min.css')); ?>">
<div class="row">
    <div class="list-group sidebar col-sm-5 col-xs-6">
        <div class="list-group-item">
            <p class="list-group-item-text">Halo,</p>
            <h2 class="list-group-item-heading"><?php echo html_escape($login_data['nama_lengkap']); ?></h2>
        </div>
        <a href="<?php echo site_url('client/home') ?>" class="list-group-item <?php echo ($lokasi == 'home' || $lokasi == 'detail-client') ? 'active' : ''; ?>" id="menu-data-diri">
            <div class="img-menu-container">
                <img src="<?php echo ($lokasi == 'home' || $lokasi == 'detail-client') ? base_url('assets/images/client/data_diri_bold.png') : base_url('assets/images/client/data_diri_grey.png'); ?>">
            </div>
            <div class="text-menu-container">
                <span class="hidden-sm hidden-xs">Kelengkapan Data Diri</span>
            </div>
        </a>
        <a href="<?php echo site_url('client/dokumen') ?>" class="list-group-item <?php echo ($lokasi == 'dokumen') ? 'active' : ''; ?>" id="menu-dokumen">
            <div class="img-menu-container">
                <img src="<?php echo base_url('assets/images/client/dokumen_grey.png') ?>">
            </div>
            <div class="text-menu-container">
                <span class="hidden-sm hidden-xs">Kelengkapan Dokumen</span>
            </div>
        </a>
        <a href="<?php echo site_url('client/progress') ?>" class="list-group-item <?php echo ($lokasi == 'progress') ? 'active' : ''; ?>" id="menu-progress">
            <div class="img-menu-container">
                <img src="<?php echo base_url('assets/images/client/progress_grey.png') ?>">
            </div>
            <div class="text-menu-container">
                <span class="hidden-sm hidden-xs">Progress Keseluruhan</span>
            </div>
        </a>
    </div>
    <div class="col-sm-19 col-sm-push-5 col-xs-18 col-xs-push-6 container-content">
        <div class="btn-toolbar pull-right">
            <div class="btn-group dropdown-menu-user-login">
                <div class="btn btn-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-user-circle fa-2x"></i>
                </div>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="<?php echo site_url('client/profile'); ?>"><i class="fa fa-user"></i> Profile</a></li>
                    <li><a href="<?php echo site_url('client/logout'); ?>"><i class="fa fa-sign-out"></i> Keluar</a></li>
                </ul>
            </div>
        </div>       
        <div class="panel panel-content">
            <?php
            if ($this->session->flashdata('msg')) {
                echo $this->session->flashdata('msg');
            }
            ?>
            <?php $this->load->view('pages/' . $page); ?>
        </div>
    </div>
</div>
<?php $this->load->view('templates/parts/footer'); ?>
<!--<img src='<?php // echo site_url('assets/images/Maven Dashboard-01.png') ?>'>-->