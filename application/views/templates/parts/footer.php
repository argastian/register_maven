        <script type="text/javascript" src="<?php echo base_url('assets/includes/jquery/jquery-1.12.4.min.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/includes/bootstrap/js/bootstrap.min.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/includes/simplebar/simplebar.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/js/script.min.js?v=' . md5_file('assets/js/script.min.js')); ?>"></script>
        <?php
        if (isset($js) && $js) {
            if (is_array($js)) {
                foreach ($js as $item) {
                    if (file_exists('./assets/' . $item . '.min.js')) {
                        ?>
                        <script type="text/javascript" src="<?php echo base_url('assets/' . $item . '.min.js?v=' . md5_file('assets/' . $item . '.min.js')); ?>"></script>
                        <?php
                    }
                }
            } else {
                if (file_exists('./assets/' . $js . '.min.js')) {
                    ?>
                    <script type="text/javascript" src="<?php echo base_url('assets/' . $js . '.min.js?v=' . md5_file('assets/' . $js . '.min.js')); ?>"></script>
                    <?php
                }
            }
        }
        ?>
        <script type="text/javascript">
            var site_url = '<?php echo site_url(); ?>';
            var base_url = '<?php echo base_url(); ?>';
        </script>
    </body>
</html>