<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Maven<?php echo (isset($title) && $title) ? ' | ' . $title : ''; ?></title>
        <link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/includes/bootstrap/css/bootstrap.min.css') ?>">
        <link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/css/maven-theme.min.css') ?>">
        <link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/includes/font-awesome/css/font-awesome.min.css') ?>">
        <link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/includes/pe-icon-7-stroke/css/pe-icon-7-stroke.css') ?>">
        <link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/includes/pe-icon-7-stroke/css/helper.css') ?>">
        <link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/includes/simplebar/simplebar.css') ?>">
        <link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/css/style.min.css?v=' . md5_file('assets/css/style.min.css')); ?>">
        <?php
        if (isset($css) && $css) {
            if (is_array($css)) {
                foreach ($css as $item) {
                    if (file_exists('./assets/' . $item . '.min.css')) {
                        ?>
                        <link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/' . $item . '.min.css?v=' . md5_file('assets/' . $item . '.min.css')); ?>">
                        <?php
                    }
                }
            } else {
                if (file_exists('./assets/' . $css . '.min.css')) {
                    ?>
                    <link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/' . $css . '.min.css?v=' . md5_file('assets/' . $css . '.min.css')); ?>">
                    <?php
                }
            }
        }
        ?>     
    </head>

    <body>