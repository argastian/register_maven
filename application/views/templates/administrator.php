<?php
$this->load->view('templates/parts/header');
$lokasi = $this->uri->segment(2);
?>
<link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/css/pages/administrator.min.css?v=' . md5_file('assets/css/pages/administrator.min.css')); ?>">
<div class="row">
    <div class="list-group sidebar col-sm-5 col-xs-8">
        <div class="list-group-item">
            <p class="list-group-item-text">Halo,</p>
            <h2 class="list-group-item-heading"><?php echo html_escape($login_data['nama']); ?></h2>
        </div>
        <a href="<?php echo site_url('administrator/home') ?>" class="list-group-item <?php echo ($lokasi == 'home' || $lokasi == 'detail-client') ? 'active' : ''; ?>"><i class="pe-7s-id pe-4x pe-va"></i> <span class="hidden-xs">Client</span></a>
        <a href="<?php echo site_url('administrator/faq') ?>" class="list-group-item <?php echo ($lokasi == 'faq' || $lokasi == 'kelola-faq' || $lokasi == 'tambah-faq') ? 'active' : ''; ?>"><i class="pe-7s-help1 pe-4x pe-va"></i> <span class="hidden-xs">FAQ</span></a>
        <a href="<?php echo site_url('administrator/setting') ?>" class="list-group-item <?php echo ($lokasi == 'setting') ? 'active' : ''; ?>"><i class="pe-7s-tools pe-4x pe-va"></i> <span class="hidden-xs">Setting</span></a>
    </div>
    <div class="col-sm-19 col-sm-push-5 col-xs-16 col-xs-push-8 container-content">
        <div class="btn-toolbar pull-right">
            <div class="btn-group dropdown-menu-user-login">
                <div class="btn btn-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-user-circle fa-2x"></i>
                </div>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="<?php echo site_url('administrator/profile'); ?>"><i class="fa fa-user"></i> Profile</a></li>
                    <li><a href="<?php echo site_url('administrator/logout'); ?>"><i class="fa fa-sign-out"></i> Keluar</a></li>
                </ul>
            </div>
        </div>       
        <div class="panel panel-content">
            <div class="panel-body">
                <?php
                if ($this->session->flashdata('msg')) {
                    echo $this->session->flashdata('msg');
                }
                ?>
                <?php $this->load->view('pages/' . $page); ?>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('templates/parts/footer'); ?>