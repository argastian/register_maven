<div class="container">
    <div class="row">
        <div class="col-md-24">
            <div class="text-center header-text">
                <h1>Buat perusahaanmu dengan lima langkah mudah!</h1>
                <p class="text-muted">Kita Mulai!</p>
            </div>
        </div>
        <div class="col-md-24">
            <!--<form id="form-maven">-->
            <?php
            if (preg_match('/^(http|https):\\/\\/[a-z0-9_]+([\\-\\.]{1}[a-z_0-9]+)*\\.[_a-z]{2,5}' . '((:[0-9]{1,5})?\\/.*)?$/i', $config_app->link_youtube)) {
                $url_youtube = $config_app->link_youtube;
            } else {
                preg_match('/src="([^"]+)"/', $config_app->link_youtube, $match);
                $url_youtube = $match[1];
            }
            echo form_open(site_url('register/submit'), array('id' => 'form-maven'));
            ?>
            <div class="panel panel-register">
                <div class="panel-body">
                    <div class="embed-responsive embed-responsive-16by9" style="background-image: url(<?php echo base_url('assets/images/Maven-UI_UX-1_10.jpg'); ?>)">
                        <iframe class="embed-responsive-item" src="<?php echo $url_youtube; ?>" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>    
                    </div>
                    <?php $this->load->view('pages/form/form_profil'); ?>
                    <?php $this->load->view('pages/form/form_nama'); ?>
                    <?php $this->load->view('pages/form/form_alamat'); ?>
                    <?php $this->load->view('pages/form/form_modal'); ?>
                    <?php $this->load->view('pages/form/form_pemegang_saham'); ?>
                    <?php $this->load->view('pages/form/form_direksi'); ?>
                    <?php $this->load->view('pages/form/resume'); ?>
                    <?php $this->load->view('pages/form/selesai'); ?>
                </div>
            </div>
            <div class="panel panel-button">
                <div class="pull-right">
                    <div class="btn btn-default disabled btn-info-status hide">Data Tersimpan</div>
                    <button class="btn btn-primary text-uppercase btn-prev hide">SEBELUMNYA</button>
                    <button class="btn btn-primary text-uppercase btn-next" data-loading-text="memproses data...">BERIKUTNYA</button>
                </div>           
            </div>
            </form>
        </div>
    </div>
</div>