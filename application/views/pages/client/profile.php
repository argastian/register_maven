<?php echo form_open(site_url('client/profile/submit'), array('class' => 'form-horizontal')); ?>
<input type="hidden" name="kode" value="<?php echo $profile->kode ?>">
<div class="form-container">    
    <div class="form-group">
        <label for="nama" class="col-sm-6">Nama Lengkap</label> 
        <div class="col-sm-18">
            <input type="text" class="form-control" id="nama_lengkap" name="nama_lengkap" placeholder="Jhon Doe" value="<?php echo html_escape($profile->nama_lengkap) ?>" required="">            
        </div>
    </div>
    <div class="form-group">
        <label for="email_notifikasi_client" class="col-sm-6">Alamat Email</label>
        <div class="col-sm-18">
            <input type="email" class="form-control" id="alamat_email" name="alamat_email" placeholder="jhon.doe@example.com" value="<?php echo html_escape($profile->alamat_email) ?>" required="">
        </div>
    </div>
    <div class="form-group">
        <label for="no_telpon" class="col-sm-6">No Telpon</label>
        <div class="col-sm-18">
            <input type="text" class="form-control" id="no_telpon" name="no_telpon" placeholder="6361905157" onKeyPress="return good_chars(event, '0123456789', this)" value="<?php echo html_escape($profile->no_telpon) ?>" required="">
        </div>
    </div>
    <div class="form-group">
        <label for="password" class="col-sm-6">Password Baru</label>
        <div class="col-sm-18">
            <input type="password" class="form-control" id="password" name="password" placeholder="Password Baru">
        </div>
    </div>
    <div class="form-group">
        <label for="retype_password" class="col-sm-6">Ulangi Password</label>
        <div class="col-sm-18">
            <input type="password" class="form-control" id="retype_password" name="retype_password" placeholder="Ulangi Password">
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-24">
            <button type="submit" class="btn btn-primary pull-right">SIMPAN</button>
        </div>
    </div>
</div>
</form>