<div class="row">
    <div class="col-md-24 text-center">
        <h2><span id="persentase_progress"><?php echo (isset($perusahaan->status) ? $perusahaan->status : '0'); ?></span>% Selesai</h2>
    </div>
    <div class="col-md-24">
        <input type="range" min="0" max="100" step="25" class="slider" id="range_progress" readonly="" disabled="" value="<?php echo (isset($perusahaan->status) ? $perusahaan->status : '0'); ?>">
    </div>
</div>