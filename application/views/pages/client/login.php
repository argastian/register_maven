<div class="login">
    <h2>Login Client</h2>
    <?php
    if ($this->session->flashdata('msg')) {
        echo $this->session->flashdata('msg');
    }
    ?>
    <?php echo form_open('', array('method' => 'POST')); ?>
    <input type="email" name="email" placeholder="Email" required="required" />
    <input type="password" name="password" placeholder="Password" required="required" />
    <div class="row">
        <div class="col-sm-12">
            <div class="checkbox">
                <label><input type="checkbox" value="ingat" name="ingat">Ingat saya</label>
            </div>
        </div>
        <div class="col-sm-12">
            <a href="<?php echo site_url('client/reset'); ?>" class="pull-right">Lupa password?</a>
        </div>
    </div>
    <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-lock"></i> Masuk</button>
</form>
</div>