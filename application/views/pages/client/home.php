<div class="panel-group menu-acordion" id="acordion-data-diri" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne" data-toggle="collapse" data-parent="#acordion-data-diri" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
            <div class="row">
                <div class="col-md-18 col-sm-24 title-container">
                    <h4 class="panel-title">
                        1. Tentukan Nama & Tujuan Perusahaanmu <span class="pull-right"><i class="fa fa-caret-up"></i></span>
                    </h4>
                </div>
                <div class="col-md-6 hidden-sm hidden-xs info-container text-center">
                    <div class="panel-info hide">
                        <span class="text-success"><strong>Everything's Good!</strong></span>
                    </div>
                </div>
            </div>
        </div>
        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-18 col-sm-24 data-container form-1">
                        <?php echo form_open(site_url('client/home/submit_nama'), array('class' => 'form-horizontal')); ?>
                        <input type="hidden" name="id_perusahaan" value="<?php echo $perusahaan->kode; ?>">
                        <div class="form-container">
                            <div class="form-group">
                                <label for="nama_perusahaan_1" class="col-md-5">Pilih Nama 1</label>
                                <div class="col-md-19">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                PT
                                            </div>
                                            <input type="text" class="form-control" id="nama_perusahaan_1" name="nama_perusahaan_1" placeholder="Kolaborasi Kapital Indonesia" onKeyPress="return prevent_number(event)" value="<?php echo isset($perusahaan->nama_perusahaan_1) ? $perusahaan->nama_perusahaan_1 : ''; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="nama_perusahaan_2" class="col-md-5">Pilih Nama 2</label>  
                                <div class="col-md-19">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                PT
                                            </div>
                                            <input type="text" class="form-control" id="nama_perusahaan_2" name="nama_perusahaan_2" placeholder="Kapital Kolaborasi Indonesia" onKeyPress="return prevent_number(event)" value="<?php echo isset($perusahaan->nama_perusahaan_1) ? $perusahaan->nama_perusahaan_2 : ''; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="nama_perusahaan_3" class="col-md-5">Pilih Nama 3</label>
                                <div class="col-md-19">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                PT
                                            </div>
                                            <input type="text" class="form-control" id="nama_perusahaan_3" name="nama_perusahaan_3" placeholder="Kolaborasi Kapital Indonesia" onKeyPress="return prevent_number(event)" value="<?php echo isset($perusahaan->nama_perusahaan_1) ? $perusahaan->nama_perusahaan_3 : ''; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group textarea">
                                <label for="tujuan_pendirian" class="col-md-5">Tujuan Perusahaan</label>
                                <div class="col-md-19">
                                    <textarea class="form-control" id="tujuan_pendirian" name="tujuan_pendirian" placeholder="Jelaskan secara singkat bisnis yang akan Anda jalani menggunakan PT ini"><?php echo isset($perusahaan->nama_perusahaan_1) ? $perusahaan->tujuan_pendirian : ''; ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-24">
                                    <button type="submit" class="btn btn-primary pull-right">SIMPAN</button>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                    <div class="col-md-6 hidden-sm hidden-xs faq-container">
                        <div class="panel-faq">
                            <div class="text-center"><strong><i>Daftar Pertanyaan Umum</i></strong></div>
                            <div id="faq-nama-tujuan" data-simplebar>
                                <ol>
                                    <?php
                                    if ($faq) {
                                        foreach ($faq as $item) {
                                            if ($item->kategori == 'nama_tujuan') {
                                                ?>
                                                <li><?php echo $item->tanya ?></li>
                                                <?php echo $item->jawab ?>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading collapsed" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#acordion-data-diri" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
            <div class="row">
                <div class="col-md-18 col-sm-24 title-container">
                    <h4 class="panel-title">
                        2. Tentukan Alamat Domisili <span class="pull-right"><i class="fa fa-caret-down"></i>
                    </h4>
                </div>
                <div class="col-md-6 hidden-sm hidden-xs info-container text-center">
                    <div class="panel-info">
                        <span class="text-success"><strong>Everything's Good!</strong></span>
                    </div>
                </div>
            </div>
        </div>
        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-18 col-sm-24 data-container form-2">
                        <?php echo form_open(site_url('client/home/submit_alamat'), array('class' => 'form-horizontal')); ?>
                        <input type="hidden" name="id_perusahaan" value="<?php echo $perusahaan->kode; ?>">
                        <div class="form-container">    
                            <div class="form-group">
                                <label for="kepemilikan" class="col-md-5">Kepemilikan</label>
                                <div class="col-md-19">
                                    <select class="form-control" id="kepemilikan" name="alamat[kepemilikan]">
                                        <option value="virtual_office" <?php echo (isset($perusahaan->kepemilikan) && $perusahaan->kepemilikan == 'virtual_office') ? 'selected' : ''; ?>>Virtual Office</option>
                                        <option value="sewa" <?php echo (isset($perusahaan->kepemilikan) && $perusahaan->kepemilikan == 'sewa') ? 'selected' : ''; ?>>Sewa</option>
                                        <option value="milik_sendiri" <?php echo (isset($perusahaan->kepemilikan) && $perusahaan->kepemilikan == 'milik_sendiri') ? 'selected' : ''; ?>>Milik Sendiri</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="alamat_domisili" class="col-md-5">Alamat Domisili</label>
                                <div class="col-md-19">
                                    <input type="text" class="form-control" id="alamat_domisili" name="alamat[alamat_domisili]" placeholder="1635 Franklin Street Montgomery" value="<?php echo isset($perusahaan->alamat_domisili) ? $perusahaan->alamat_domisili : '' ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="nama_gedung" class="col-md-5">Nama Gedung <span class="label-description">(jika didalam gedung)</span></label>
                                <div class="col-md-19">
                                    <input type="text" class="form-control" id="nama_gedung" name="alamat[nama_gedung]" placeholder="Prestige Dune Tower" value="<?php echo isset($perusahaan->nama_gedung) ? $perusahaan->nama_gedung : '' ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="provinsi" class="col-md-5">Provinsi</label>
                                <div class="col-md-19">
                                    <select class="form-control" id="provinsi" name="alamat[provinsi]">
                                        <option>Pilih Provinsi</option>
                                        <?php
                                        foreach ($provinsi as $prov) {
                                            $selected = (isset($perusahaan->provinsi) && ($perusahaan->provinsi == $prov->kode)) ? 'selected' : '';
                                            ?>
                                            <option value="<?php echo $prov->kode ?>" <?php echo $selected; ?>><?php echo display_nama_wilayah($prov->nama); ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="kota_madya" class="col-md-5">Kota Madya</label>
                                <div class="col-md-19">
                                    <select class="form-control" id="kota_madya" name="alamat[kota_madya]">
                                        <option>Pilih Kota Madya</option>
                                        <?php
                                        if (isset($perusahaan->kota_madya)) {
                                            foreach ($kota_madya as $kota) {
                                                $selected = ($perusahaan->kota_madya == $kota->kode) ? 'selected' : '';
                                                ?>
                                                <option value="<?php echo $kota->kode ?>" <?php echo $selected; ?>><?php echo display_nama_wilayah($kota->nama); ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="kecamatan" class="col-md-5">Kecamatan</label>
                                <div class="col-md-19">
                                    <select class="form-control" id="kecamatan" name="alamat[kecamatan]">
                                        <option>Pilih Kecamatan</option>
                                        <?php
                                        if (isset($perusahaan->kecamatan)) {
                                            foreach ($kecamatan as $kec) {
                                                $selected = ($perusahaan->kecamatan == $kec->kode) ? 'selected' : '';
                                                ?>
                                                <option value="<?php echo $kec->kode ?>" <?php echo $selected; ?>><?php echo display_nama_wilayah($kec->nama); ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="kelurahan" class="col-md-5">Kelurahan</label>
                                <div class="col-md-19">
                                    <select class="form-control" id="kelurahan" name="alamat[kelurahan]">
                                        <option>Pilih Kelurahan</option>
                                        <?php
                                        if (isset($perusahaan->kelurahan)) {
                                            foreach ($kelurahan as $kel) {
                                                $selected = ($perusahaan->kelurahan == $kel->kode) ? 'selected' : '';
                                                ?>
                                                <option value="<?php echo $kel->kode ?>" <?php echo $selected; ?>><?php echo display_nama_wilayah($kel->nama); ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-24">
                                    <button type="submit" class="btn btn-primary pull-right">SIMPAN</button>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                    <div class="col-md-6 hidden-sm hidden-xs faq-container">
                        <div class="panel-faq">
                            <div class="text-center"><strong><i>Daftar Pertanyaan Umum</i></strong></div>
                            <div id="faq-domisili" data-simplebar>
                                <ol>
                                    <?php
                                    if ($faq) {
                                        foreach ($faq as $item) {
                                            if ($item->kategori == 'domisili') {
                                                ?>
                                                <li><?php echo $item->tanya ?></li>
                                                <?php echo $item->jawab ?>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading collapsed" role="tab" id="headingThree" data-toggle="collapse" data-parent="#acordion-data-diri" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
            <div class="row">
                <div class="col-md-18 col-sm-24 title-container">
                    <h4 class="panel-title">
                        3. Tentukan Modal Perusahaan <span class="pull-right"><i class="fa fa-caret-down"></i>
                    </h4>
                </div>
                <div class="col-md-6 hidden-sm hidden-xs info-container text-center">
                    <div class="panel-info">
                        <span class="text-success"><strong>Everything's Good!</strong></span>
                    </div>
                </div>
            </div>
        </div>
        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-18 col-sm-24 data-container form-3">
                        <?php echo form_open(site_url('client/home/submit_modal'), array('class' => 'form-horizontal')); ?>
                        <input type="hidden" name="id_perusahaan" value="<?php echo $perusahaan->kode; ?>">
                        <div class="form-container">
                            <div class="form-group">
                                <label for="modal_dasar" class="col-md-5">Modal Dasar</label>
                                <div class="col-md-19">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                Rp
                                            </div>
                                            <input type="text" class="form-control" id="modal_dasar" name="modal_dasar" name="modal_dasar" placeholder="50.000.000" onKeyPress="return good_chars(event, '.0123456789', this)" value="<?php echo isset($perusahaan->modal_dasar) ? $perusahaan->modal_dasar : '' ?>">
                                        </div>
                                        <small>
                                            adalah seluruh nilai nominal saham Perseroan yang disebut dalam Anggaran Dasar. Modal dasar pada prinsipnya merupakan total jumlah saham yang dapat diterbitkan oleh Perseroan.
                                        </small>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="modal_dasar" class="col-md-5">Modal Disetor & Ditempatkan</label>
                                <div class="col-md-19">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                Rp
                                            </div>
                                            <input type="text" class="form-control" id="modal_disetor" name="modal_disetor" name="modal_disetor" placeholder="50.000.000" onKeyPress="return good_chars(event, '.0123456789', this)" value="<?php echo isset($perusahaan->modal_disetor) ? $perusahaan->modal_disetor : '' ?>">
                                        </div>
                                        <small>
                                            jumlah saham yang sudah diambil pendiri atau pemegang saham. Dengan kata lain, modal ditempatkan itu adalah modal yang disanggupi pendiri atau pemegang saham untuk dilunasinya, dan saham itu telah diserahkan kepadanya untuk dimiliki.
                                        </small>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="modal_dasar" class="col-md-5">Nominal Saham /Lembar</label>
                                <div class="col-md-19">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                Rp
                                            </div>
                                            <input type="text" class="form-control" id="nominal_saham" name="nominal_saham" name="nominal_saham" placeholder="50.000.000" onKeyPress="return good_chars(event, '.0123456789', this)" value="<?php echo isset($perusahaan->nominal_saham) ? $perusahaan->nominal_saham : '' ?>">
                                        </div>
                                        <small>
                                            Nominal saham per lembar adalah harga pecahan saham per lembar yang dikeluarkan perusahaan.
                                        </small>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-24">
                                    <button type="submit" class="btn btn-primary pull-right">SIMPAN</button>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                    <div class="col-md-6 hidden-sm hidden-xs faq-container">
                        <div class="panel-faq">
                            <div class="text-center"><strong><i>Daftar Pertanyaan Umum</i></strong></div>
                            <div id="faq-modal" data-simplebar>
                                <ol>
                                    <?php
                                    if ($faq) {
                                        foreach ($faq as $item) {
                                            if ($item->kategori == 'modal') {
                                                ?>
                                                <li><?php echo $item->tanya ?></li>
                                                <?php echo $item->jawab ?>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading collapsed" role="tab" id="headingFour" data-toggle="collapse" data-parent="#acordion-data-diri" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
            <div class="row">
                <div class="col-md-18 col-sm-24 title-container">
                    <h4 class="panel-title">
                        4. Tentukan Susunan Organ Perusahaan <span class="pull-right"><i class="fa fa-caret-down"></i>
                    </h4>
                </div>
                <div class="col-md-6 hidden-sm hidden-xs info-container text-center">
                    <div class="panel-info">
                        <span class="text-success"><strong>Everything's Good!</strong></span>
                    </div>
                </div>
            </div>
        </div>
        <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-18 col-sm-24 data-container">

                        <div>

                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#pemegang-saham" aria-controls="pemegang-saham" role="tab" data-toggle="tab">Pemegang Saham</a></li>
                                <li role="presentation"><a href="#pengurus-perusahaan" aria-controls="pengurus-perusahaan" role="tab" data-toggle="tab">Pengurus Perusahaan</a></li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="pemegang-saham">
                                    <?php echo form_open(site_url('client/home/submit_pemegang_saham'), array('class' => 'form-horizontal')); ?>
                                    <input type="hidden" name="id_perusahaan" value="<?php echo $perusahaan->kode; ?>">
                                    <div class="form-container form-4">
                                        <div id="container-pemegang-saham">
                                            <?php if (!isset($pemegang_saham_perseorangan) || !isset($pemegang_saham_perusahaan) || (isset($pemegang_saham_perseorangan) && $pemegang_saham_perseorangan == '') || (isset($pemegang_saham_perusahaan) && $pemegang_saham_perusahaan == '')) { ?>
                                                <div class="item-pemegang-saham">
                                                    <div class="item-label">Pemegang Saham 1</div>
                                                    <hr>               
                                                    <div class="form-group">
                                                        <label for="jenis_pemegang_saham[0]" class="col-md-5">Jenis Pemegang Saham</label>
                                                        <div class="col-md-19">
                                                            <select class="form-control" id="jenis_pemegang_saham[0]" name="jenis_pemegang_saham[0]">
                                                                <option value="perseorangan">Perseorangan</option>
                                                                <option value="perusahaan">Perusahaan</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="group-pemegang-saham saham-perseorangan">
                                                        <div class="form-group">
                                                            <label for="pemegang_saham_perseorangan[0]" class="col-md-5">Nama Lengkap</label>
                                                            <div class="col-md-19">
                                                                <input type="text" class="form-control" id="pemegang_saham_perseorangan[0]" name="pemegang_saham[perseorangan][0][nama]" placeholder="Sutansyah Marahakim">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="ktp_pemegang_saham_perseorangan[0]" class="col-md-5">No KTP</label>
                                                            <div class="col-md-19">
                                                                <input type="text" class="form-control" id="ktp_pemegang_saham_perseorangan[0]" name="pemegang_saham[perseorangan][0][no_ktp]" placeholder="1234567890" onKeyPress="return good_chars(event, '0123456789', this)">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="alamat_pemegang_saham_perseorangan[0]" class="col-md-5">Alamat Sesuai KTP</label>
                                                            <div class="col-md-19">
                                                                <input type="text" class="form-control" id="alamat_pemegang_saham_perseorangan[0]" name="pemegang_saham[perseorangan][0][alamat]" placeholder="Jl. ABC No. 123">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="npwp_pemegang_saham_perseorangan[0]" class="col-md-5">No NPWP</label>
                                                            <div class="col-md-19">
                                                                <input type="text" class="form-control" id="npwp_pemegang_saham_perseorangan[0]" name="pemegang_saham[perseorangan][0][no_npwp]" placeholder="1234567890" onKeyPress="return good_chars(event, '0123456789', this)">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="persentase_pemegang_saham_perseorangan[0]" class="col-md-5">Jumlah Persentase Saham</label>
                                                            <div class="col-md-19">
                                                                <div class="form-group">
                                                                    <div class="input-group">
                                                                        <input type="text" class="form-control" id="persentase_pemegang_saham_perseorangan[0]" name="pemegang_saham[perseorangan][0][persentase_saham]" placeholder="30" onKeyPress="return good_chars(event, '.0123456789', this)">
                                                                        <div class="input-group-addon">
                                                                            %
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="group-pemegang-saham saham-perusahaan hide">
                                                        <div class="form-group">
                                                            <label for="pemegang_saham_perusahaan[0]" class="col-md-5">Nama PT</label>
                                                            <div class="col-md-19">
                                                                <div class="form-group">
                                                                    <div class="input-group">
                                                                        <div class="input-group-addon">
                                                                            PT
                                                                        </div>
                                                                        <input type="text" class="form-control" id="pemegang_saham_perusahaan[0]" name="pemegang_saham[perusahaan][0][nama]" placeholder="Kolaborasi Kapital Indonesia" onKeyPress="return prevent_number(event)">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="no_akta_perusahaan[0]" class="col-md-5">No Akta Pendirian</label>
                                                            <div class="col-md-19">
                                                                <input type="text" class="form-control" id="no_akta_perusahaan[0]" name="pemegang_saham[perusahaan][0][no_akta]" placeholder="1234567890">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="tanggal_pendirian_perusahaan[0]" class="col-md-5">Tanggal Pendirian</label>
                                                            <div class="col-md-19">
                                                                <input type="text" class="form-control datepicker" id="tanggal_pendirian_perusahaan[0]" name="pemegang_saham[perusahaan][0][tanggal_pendirian]" placeholder="2018-01-11" data-date-max-date="now">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="sk_menkumham_perusahaan[0]" class="col-md-5">No SK Menkumham</label>
                                                            <div class="col-md-19">
                                                                <input type="text" class="form-control" id="sk_menkumham_perusahaan[0]" name="pemegang_saham[perusahaan][0][no_sk]" placeholder="1234567890">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="npwp_pemegang_saham_perusahaan[0]" class="col-md-5">No NPWP Perusahaan</label>
                                                            <div class="col-md-19">
                                                                <input type="text" class="form-control" id="npwp_pemegang_saham_perusahaan[0]" name="pemegang_saham[perusahaan][0][no_npwp]" placeholder="1234567890" onKeyPress="return good_chars(event, '0123456789', this)">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="persentase_pemegang_saham_perusahaan[0]" class="col-md-5">Jumlah Persentase Saham</label>
                                                            <div class="col-md-19">
                                                                <div class="form-group">
                                                                    <div class="input-group">
                                                                        <input type="text" class="form-control" id="persentase_pemegang_saham_perusahaan[0]" name="pemegang_saham[perusahaan][0][persentase_saham]" placeholder="30" onKeyPress="return good_chars(event, '.0123456789', this)">
                                                                        <div class="input-group-addon">
                                                                            %
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="item-pemegang-saham">
                                                    <div class="item-label">Pemegang Saham 2</div>
                                                    <hr>               
                                                    <div class="form-group">
                                                        <label for="jenis_pemegang_saham[1]" class="col-md-5">Jenis Pemegang Saham</label>
                                                        <div class="col-md-19">
                                                            <select class="form-control" id="jenis_pemegang_saham[1]" name="jenis_pemegang_saham[1]">
                                                                <option value="perseorangan">Perseorangan</option>
                                                                <option value="perusahaan">Perusahaan</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="group-pemegang-saham saham-perseorangan">
                                                        <div class="form-group">
                                                            <label for="pemegang_saham_perseorangan[1]" class="col-md-5">Nama Lengkap</label>
                                                            <div class="col-md-19">
                                                                <input type="text" class="form-control" id="pemegang_saham_perseorangan[1]" name="pemegang_saham[perseorangan][1][nama]" placeholder="Sutansyah Marahakim">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="ktp_pemegang_saham_perseorangan[1]" class="col-md-5">No KTP</label>
                                                            <div class="col-md-19">
                                                                <input type="text" class="form-control" id="ktp_pemegang_saham_perseorangan[1]" name="pemegang_saham[perseorangan][1][no_ktp]" placeholder="1234567890" onKeyPress="return good_chars(event, '0123456789', this)">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="alamat_pemegang_saham_perseorangan[1]" class="col-md-5">Alamat Sesuai KTP</label>
                                                            <div class="col-md-19">
                                                                <input type="text" class="form-control" id="alamat_pemegang_saham_perseorangan[1]" name="pemegang_saham[perseorangan][1][alamat]" placeholder="Jl. ABC No. 123">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="npwp_pemegang_saham_perseorangan[1]" class="col-md-5">No NPWP</label>
                                                            <div class="col-md-19">
                                                                <input type="text" class="form-control" id="npwp_pemegang_saham_perseorangan[1]" name="pemegang_saham[perseorangan][1][no_npwp]" placeholder="1234567890" onKeyPress="return good_chars(event, '0123456789', this)">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="persentase_pemegang_saham_perseorangan[1]" class="col-md-5">Jumlah Persentase Saham</label>
                                                            <div class="col-md-19">
                                                                <div class="form-group">
                                                                    <div class="input-group">
                                                                        <input type="text" class="form-control" id="persentase_pemegang_saham_perseorangan[1]" name="pemegang_saham[perseorangan][1][persentase_saham]" placeholder="30" onKeyPress="return good_chars(event, '.0123456789', this)">
                                                                        <div class="input-group-addon">
                                                                            %
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="group-pemegang-saham saham-perusahaan hide">
                                                        <div class="form-group">
                                                            <label for="pemegang_saham_perusahaan[1]" class="col-md-5">Nama PT</label>
                                                            <div class="col-md-19">
                                                                <div class="form-group">
                                                                    <div class="input-group">
                                                                        <div class="input-group-addon">
                                                                            PT
                                                                        </div>
                                                                        <input type="text" class="form-control" id="pemegang_saham_perusahaan[1]" name="pemegang_saham[perusahaan][1][nama]" placeholder="Kolaborasi Kapital Indonesia" onKeyPress="return prevent_number(event)">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="no_akta_perusahaan[1]" class="col-md-5">No Akta Pendirian</label>
                                                            <div class="col-md-19">
                                                                <input type="text" class="form-control" id="no_akta_perusahaan[1]" name="pemegang_saham[perusahaan][1][no_akta]" placeholder="1234567890">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="tanggal_pendirian_perusahaan[1]" class="col-md-5">Tanggal Pendirian</label>
                                                            <div class="col-md-19">
                                                                <input type="text" class="form-control datepicker" id="tanggal_pendirian_perusahaan[1]" name="pemegang_saham[perusahaan][1][tanggal_pendirian]" placeholder="2018-01-11" data-date-max-date="now">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="sk_menkumham_perusahaan[1]" class="col-md-5">No SK Menkumham</label>
                                                            <div class="col-md-19">
                                                                <input type="text" class="form-control" id="sk_menkumham_perusahaan[1]" name="pemegang_saham[perusahaan][1][no_sk]" placeholder="1234567890">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="npwp_pemegang_saham_perusahaan[1]" class="col-md-5">No NPWP Perusahaan</label>
                                                            <div class="col-md-19">
                                                                <input type="text" class="form-control" id="npwp_pemegang_saham_perusahaan[1]" name="pemegang_saham[perusahaan][1][no_npwp]" placeholder="1234567890" onKeyPress="return good_chars(event, '0123456789', this)">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="persentase_pemegang_saham_perusahaan[1]" class="col-md-5">Jumlah Persentase Saham</label>
                                                            <div class="col-md-19">
                                                                <div class="form-group">
                                                                    <div class="input-group">
                                                                        <input type="text" class="form-control" id="persentase_pemegang_saham_perusahaan[1]" name="pemegang_saham[perusahaan][1][persentase_saham]" placeholder="30" onKeyPress="return good_chars(event, '.0123456789', this)">
                                                                        <div class="input-group-addon">
                                                                            %
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } else { ?>
                                                <?php
                                                $no = 1;
                                                foreach ($pemegang_saham_perseorangan as $saham_perseorangan) {
                                                    ?>
                                                    <div class="item-pemegang-saham">
                                                        <div class="item-label">Pemegang Saham <?php echo $no ?></div>
                                                        <hr>
                                                        <?php if ($no > 2) { ?>
                                                            <div class="pull-right">
                                                                <div class="btn btn-default btn-delete-pemegang-saham"><i class="fa fa-trash"></i></div>
                                                            </div>
                                                        <?php } ?>
                                                        <div class="form-group">
                                                            <label for="jenis_pemegang_saham[<?php echo $no - 1; ?>]" class="col-md-5">Jenis Pemegang Saham</label>
                                                            <div class="col-md-19">
                                                                <select class="form-control" id="jenis_pemegang_saham[<?php echo $no - 1; ?>]" name="jenis_pemegang_saham[<?php echo $no - 1; ?>]" disabled="">
                                                                    <option value="perseorangan">Perseorangan</option>
                                                                </select>
                                                            </div>
                                                            <input type="hidden" name="jenis_pemegang_saham[<?php echo $no - 1; ?>]" value="perseorangan">
                                                        </div>
                                                        <div class="group-pemegang-saham saham-perseorangan">
                                                            <div class="form-group">
                                                                <label for="pemegang_saham_perseorangan[<?php echo $no - 1; ?>]" class="col-md-5">Nama Lengkap</label>
                                                                <div class="col-md-19">
                                                                    <input type="text" class="form-control" id="pemegang_saham_perseorangan[<?php echo $no - 1; ?>]" name="pemegang_saham[perseorangan][<?php echo $no - 1; ?>][nama]" placeholder="Sutansyah Marahakim" value="<?php echo isset($saham_perseorangan->nama) ? $saham_perseorangan->nama : '' ?>">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="ktp_pemegang_saham_perseorangan[<?php echo $no - 1; ?>]" class="col-md-5">No KTP</label>
                                                                <div class="col-md-19">
                                                                    <input type="text" class="form-control" id="ktp_pemegang_saham_perseorangan[<?php echo $no - 1; ?>]" name="pemegang_saham[perseorangan][<?php echo $no - 1; ?>][no_ktp]" placeholder="1234567890" onKeyPress="return good_chars(event, '0123456789', this)" value="<?php echo isset($saham_perseorangan->no_ktp) ? $saham_perseorangan->no_ktp : '' ?>">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="alamat_pemegang_saham_perseorangan[<?php echo $no - 1; ?>]" class="col-md-5">Alamat Sesuai KTP</label>
                                                                <div class="col-md-19">
                                                                    <input type="text" class="form-control" id="alamat_pemegang_saham_perseorangan[<?php echo $no - 1; ?>]" name="pemegang_saham[perseorangan][<?php echo $no - 1; ?>][alamat]" placeholder="Jl. ABC No. 123" value="<?php echo isset($saham_perseorangan->alamat) ? $saham_perseorangan->alamat : '' ?>">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="npwp_pemegang_saham_perseorangan[<?php echo $no - 1; ?>]" class="col-md-5">No NPWP</label>
                                                                <div class="col-md-19">
                                                                    <input type="text" class="form-control" id="npwp_pemegang_saham_perseorangan[<?php echo $no - 1; ?>]" name="pemegang_saham[perseorangan][<?php echo $no - 1; ?>][no_npwp]" placeholder="1234567890" onKeyPress="return good_chars(event, '0123456789', this)" value="<?php echo isset($saham_perseorangan->no_npwp) ? $saham_perseorangan->no_npwp : '' ?>">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="persentase_pemegang_saham_perseorangan[<?php echo $no - 1; ?>]" class="col-md-5">Jumlah Persentase Saham</label>
                                                                <div class="col-md-19">
                                                                    <div class="form-group">
                                                                        <div class="input-group">
                                                                            <input type="text" class="form-control" id="persentase_pemegang_saham_perseorangan[<?php echo $no - 1; ?>]" name="pemegang_saham[perseorangan][<?php echo $no - 1; ?>][persentase_saham]" placeholder="30" onKeyPress="return good_chars(event, '.0123456789', this)" value="<?php echo isset($saham_perseorangan->persentase_saham) ? $saham_perseorangan->persentase_saham : '' ?>">
                                                                            <div class="input-group-addon">
                                                                                %
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php
                                                    $no++;
                                                }
                                                ?>
                                                <?php
                                                foreach ($pemegang_saham_perusahaan as $saham_perusahaan) {
                                                    ?>
                                                    <div class="item-pemegang-saham">
                                                        <div class="item-label">Pemegang Saham <?php echo $no ?></div>
                                                        <hr>    
                                                        <?php if ($no > 2) { ?>
                                                            <div class="pull-right">
                                                                <div class="btn btn-default btn-delete-pemegang-saham"><i class="fa fa-trash"></i></div>
                                                            </div>
                                                        <?php } ?>
                                                        <div class="form-group">
                                                            <label for="jenis_pemegang_saham[<?php echo $no - 1; ?>]" class="col-md-5">Jenis Pemegang Saham</label>
                                                            <div class="col-md-19">
                                                                <select class="form-control" id="jenis_pemegang_saham[<?php echo $no - 1; ?>]" name="jenis_pemegang_saham[<?php echo $no - 1; ?>]" disabled="">
                                                                    <option value="perusahaan">Perusahaan</option>
                                                                </select>
                                                            </div>
                                                            <input type="hidden" name="jenis_pemegang_saham[<?php echo $no - 1; ?>]" value="perusahaan">
                                                        </div>
                                                        <div class="group-pemegang-saham saham-perusahaan">
                                                            <div class="form-group">
                                                                <label for="pemegang_saham_perusahaan[<?php echo $no - 1; ?>]" class="col-md-5">Nama PT</label>
                                                                <div class="col-md-19">
                                                                    <div class="form-group">
                                                                        <div class="input-group">
                                                                            <div class="input-group-addon">
                                                                                PT
                                                                            </div>
                                                                            <input type="text" class="form-control" id="pemegang_saham_perusahaan[<?php echo $no - 1; ?>]" name="pemegang_saham[perusahaan][<?php echo $no - 1; ?>][nama]" placeholder="Kolaborasi Kapital Indonesia" onKeyPress="return prevent_number(event)" value="<?php echo isset($saham_perusahaan->nama) ? $saham_perusahaan->nama : '' ?>">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="no_akta_perusahaan[<?php echo $no - 1; ?>]" class="col-md-5">No Akta Pendirian</label>
                                                                <div class="col-md-19">
                                                                    <input type="text" class="form-control" id="no_akta_perusahaan[<?php echo $no - 1; ?>]" name="pemegang_saham[perusahaan][<?php echo $no - 1; ?>][no_akta]" placeholder="1234567890" value="<?php echo isset($saham_perusahaan->no_akta) ? $saham_perusahaan->no_akta : '' ?>">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="tanggal_pendirian_perusahaan[<?php echo $no - 1; ?>]" class="col-md-5">Tanggal Pendirian</label>
                                                                <div class="col-md-19">
                                                                    <input type="text" class="form-control datepicker" id="tanggal_pendirian_perusahaan[<?php echo $no - 1; ?>]" name="pemegang_saham[perusahaan][<?php echo $no - 1; ?>][tanggal_pendirian]" placeholder="2018-01-11" data-date-max-date="now" value="<?php echo isset($saham_perusahaan->tanggal_pendirian) ? $saham_perusahaan->tanggal_pendirian : '' ?>">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="sk_menkumham_perusahaan[<?php echo $no - 1; ?>]" class="col-md-5">No SK Menkumham</label>
                                                                <div class="col-md-19">
                                                                    <input type="text" class="form-control" id="sk_menkumham_perusahaan[<?php echo $no - 1; ?>]" name="pemegang_saham[perusahaan][<?php echo $no - 1; ?>][no_sk]" placeholder="1234567890" value="<?php echo isset($saham_perusahaan->no_sk) ? $saham_perusahaan->no_sk : '' ?>">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="npwp_pemegang_saham_perusahaan[<?php echo $no - 1; ?>]" class="col-md-5">No NPWP Perusahaan</label>
                                                                <div class="col-md-19">
                                                                    <input type="text" class="form-control" id="npwp_pemegang_saham_perusahaan[<?php echo $no - 1; ?>]" name="pemegang_saham[perusahaan][<?php echo $no - 1; ?>][no_npwp]" placeholder="1234567890" onKeyPress="return good_chars(event, '0123456789', this)" value="<?php echo isset($saham_perusahaan->no_npwp) ? $saham_perusahaan->no_npwp : '' ?>">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="persentase_pemegang_saham_perusahaan[<?php echo $no - 1; ?>]" class="col-md-5">Jumlah Persentase Saham</label>
                                                                <div class="col-md-19">
                                                                    <div class="form-group">
                                                                        <div class="input-group">
                                                                            <input type="text" class="form-control" id="persentase_pemegang_saham_perusahaan[<?php echo $no - 1; ?>]" name="pemegang_saham[perusahaan][<?php echo $no - 1; ?>][persentase_saham]" placeholder="30" onKeyPress="return good_chars(event, '.0123456789', this)" value="<?php echo isset($saham_perusahaan->persentase_saham) ? $saham_perusahaan->persentase_saham : '' ?>">
                                                                            <div class="input-group-addon">
                                                                                %
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php
                                                    $no++;
                                                }
                                                ?>
                                            <?php } ?>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <div class="btn-group dropup">
                                                    <div id="btn-add-pemegang-saham" class="btn btn-link pull-left dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <span class="fa fa-stack">
                                                            <i class="fa fa-circle"></i>
                                                            <i class="fa fa-plus-circle"></i>   
                                                        </span>
                                                        Tambah Pemegang Saham
                                                    </div>
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li class="btn-add-pemegang-saham" data-type="perseorangan"><div>Perseorangan</div></li>
                                                        <li class="btn-add-pemegang-saham" data-type="perusahaan"><div>Perusahaan</div></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <button type="submit" class="btn btn-primary pull-right">SIMPAN</button>
                                            </div>
                                        </div>
                                    </div>
                                    </form>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="pengurus-perusahaan">
                                    <?php echo form_open(site_url('client/home/submit_pengurus_perusahaan'), array('class' => 'form-horizontal')); ?>
                                    <input type="hidden" name="id_perusahaan" value="<?php echo $perusahaan->kode; ?>">
                                    <div class="form-container form-5">
                                        <div id="container-pengurus">
                                            <?php if (!isset($pengurus_perusahaan) || isset($pengurus_perusahaan) && $pengurus_perusahaan == '') { ?>
                                                <div class="item-pengurus">
                                                    <div class="item-label">Pengurus Perusahaan 1</div>
                                                    <hr>
                                                    <div class="form-group">
                                                        <label for="jenis_jabatan[0]" class="col-md-5">Jenis Jabatan</label>
                                                        <div class="col-md-19">
                                                            <select class="form-control" id="jenis_jabatan[0]" name="pengurus[0][jenis_jabatan]">
                                                                <option value="direksi">Dewan Direksi</option>
                                                                <option value="komisaris">Dewan Komisaris</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="jabatan[0]" class="col-md-5">Nama Jabatan</label>
                                                        <div class="col-md-19">
                                                            <input type="text" class="form-control" id="jabatan[0]" name="pengurus[0][nama_jabatan]" placeholder="Direktur">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="pengurus[0]" class="col-md-5">Nama Pengurus</label>
                                                        <div class="col-md-19">
                                                            <input type="text" class="form-control" id="pengurus[0]" name="pengurus[0][nama_pengurus]" placeholder="Sutansyah Marahakim">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="ktp_pengurus[0]" class="col-md-5">No KTP</label>
                                                        <div class="col-md-19">
                                                            <input type="text" class="form-control" id="ktp_pengurus[0]" name="pengurus[0][no_ktp]" placeholder="1234567890" onKeyPress="return good_chars(event, '0123456789', this)">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="npwp_pengurus[0]" class="col-md-5">No NPWP</label>
                                                        <div class="col-md-19">
                                                            <input type="text" class="form-control" id="npwp_pengurus[0]" name="pengurus[0][no_npwp]" placeholder="1234567890" onKeyPress="return good_chars(event, '0123456789', this)">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="alamat_pengurus[0]" class="col-md-5">Alamat Sesuai KTP</label>
                                                        <div class="col-md-19">
                                                            <input type="text" class="form-control" id="alamat_pengurus[0]" name="pengurus[0][alamat]" placeholder="Jl. ABC No. 123">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="item-pengurus">
                                                    <div class="item-label">Pengurus Perusahaan 2</div>
                                                    <hr>
                                                    <div class="form-group">
                                                        <label for="jenis_jabatan[1]" class="col-md-5">Jenis Jabatan</label>
                                                        <div class="col-md-19">
                                                            <select class="form-control" id="jenis_jabatan[1]" name="pengurus[1][jenis_jabatan]">
                                                                <option value="direksi">Dewan Direksi</option>
                                                                <option value="komisaris" selected>Dewan Komisaris</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="jabatan[1]" class="col-md-5">Nama Jabatan</label>
                                                        <div class="col-md-19">
                                                            <input type="text" class="form-control" id="jabatan[1]" name="pengurus[1][nama_jabatan]" placeholder="Komisaris">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="pengurus[1]" class="col-md-5">Nama Pengurus</label>
                                                        <div class="col-md-19">
                                                            <input type="text" class="form-control" id="pengurus[1]" name="pengurus[1][nama_pengurus]" placeholder="Sutansyah Marahakim">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="ktp_pengurus[1]" class="col-md-5">No KTP</label>
                                                        <div class="col-md-19">
                                                            <input type="text" class="form-control" id="ktp_pengurus[1]" name="pengurus[1][no_ktp]" placeholder="1234567890" onKeyPress="return good_chars(event, '0123456789', this)">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="npwp_pengurus[1]" class="col-md-5">No NPWP</label>
                                                        <div class="col-md-19">
                                                            <input type="text" class="form-control" id="npwp_pengurus[1]" name="pengurus[1][no_npwp]" placeholder="1234567890" onKeyPress="return good_chars(event, '0123456789', this)">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="alamat_pengurus[1]" class="col-md-5">Alamat Sesuai KTP</label>
                                                        <div class="col-md-19">
                                                            <input type="text" class="form-control" id="alamat_pengurus[1]" name="pengurus[1][alamat]" placeholder="Jl. ABC No. 123">
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } else { ?>
                                                <?php
                                                $no = 1;
                                                foreach ($pengurus_perusahaan as $pengurus) {
                                                    ?>
                                                    <div class="item-pengurus">
                                                        <div class="item-label">Pengurus Perusahaan <?php echo $no; ?></div>
                                                        <hr>
                                                        <?php if ($no > 2) { ?>
                                                            <div class="pull-right">
                                                                <div class="btn btn-default btn-delete-pengurus"><i class="fa fa-trash"></i></div>
                                                            </div>
                                                        <?php } ?>
                                                        <div class="form-group">
                                                            <label for="jenis_jabatan[<?php echo $no - 1; ?>]" class="col-md-5">Jenis Jabatan</label>
                                                            <div class="col-md-19">
                                                                <select class="form-control" id="jenis_jabatan[<?php echo $no - 1; ?>]" disabled="">
                                                                    <?php if ($pengurus->jenis_jabatan == 'direksi') { ?>
                                                                        <option value="direksi">Dewan Direksi</option>
                                                                    <?php } ?>
                                                                    <?php if ($pengurus->jenis_jabatan == 'komisaris') { ?>
                                                                        <option value="komisaris" selected>Dewan Komisaris</option>
                                                                    <?php } ?>
                                                                </select>
                                                            </div>
                                                            <input type="hidden" name="pengurus[<?php echo $no - 1; ?>][jenis_jabatan]" value="<?php echo isset($pengurus->jenis_jabatan) ? $pengurus->jenis_jabatan : '' ?>">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="jabatan[<?php echo $no - 1; ?>]" class="col-md-5">Nama Jabatan</label>
                                                            <div class="col-md-19">
                                                                <input type="text" class="form-control" id="jabatan[<?php echo $no - 1; ?>]" name="pengurus[<?php echo $no - 1; ?>][nama_jabatan]" placeholder="Komisaris" value="<?php echo isset($pengurus->nama_jabatan) ? $pengurus->nama_jabatan : '' ?>">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="pengurus[<?php echo $no - 1; ?>]" class="col-md-5">Nama Pengurus</label>
                                                            <div class="col-md-19">
                                                                <input type="text" class="form-control" id="pengurus[<?php echo $no - 1; ?>]" name="pengurus[<?php echo $no - 1; ?>][nama_pengurus]" placeholder="Sutansyah Marahakim" value="<?php echo isset($pengurus->nama_pengurus) ? $pengurus->nama_pengurus : '' ?>">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="ktp_pengurus[<?php echo $no - 1; ?>]" class="col-md-5">No KTP</label>
                                                            <div class="col-md-19">
                                                                <input type="text" class="form-control" id="ktp_pengurus[<?php echo $no - 1; ?>]" name="pengurus[<?php echo $no - 1; ?>][no_ktp]" placeholder="1234567890" onKeyPress="return good_chars(event, '0123456789', this)" value="<?php echo isset($pengurus->no_ktp) ? $pengurus->no_ktp : '' ?>">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="npwp_pengurus[<?php echo $no - 1; ?>]" class="col-md-5">No NPWP</label>
                                                            <div class="col-md-19">
                                                                <input type="text" class="form-control" id="npwp_pengurus[<?php echo $no - 1; ?>]" name="pengurus[<?php echo $no - 1; ?>][no_npwp]" placeholder="1234567890" onKeyPress="return good_chars(event, '0123456789', this)" value="<?php echo isset($pengurus->no_npwp) ? $pengurus->no_npwp : '' ?>">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="alamat_pengurus[<?php echo $no - 1; ?>]" class="col-md-5">Alamat Sesuai KTP</label>
                                                            <div class="col-md-19">
                                                                <input type="text" class="form-control" id="alamat_pengurus[<?php echo $no - 1; ?>]" name="pengurus[<?php echo $no - 1; ?>][alamat]" placeholder="Jl. ABC No. 123" value="<?php echo isset($pengurus->alamat) ? $pengurus->alamat : '' ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php
                                                    $no++;
                                                }
                                                ?>
                                            <?php } ?>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <div class="btn-group dropup">
                                                    <div id="btn-add-pengurus-perusahaan" class="btn btn-link pull-left dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <span class="fa fa-stack">
                                                            <i class="fa fa-circle"></i>
                                                            <i class="fa fa-plus-circle"></i>   
                                                        </span>
                                                        Tambah Pengurus Perusahaan
                                                    </div>
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li class="btn-add-pengurus-perusahaan" data-type="direksi"><div>Dewan Direksi</div></li>
                                                        <li class="btn-add-pengurus-perusahaan" data-type="komisaris"><div>Dewan Komisaris</div></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <button type="submit" class="btn btn-primary pull-right">SIMPAN</button>
                                            </div>
                                        </div>
                                    </div>
                                    </form>
                                </div>
                            </div>

                        </div>

                    </div>
                    <div class="col-md-6 hidden-sm hidden-xs faq-container">
                        <div class="panel-faq">
                            <div class="text-center"><strong><i>Daftar Pertanyaan Umum</i></strong></div>
                            <div id="faq-organ" data-simplebar>
                                <ol>
                                    <?php
                                    if ($faq) {
                                        foreach ($faq as $item) {
                                            if ($item->kategori == 'organ') {
                                                ?>
                                                <li><?php echo $item->tanya ?></li>
                                                <?php echo $item->jawab ?>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading collapsed" role="tab" id="headingFive" data-toggle="collapse" data-parent="#acordion-data-diri" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
            <div class="row">
                <div class="col-md-18 col-sm-24 title-container">
                    <h4 class="panel-title">
                        5. Tentukan Bidang Usaha <span class="pull-right"><i class="fa fa-caret-down"></i>
                    </h4>
                </div>
                <div class="col-md-6 hidden-sm hidden-xs info-container text-center">
                    <div class="panel-info">
                        <span class="text-success"><strong>Everything's Good!</strong></span>
                    </div>
                </div>
            </div>
        </div>
        <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-18 col-sm-24 data-container form-6">
                        <?php echo form_open(site_url('client/home/submit_bidang'), array('class' => 'form-horizontal')); ?>
                        <input type="hidden" name="id_perusahaan" value="<?php echo $perusahaan->kode; ?>">
                        <div class="form-container">    
                            <div class="form-group textarea">
                                <label for="bidang_usaha" class="col-md-5">Bidang Usaha</label>
                                <div class="col-md-19">
                                    <textarea class="form-control" id="bidang_usaha" name="bidang" placeholder="Bidang usaha PT ini"><?php echo isset($perusahaan->bidang) ? $perusahaan->bidang : ''; ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-24">
                                    <button type="submit" class="btn btn-primary pull-right">SIMPAN</button>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                    <div class="col-md-6 hidden-sm hidden-xs faq-container">
                        <div class="panel-faq">
                            <div class="text-center"><strong><i>Daftar Pertanyaan Umum</i></strong></div>
                            <div id="faq-bidang" data-simplebar>
                                <ol>
                                    <?php
                                    if ($faq) {
                                        foreach ($faq as $item) {
                                            if ($item->kategori == 'bidang') {
                                                ?>
                                                <li><?php echo $item->tanya ?></li>
                                                <?php echo $item->jawab ?>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>