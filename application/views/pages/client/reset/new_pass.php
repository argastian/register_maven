<div class="new_pass">
    <?php
    if ($this->session->flashdata('msg')) {
        echo $this->session->flashdata('msg');
    }
    ?>
    <p>Masukan password baru Anda</p>
    <?php echo form_open('', array('method' => 'POST')); ?>
    <input type="password" name="password" placeholder="Buat Password Baru" required="required" />
    <input type="password" name="retype_password" placeholder="Ulangi Password" required="required" />
    <input type="hidden" name="token" value="<?php echo $token ?>">
    <button type="submit" class="btn btn-primary btn-block">Proses</button>
</form>
</div>