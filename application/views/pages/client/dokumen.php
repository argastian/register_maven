<div class="panel-group menu-acordion" id="acordion-data-diri" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne" data-toggle="collapse" data-parent="#acordion-data-diri" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
            <div class="row">
                <div class="col-md-18 col-sm-24 title-container">
                    <h4 class="panel-title">
                        Upload Kelengkapan File Organ <span class="pull-right"><i class="fa fa-caret-up"></i></span>
                    </h4>
                </div>
                <div class="col-md-6 hidden-sm hidden-xs info-container text-center">
                    <div class="panel-info hide">
                        <span class="text-success"><strong>Everything's Good!</strong></span>
                    </div>
                </div>
            </div>
        </div>
        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-18 col-sm-24 data-container">
                        <?php echo form_open_multipart(site_url('client/dokumen/submit_file_organ'), array('class' => 'form-horizontal')); ?>
                        <input type="hidden" name="id_client" value="<?php echo $login_data['kode']; ?>">
                        <div class="form-container">
                            <div id="container-pemegang-saham">
                                <?php
                                $no = 0;
                                $dokumen_pemegang_saham = array();
                                $dokumen_pengurus_perusahaan = array();
                                $dokumen_lain = array();
                                foreach ($file_dokumen as $file) {
                                    if ($file->kode_pemegang_saham) {
                                        $dokumen_pemegang_saham[$file->kode_pemegang_saham][$file->tipe] = str_replace("./doc/" . $login_data['kode'] . "/" . $file->kode_pemegang_saham . "/", "", $file->nama_file);
                                    } else if ($file->kode_pengurus_perusahaan) {
                                        $dokumen_pengurus_perusahaan[$file->kode_pengurus_perusahaan][$file->tipe] = str_replace("./doc/" . $login_data['kode'] . "/" . $file->kode_pengurus_perusahaan . "/", "", $file->nama_file);
                                    } else {
                                        $dokumen_lain[$file->tipe] = str_replace("./doc/" . $login_data['kode'] . "/", "", $file->nama_file);
                                    }
                                }
                                if (isset($pemegang_saham_perseorangan)) {
                                    foreach ($pemegang_saham_perseorangan as $saham_perseorangan) {
                                        ?>
                                        <div class="item-pemegang-saham">
                                            <div class="item-label">Pemegang Saham : <?php echo $saham_perseorangan->nama; ?></div>
                                            <hr>
                                            <div class="form-group input-file">
                                                <label for="ktp_pemegang_saham[<?php echo $saham_perseorangan->kode; ?>]" class="col-md-9"><?php
                                                    $no++;
                                                    echo $no
                                                    ?>. KTP Pemegang Saham</label>
                                                <div class="input-file-container col-md-15">
                                                    <input type="text" class="form-control" readonly="" value="<?php echo isset($dokumen_pemegang_saham[$saham_perseorangan->kode]['ktp_pemegang_saham']) ? $dokumen_pemegang_saham[$saham_perseorangan->kode]['ktp_pemegang_saham'] : '' ?>">
                                                    <label class="btn-bs-file btn btn-primary">
                                                        UPLOAD
                                                        <input type="file" class="btn_select_file" name="ktp_pemegang_saham[<?php echo $saham_perseorangan->kode; ?>]" id="ktp_pemegang_saham[<?php echo $saham_perseorangan->kode; ?>]" accept=".jpg, .png, .jpeg, .gif, .pdf"/>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group input-file">
                                                <label for="npwp_pemegang_saham[<?php echo $saham_perseorangan->kode; ?>]" class="col-md-9"><?php
                                                    $no++;
                                                    echo $no
                                                    ?>. NPWP Pemegang Saham</label>
                                                <div class="input-file-container col-md-15">
                                                    <input type="text" class="form-control" readonly="" value="<?php echo isset($dokumen_pemegang_saham[$saham_perseorangan->kode]['npwp_pemegang_saham']) ? $dokumen_pemegang_saham[$saham_perseorangan->kode]['npwp_pemegang_saham'] : '' ?>">
                                                    <label class="btn-bs-file btn btn-primary">
                                                        UPLOAD
                                                        <input type="file" class="btn_select_file" name="npwp_pemegang_saham[<?php echo $saham_perseorangan->kode; ?>]" id="npwp_pemegang_saham[<?php echo $saham_perseorangan->kode; ?>]" accept=".jpg, .png, .jpeg, .gif, .pdf"/>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group input-file">
                                                <label for="kk_pemegang_saham[<?php echo $saham_perseorangan->kode; ?>]" class="col-md-9"><?php
                                                    $no++;
                                                    echo $no
                                                    ?>. Kartu Keluarga Pemegang Saham</label>
                                                <div class="input-file-container col-md-15">
                                                    <input type="text" class="form-control" readonly="" value="<?php echo isset($dokumen_pemegang_saham[$saham_perseorangan->kode]['kk_pemegang_saham']) ? $dokumen_pemegang_saham[$saham_perseorangan->kode]['kk_pemegang_saham'] : '' ?>">
                                                    <label class="btn-bs-file btn btn-primary">
                                                        UPLOAD
                                                        <input type="file" class="btn_select_file" name="kk_pemegang_saham[<?php echo $saham_perseorangan->kode; ?>]" id="kk_pemegang_saham[<?php echo $saham_perseorangan->kode; ?>]" accept=".jpg, .png, .jpeg, .gif, .pdf"/>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                }
                                if (isset($pengurus_perusahaan)) {
                                    foreach ($pengurus_perusahaan as $pengurus) {
                                        ?>
                                        <div class="item-pemegang-saham">
                                            <div class="item-label">Pengurus Perusahaan : <?php echo $pengurus->nama_pengurus; ?></div>
                                            <hr>
                                            <div class="form-group input-file">
                                                <label for="ktp_pengurus_perusahaan[<?php echo $pengurus->kode; ?>]" class="col-md-9"><?php
                                                    $no++;
                                                    echo $no
                                                    ?>. KTP Pengurus Perusahaan</label>
                                                <div class="input-file-container col-md-15">
                                                    <input type="text" class="form-control" readonly="" value="<?php echo isset($dokumen_pengurus_perusahaan[$pengurus->kode]['ktp_pengurus_perusahaan']) ? $dokumen_pengurus_perusahaan[$pengurus->kode]['ktp_pengurus_perusahaan'] : '' ?>">
                                                    <label class="btn-bs-file btn btn-primary">
                                                        UPLOAD
                                                        <input type="file" class="btn_select_file" name="ktp_pengurus_perusahaan[<?php echo $pengurus->kode; ?>]" id="ktp_pengurus_perusahaan[<?php echo $pengurus->kode; ?>]" accept=".jpg, .png, .jpeg, .gif, .pdf"/>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group input-file">
                                                <label for="npwp_pengurus_perusahaan[<?php echo $pengurus->kode; ?>]" class="col-md-9"><?php
                                                    $no++;
                                                    echo $no
                                                    ?>. NPWP Pengurus Perusahaan</label>
                                                <div class="input-file-container col-md-15">
                                                    <input type="text" class="form-control" readonly="" value="<?php echo isset($dokumen_pengurus_perusahaan[$pengurus->kode]['npwp_pengurus_perusahaan']) ? $dokumen_pengurus_perusahaan[$pengurus->kode]['npwp_pengurus_perusahaan'] : '' ?>">
                                                    <label class="btn-bs-file btn btn-primary">
                                                        UPLOAD
                                                        <input type="file" class="btn_select_file" name="npwp_pengurus_perusahaan[<?php echo $pengurus->kode; ?>]" id="npwp_pengurus_perusahaan[<?php echo $pengurus->kode; ?>]" accept=".jpg, .png, .jpeg, .gif, .pdf"/>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group input-file">
                                                <label for="kk_pengurus_perusahaan[<?php echo $pengurus->kode; ?>]" class="col-md-9"><?php
                                                    $no++;
                                                    echo $no
                                                    ?>. Kartu Keluarga Pengurus Perusahaan</label>
                                                <div class="input-file-container col-md-15">
                                                    <input type="text" class="form-control" readonly="" value="<?php echo isset($dokumen_pengurus_perusahaan[$pengurus->kode]['kk_pengurus_perusahaan']) ? $dokumen_pengurus_perusahaan[$pengurus->kode]['kk_pengurus_perusahaan'] : '' ?>">
                                                    <label class="btn-bs-file btn btn-primary">
                                                        UPLOAD
                                                        <input type="file" class="btn_select_file" name="kk_pengurus_perusahaan[<?php echo $pengurus->kode; ?>]" id="kk_pengurus_perusahaan[<?php echo $pengurus->kode; ?>]" accept=".jpg, .png, .jpeg, .gif, .pdf"/>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                }
                                ?>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-24">
                                    <button type="submit" class="btn btn-primary pull-right">SIMPAN</button>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                    <div class="col-md-6 hidden-sm hidden-xs faq-container">
                        <div class="panel-faq">
                            <div class="text-center"><strong><i>Daftar Pertanyaan Umum</i></strong></div>
                            <div id="faq-nama-tujuan" data-simplebar>
                                <ol>
                                    <?php
                                    if ($faq) {
                                        foreach ($faq as $item) {
                                            if ($item->kategori == 'file_organ') {
                                                ?>
                                                <li><?php echo $item->tanya ?></li>
                                                <?php echo $item->jawab ?>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading collapsed" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#acordion-data-diri" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
            <div class="row">
                <div class="col-md-18 col-sm-24 title-container">
                    <h4 class="panel-title">
                        Upload Kelengkapan Berkas Domisili <span class="pull-right"><i class="fa fa-caret-down"></i>
                    </h4>
                </div>
                <div class="col-md-6 hidden-sm hidden-xs info-container text-center">
                    <div class="panel-info">
                        <span class="text-success"><strong>Everything's Good!</strong></span>
                    </div>
                </div>
            </div>
        </div>
        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-18 col-sm-24 data-container">
                        <?php echo form_open_multipart(site_url('client/dokumen/submit_file_domisili'), array('class' => 'form-horizontal')); ?>
                        <input type="hidden" name="id_client" value="<?php echo $login_data['kode']; ?>">
                        <div class="form-container">    
                            <div class="form-group input-file">
                                <label for="domisili_gedung" class="col-md-9">1. Surat Domisili Gedung</label>
                                <div class="input-file-container col-md-15">
                                    <input type="text" class="form-control" readonly="" value="<?php echo isset($dokumen_lain['domisili_gedung']) ? $dokumen_lain['domisili_gedung'] : '' ?>">
                                    <label class="btn-bs-file btn btn-primary">
                                        UPLOAD
                                        <input type="file" class="btn_select_file" name="domisili_gedung" id="domisili_gedung" accept=".jpg, .png, .jpeg, .gif, .pdf"/>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group input-file">
                                <label for="kontrak_virtual_office" class="col-md-9">2. Kontrak Virtual Office</label>
                                <div class="input-file-container col-md-15">
                                    <input type="text" class="form-control" readonly="" value="<?php echo isset($dokumen_lain['kontrak_virtual_office']) ? $dokumen_lain['kontrak_virtual_office'] : '' ?>">
                                    <label class="btn-bs-file btn btn-primary">
                                        UPLOAD
                                        <input type="file" class="btn_select_file" name="kontrak_virtual_office" id="kontrak_virtual_office" accept=".jpg, .png, .jpeg, .gif, .pdf"/>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-24">
                                    <button type="submit" class="btn btn-primary pull-right">SIMPAN</button>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                    <div class="col-md-6 hidden-sm hidden-xs faq-container">
                        <div class="panel-faq">
                            <div class="text-center"><strong><i>Daftar Pertanyaan Umum</i></strong></div>
                            <div id="faq-domisili" data-simplebar>
                                <ol>
                                    <?php
                                    if ($faq) {
                                        foreach ($faq as $item) {
                                            if ($item->kategori == 'file_domisili') {
                                                ?>
                                                <li><?php echo $item->tanya ?></li>
                                                <?php echo $item->jawab ?>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading collapsed" role="tab" id="headingThree" data-toggle="collapse" data-parent="#acordion-data-diri" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
            <div class="row">
                <div class="col-md-18 col-sm-24 title-container">
                    <h4 class="panel-title">
                        Upload Surat Keterangan Lainnya <span class="pull-right"><i class="fa fa-caret-down"></i>
                    </h4>
                </div>
                <div class="col-md-6 hidden-sm hidden-xs info-container text-center">
                    <div class="panel-info">
                        <span class="text-success"><strong>Everything's Good!</strong></span>
                    </div>
                </div>
            </div>
        </div>
        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-18 col-sm-24 data-container">

                        <?php echo form_open_multipart(site_url('client/dokumen/submit_file_lain'), array('class' => 'form-horizontal')); ?>
                        <input type="hidden" name="id_client" value="<?php echo $login_data['kode']; ?>">

                        <div class="form-container">
                            <div class="form-group">
                                <div class="col-sm-8">
                                    <strong>Langkah 1</strong>
                                    <div class="deskripsi">Download template dokumen di bawah dan print di kertas</div>
                                </div>
                                <div class="col-sm-8">
                                    <strong>Langkah 2</strong>
                                    <div class="deskripsi">Tanda tangani semua berkas di tempat yang telah ditandai</div>
                                </div>
                                <div class="col-sm-8">
                                    <strong>Langkah 3</strong>
                                    <div class="deskripsi">Scan / foto dokumen yang telah di tanda tangani dan upload di sini</div>
                                </div>
                            </div>
                            <div class="form-group input-file">
                                <label for="nama_file_domisili" class="col-lg-8 col-sm-24">1. Surat Pernyataan Setoran Modal</label>
                                <div class="col-lg-7 col-sm-9 col-xs-8">
                                    <a href="<?php echo site_url('client/dokumen/template/setoran_modal') ?>" class="btn btn-info btn-block btn-download-file">DOWNLOAD <span class="hidden-xs">TEMPLATE</span></a>
                                </div>
                                <div class="input-file-container col-lg-9 col-sm-15 col-xs-16">
                                    <input type="text" class="form-control" readonly="" value="<?php echo isset($dokumen_lain['setoran_modal']) ? $dokumen_lain['setoran_modal'] : '' ?>">
                                    <label class="btn-bs-file btn btn-primary">
                                        UPLOAD
                                        <input type="file" class="btn_select_file" name="setoran_modal" id="setoran_modal[<?php echo $pengurus->kode; ?>]" accept=".jpg, .png, .jpeg, .gif, .pdf"/>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group input-file">
                                <label for="pernyataan_keabsahan[<?php echo $pengurus->kode; ?>]" class="col-lg-8 col-sm-24">2. Pernyataan Keabsahan</label>
                                <div class="col-lg-7 col-sm-9 col-xs-8">
                                    <a href="<?php echo site_url('client/dokumen/template/pernyataan_keabsahan') ?>" class="btn btn-info btn-block btn-download-file">DOWNLOAD TEMPLATE</a>
                                </div>
                                <div class="input-file-container col-lg-9 col-sm-15 col-xs-16">
                                    <input type="text" class="form-control" readonly="" value="<?php echo isset($dokumen_lain['pernyataan_keabsahan']) ? $dokumen_lain['pernyataan_keabsahan'] : '' ?>">
                                    <label class="btn-bs-file btn btn-primary">
                                        UPLOAD
                                        <input type="file" class="btn_select_file" name="pernyataan_keabsahan" id="pernyataan_keabsahan[<?php echo $pengurus->kode; ?>]" accept=".jpg, .png, .jpeg, .gif, .pdf"/>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group input-file">
                                <label for="pernyataan_domisili[<?php echo $pengurus->kode; ?>]" class="col-lg-8 col-sm-24">3. Pernyataan Domisili</label>
                                <div class="col-lg-7 col-sm-9 col-xs-8">
                                    <a href="<?php echo site_url('client/dokumen/template/pernyataan_domisili') ?>" class="btn btn-info btn-block btn-download-file">DOWNLOAD TEMPLATE</a>
                                </div>
                                <div class="input-file-container col-lg-9 col-sm-15 col-xs-16">
                                    <input type="text" class="form-control" readonly="" value="<?php echo isset($dokumen_lain['pernyataan_domisili']) ? $dokumen_lain['pernyataan_domisili'] : '' ?>">
                                    <label class="btn-bs-file btn btn-primary">
                                        UPLOAD
                                        <input type="file" class="btn_select_file" name="pernyataan_domisili" id="pernyataan_domisili[<?php echo $pengurus->kode; ?>]" accept=".jpg, .png, .jpeg, .gif, .pdf"/>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group input-file">
                                <label for="kuasa_hadap[<?php echo $pengurus->kode; ?>]" class="col-lg-8 col-sm-24">4. Kuasa Hadap (opsional)</label>
                                <div class="col-lg-7 col-sm-9 col-xs-8">
                                    <a href="<?php echo site_url('client/dokumen/template/kuasa_hadap') ?>" class="btn btn-info btn-block btn-download-file">DOWNLOAD TEMPLATE</a>
                                </div>
                                <div class="input-file-container col-lg-9 col-sm-15 col-xs-16">
                                    <input type="text" class="form-control" readonly="" value="<?php echo isset($dokumen_lain['kuasa_hadap']) ? $dokumen_lain['kuasa_hadap'] : '' ?>">
                                    <label class="btn-bs-file btn btn-primary">
                                        UPLOAD
                                        <input type="file" class="btn_select_file" name="kuasa_hadap" id="kuasa_hadap[<?php echo $pengurus->kode; ?>]" accept=".jpg, .png, .jpeg, .gif, .pdf"/>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-24">
                                    <button type="submit" class="btn btn-primary pull-right">SIMPAN</button>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <strong>Langkah 4</strong>
                                    <div class="deskripsi">Kirimkan hard copy dari dokumen yang sudah ditanda tangani ke kantor kami di alamat:</div>
                                </div>
                                <div class="col-sm-12">
                                    <br>
                                    <div class="deskripsi"><?php echo $config_app->alamat_kantor ?></div>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                    <div class="col-md-6 hidden-sm hidden-xs faq-container">
                        <div class="panel-faq">
                            <div class="text-center"><strong><i>Daftar Pertanyaan Umum</i></strong></div>
                            <div id="faq-modal" data-simplebar>
                                <ol>
                                    <?php
                                    if ($faq) {
                                        foreach ($faq as $item) {
                                            if ($item->kategori == 'file_lain') {
                                                ?>
                                                <li><?php echo $item->tanya ?></li>
                                                <?php echo $item->jawab ?>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>