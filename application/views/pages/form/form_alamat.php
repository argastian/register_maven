<div id="form-2" class="form-maven hide">
    <div class="form-title">
        <div class="form-title-image" style="background-image: url(<?php echo base_url('assets/images/form_2.png'); ?>)">
        </div>
        <div class="form-title-text">
            <h4>Tentukan Alamat Domisili</h4>
            <p>Masukkan alamat usahamu dengan detail. Jika kamu belum memiliki alamat kantor, kami menyediakan pilihan <strong>Virtual Office</strong> yang kamu bisa gunakan. <a href="<?php echo ($config_app->faq_alamat) ? $config_app->faq_alamat : '#'; ?>" target="<?php echo ($config_app->faq_alamat) ? '_BLANK' : ''; ?>">FAQ</a></p>
        </div>
    </div>
    <div class="form-container">
        <div>
            <ul class="nav nav-tabs" id="tab-location">
                <div class="liner"></div>
                <li class="<?php echo ((isset($perusahaan->kepemilikan) && $perusahaan->kepemilikan == 'virtual_office') || !isset($perusahaan->kepemilikan)) ? 'active' : '' ?>">
                    <a href="#virtual-office" title="Virtual Office">
                        <span class="round-tabs">
                            Virtual Office
                        </span>
                    </a>
                </li>
                <li class="<?php echo (isset($perusahaan->kepemilikan) && $perusahaan->kepemilikan == 'sewa') ? 'active' : '' ?>">
                    <a href="#sewa" title="Sewa">
                        <span class="round-tabs">
                            Sewa
                        </span> 
                    </a>
                </li>
                <li class="<?php echo (isset($perusahaan->kepemilikan) && $perusahaan->kepemilikan == 'milik_sendiri') ? 'active' : '' ?>">
                    <a href="#milik-sendiri" title="Milik Sendiri">
                        <span class="round-tabs">
                            Milik Sendiri
                        </span> 
                    </a>
                </li>
            </ul>
            <input type="hidden" name="kepemilikan" value="<?php echo isset($perusahaan->kepemilikan) ? $perusahaan->kepemilikan : 'virtual_office' ?>">
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane <?php echo ((isset($perusahaan->kepemilikan) && $perusahaan->kepemilikan == 'virtual_office') || !isset($perusahaan->kepemilikan)) ? 'active' : '' ?>" id="virtual-office">
                    <div class="form-group">
                        <label for="alamat_domisili_vo">Alamat Domisili</label>
                        <input type="text" class="form-control" id="alamat_domisili_vo" name="alamat[virtual_office][alamat_domisili]" placeholder="1635 Franklin Street Montgomery" value="<?php echo isset($perusahaan->alamat_domisili) ? $perusahaan->alamat_domisili : '' ?>">
                    </div>
                    <div class="form-group">
                        <label for="nama_gedung_vo">Nama Gedung <span class="label-description">(jika didalam gedung)</span></label>
                        <input type="text" class="form-control" id="nama_gedung_vo" name="alamat[virtual_office][nama_gedung]" placeholder="Prestige Dune Tower" value="<?php echo isset($perusahaan->nama_gedung) ? $perusahaan->nama_gedung : '' ?>">
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <label for="provinsi_vo">Provinsi</label>
                            <select class="form-control" id="provinsi_vo" name="alamat[virtual_office][provinsi]">
                                <option>Pilih Provinsi</option>
                                <?php
                                foreach ($provinsi as $prov) {
                                    $selected = (isset($perusahaan->provinsi) && ($perusahaan->provinsi == $prov->kode)) ? 'selected' : '';
                                    ?>
                                    <option value="<?php echo $prov->kode ?>" <?php echo $selected; ?>><?php echo display_nama_wilayah($prov->nama); ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="kota_madya_vo">Kota Madya</label>
                            <select class="form-control" id="kota_madya_vo" name="alamat[virtual_office][kota_madya]">
                                <option>Pilih Kota Madya</option>
                                <?php
                                if (isset($perusahaan->kota_madya)) {
                                    foreach ($kota_madya as $kota) {
                                        $selected = ($perusahaan->kota_madya == $kota->kode) ? 'selected' : '';
                                        ?>
                                        <option value="<?php echo $kota->kode ?>" <?php echo $selected; ?>><?php echo display_nama_wilayah($kota->nama); ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="row">                        
                        <div class="form-group col-sm-12">
                            <label for="kecamatan_vo">Kecamatan</label>
                            <select class="form-control" id="kecamatan_vo" name="alamat[virtual_office][kecamatan]">
                                <option>Pilih Kecamatan</option>
                                <?php
                                if (isset($perusahaan->kecamatan)) {
                                    foreach ($kecamatan as $kec) {
                                        $selected = ($perusahaan->kecamatan == $kec->kode) ? 'selected' : '';
                                        ?>
                                        <option value="<?php echo $kec->kode ?>" <?php echo $selected; ?>><?php echo display_nama_wilayah($kec->nama); ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="kelurahan_vo">Kelurahan</label>
                            <select class="form-control" id="kelurahan_vo" name="alamat[virtual_office][kelurahan]">
                                <option>Pilih Kelurahan</option>
                                <?php
                                if (isset($perusahaan->kelurahan)) {
                                    foreach ($kelurahan as $kel) {
                                        $selected = ($perusahaan->kelurahan == $kel->kode) ? 'selected' : '';
                                        ?>
                                        <option value="<?php echo $kel->kode ?>" <?php echo $selected; ?>><?php echo display_nama_wilayah($kel->nama); ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane <?php echo (isset($perusahaan->kepemilikan) && $perusahaan->kepemilikan == 'sewa') ? 'active' : '' ?>" id="sewa">
                    <div class="form-group">
                        <label for="alamat_domisili_sewa">Alamat Domisili</label>
                        <input type="text" class="form-control" id="alamat_domisili_sewa" name="alamat[sewa][alamat_domisili]" placeholder="1635 Franklin Street Montgomery" value="<?php echo isset($perusahaan->alamat_domisili) ? $perusahaan->alamat_domisili : '' ?>">
                    </div>
                    <div class="form-group">
                        <label for="nama_gedung_sewa">Nama Gedung <span class="label-description">(jika didalam gedung)</span></label>
                        <input type="text" class="form-control" id="nama_gedung_sewa" name="alamat[sewa][nama_gedung]" placeholder="Prestige Dune Tower" value="<?php echo isset($perusahaan->nama_gedung) ? $perusahaan->nama_gedung : '' ?>">
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <label for="provinsi_sewa">Provinsi</label>
                            <select class="form-control" id="provinsi_sewa" name="alamat[sewa][provinsi]">
                                <option>Pilih Provinsi</option>
                                <?php
                                foreach ($provinsi as $prov) {
                                    $selected = (isset($perusahaan->provinsi) && ($perusahaan->provinsi == $prov->kode)) ? 'selected' : '';
                                    ?>
                                    <option value="<?php echo $prov->kode ?>" <?php echo $selected; ?>><?php echo display_nama_wilayah($prov->nama); ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="kota_madya_sewa">Kota Madya</label>
                            <select class="form-control" id="kota_madya_sewa" name="alamat[sewa][kota_madya]">
                                <option>Pilih Kota Madya</option>
                                <?php
                                if (isset($perusahaan->kota_madya)) {
                                    foreach ($kota_madya as $kota) {
                                        $selected = ($perusahaan->kota_madya == $kota->kode) ? 'selected' : '';
                                        ?>
                                        <option value="<?php echo $kota->kode ?>" <?php echo $selected; ?>><?php echo display_nama_wilayah($kota->nama); ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <label for="kecamatan_sewa">Kecamatan</label>
                            <select class="form-control" id="kecamatan_sewa" name="alamat[sewa][kecamatan]">
                                <option>Pilih Kecamatan</option>
                                <?php
                                if (isset($perusahaan->kecamatan)) {
                                    foreach ($kecamatan as $kec) {
                                        $selected = ($perusahaan->kecamatan == $kec->kode) ? 'selected' : '';
                                        ?>
                                        <option value="<?php echo $kec->kode ?>" <?php echo $selected; ?>><?php echo display_nama_wilayah($kec->nama); ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="kelurahan_sewa">Kelurahan</label>
                            <select class="form-control" id="kelurahan_sewa" name="alamat[sewa][kelurahan]">
                                <option>Pilih Kelurahan</option>
                                <?php
                                if (isset($perusahaan->kelurahan)) {
                                    foreach ($kelurahan as $kel) {
                                        $selected = ($perusahaan->kelurahan == $kel->kode) ? 'selected' : '';
                                        ?>
                                        <option value="<?php echo $kel->kode ?>" <?php echo $selected; ?>><?php echo display_nama_wilayah($kel->nama); ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane<?php echo (isset($perusahaan->kepemilikan) && $perusahaan->kepemilikan == 'milik_sendiri') ? 'active' : '' ?>" id="milik-sendiri">
                    <div class="form-group">
                        <label for="alamat_domisili_sendiri">Alamat Domisili</label>
                        <input type="text" class="form-control" id="alamat_domisili_sendiri" name="alamat[milik_sendiri][alamat_domisili]" placeholder="1635 Franklin Street Montgomery" value="<?php echo isset($perusahaan->alamat_domisili) ? $perusahaan->alamat_domisili : '' ?>">
                    </div>
                    <div class="form-group">
                        <label for="nama_gedung_sendiri">Nama Gedung <span class="label-description">(jika didalam gedung)</span></label>
                        <input type="text" class="form-control" id="nama_gedung_sendiri" name="alamat[milik_sendiri][nama_gedung]" placeholder="Prestige Dune Tower" value="<?php echo isset($perusahaan->nama_gedung) ? $perusahaan->nama_gedung : '' ?>">
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <label for="provinsi_sendiri">Provinsi</label>
                            <select class="form-control" id="provinsi_sendiri" name="alamat[milik_sendiri][provinsi]">
                                <option>Pilih Provinsi</option>
                                <?php
                                foreach ($provinsi as $prov) {
                                    $selected = (isset($perusahaan->provinsi) && ($perusahaan->provinsi == $prov->kode)) ? 'selected' : '';
                                    ?>
                                    <option value="<?php echo $prov->kode ?>" <?php echo $selected; ?>><?php echo display_nama_wilayah($prov->nama); ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="kota_madya_sendiri">Kota Madya</label>
                            <select class="form-control" id="kota_madya_sendiri" name="alamat[milik_sendiri][kota_madya]">
                                <option>Pilih Kota Madya</option>
                                <?php
                                if (isset($perusahaan->kota_madya)) {
                                    foreach ($kota_madya as $kota) {
                                        $selected = ($perusahaan->kota_madya == $kota->kode) ? 'selected' : '';
                                        ?>
                                        <option value="<?php echo $kota->kode ?>" <?php echo $selected; ?>><?php echo display_nama_wilayah($kota->nama); ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <label for="kecamatan_sendiri">Kecamatan</label>
                            <select class="form-control" id="kecamatan_sendiri" name="alamat[milik_sendiri][kecamatan]">
                                <option>Pilih Kecamatan</option>
                                <?php
                                if (isset($perusahaan->kecamatan)) {
                                    foreach ($kecamatan as $kec) {
                                        $selected = ($perusahaan->kecamatan == $kec->kode) ? 'selected' : '';
                                        ?>
                                        <option value="<?php echo $kec->kode ?>" <?php echo $selected; ?>><?php echo display_nama_wilayah($kec->nama); ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="kelurahan_sendiri">Kelurahan</label>
                            <select class="form-control" id="kelurahan_sendiri" name="alamat[milik_sendiri][kelurahan]">
                                <option>Pilih Kelurahan</option>
                                <?php
                                if (isset($perusahaan->kelurahan)) {
                                    foreach ($kelurahan as $kel) {
                                        $selected = ($perusahaan->kelurahan == $kel->kode) ? 'selected' : '';
                                        ?>
                                        <option value="<?php echo $kel->kode ?>" <?php echo $selected; ?>><?php echo display_nama_wilayah($kel->nama); ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>