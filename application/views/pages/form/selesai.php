<div id="form-finish" class="hide">
    <p>
        Terima kasih telah melengkapi data data pendirian perusahaan anda, saat ini data anda
        akan di proses oleh tim kami. Tim kami akan menghubungi anda dalam waktu dekat.
    </p>
    <p>
        Untuk mengubah atau menambah data anda, bisa login di <a href="<?php echo site_url('client'); ?>">sini</a>.
    </p>
</div>