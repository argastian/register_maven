<div id="form-0" class="form-maven">
    <div class="form-title">
        <h4>Ayo Kita Mulai!</h4>
        <p>Sebelum melangkah lebih jauh, lengkapi data diri Anda terlebih dahuhlu. <a href="<?php echo ($config_app->faq_profile) ? $config_app->faq_profile : '#'; ?>" target="<?php echo ($config_app->faq_profile) ? '_BLANK' : ''; ?>">FAQ</a></p>
    </div>
    <div class="form-container">
        <div class="form-group">
            <label for="nama_lengkap">Nama Lengkap</label>                                    
            <input type="text" class="form-control" id="nama_lengkap" name="nama_lengkap" placeholder="Jhon Doe">
        </div>
        <div class="form-group">
            <label for="alamat_email">Alamat Email</label>
            <input type="email" class="form-control" id="alamat_email" name="alamat_email" placeholder="jhon.doe@example.com">
        </div>
        <div class="form-group">
            <label for="no_telpon">No Telpon</label>
            <input type="text" class="form-control" id="no_telpon" name="no_telpon" placeholder="6361905157" onKeyPress="return good_chars(event, '0123456789', this)">
        </div>
        <div class="form-group">
            <label for="password">Buat Password Baru <sup id="info-password" data-toggle="popover" title="Saran pembuatan password" data-content="Password minimal 6 karakter dengan kombinasi huruf kecil, huruf besar, angka dan karakter spesial." data-trigger="hover"><i class="fa fa-info-circle"></i></sup></label>
            <input type="password" class="form-control" id="password" name="password" placeholder="Buat Password Baru">
        </div>
        <div class="form-group">
            <label for="retype_password">Ulangi Password</label>
            <input type="password" class="form-control" id="retype_password" placeholder="Ulangi Password">
        </div>
    </div>
</div>