<div id="form-3" class="form-maven hide">
    <div class="form-title">
        <div class="form-title-image" style="background-image: url(<?php echo base_url('assets/images/form_3.png'); ?>)">
        </div>
        <div class="form-title-text">
            <h4>Tentukan Modal Perusahaanmu</h4>
            <p>Lengkapi struktur modal perusahaanmu secara detail dengan panduan dibawah ini. <a href="<?php echo ($config_app->faq_modal) ? $config_app->faq_modal : '#'; ?>" target="<?php echo ($config_app->faq_modal) ? '_BLANK' : ''; ?>">FAQ</a></p>
        </div>
    </div>
    <div class="form-container">
        <div class="row">
            <div class="form-group col-sm-24">
                <label for="modal_dasar">Modal Dasar</label>
                <div class="row">
                    <div class="col-sm-12">
                        <p>
                            adalah seluruh nilai nominal saham Perseroan yang disebut dalam Anggaran Dasar. Modal dasar pada prinsipnya merupakan total jumlah saham yang dapat diterbitkan oleh Perseroan.
                        </p>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    Rp
                                </div>
                                <input type="text" class="form-control" id="modal_dasar" name="modal_dasar" name="modal_dasar" placeholder="50.000.000" onKeyPress="return good_chars(event, '.0123456789', this)" value="<?php echo isset($perusahaan->modal_dasar) ? $perusahaan->modal_dasar : '' ?>">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group col-sm-24">
                <label for="modal_disetor">Modal Disetor & Ditempatkan</label>
                <div class="row">
                    <div class="col-sm-12">
                        <p>
                            jumlah saham yang sudah diambil pendiri atau pemegang saham. Dengan kata lain, modal ditempatkan itu adalah modal yang disanggupi pendiri atau pemegang saham untuk dilunasinya, dan saham itu telah diserahkan kepadanya untuk dimiliki.
                        </p>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    Rp
                                </div>
                                <input type="text" class="form-control" id="modal_disetor" name="modal_disetor" name="modal_disetor" placeholder="50.000.000" onKeyPress="return good_chars(event, '.0123456789', this)" value="<?php echo isset($perusahaan->modal_disetor) ? $perusahaan->modal_disetor : '' ?>">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group col-sm-24">
                <label for="nominal_saham">Nominal Saham/Lembar</label>
                <div class="row">
                    <div class="col-sm-12">
                        <p>
                            Nominal saham per lembar adalah harga pecahan saham per lembar yang dikeluarkan perusahaan.
                        </p>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    Rp
                                </div>
                                <input type="text" class="form-control" id="nominal_saham" name="nominal_saham" name="nominal_saham" placeholder="50.000.000" onKeyPress="return good_chars(event, '.0123456789', this)" value="<?php echo isset($perusahaan->nominal_saham) ? $perusahaan->nominal_saham : '' ?>">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>