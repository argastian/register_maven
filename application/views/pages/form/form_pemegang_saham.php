<div id="form-4" class="form-maven hide">
    <div class="form-title">
        <div class="form-title-image" style="background-image: url(<?php echo base_url('assets/images/form_4.png'); ?>)">
        </div>
        <div class="form-title-text">
            <h4>Tentukan Susunan Pemegang Saham</h4>
            <p>Lengkapi detail nama hingga jabatan setiap pemegang saham di perusahaanmu. <a href="<?php echo ($config_app->faq_pemegang_saham) ? $config_app->faq_pemegang_saham : '#'; ?>" target="<?php echo ($config_app->faq_pemegang_saham) ? '_BLANK' : ''; ?>">FAQ</a></p>
        </div>
    </div>
    <div class="form-container">
        <div id="container-pemegang-saham">
            <?php if (!isset($pemegang_saham_perseorangan) || !isset($pemegang_saham_perusahaan) || (isset($pemegang_saham_perseorangan) && $pemegang_saham_perseorangan == '') || (isset($pemegang_saham_perusahaan) && $pemegang_saham_perusahaan == '')) { ?>
                <div class="item-pemegang-saham">
                    <div class="item-label">Pemegang Saham 1</div>
                    <hr>               
                    <div class="form-group">
                        <label for="jenis_pemegang_saham[0]">Jenis Pemegang Saham</label>
                        <select class="form-control" id="jenis_pemegang_saham[0]" name="jenis_pemegang_saham[0]">
                            <option value="perseorangan">Perseorangan</option>
                            <option value="perusahaan">Perusahaan</option>
                        </select>
                    </div>
                    <div class="group-pemegang-saham saham-perseorangan">
                        <div class="row">
                            <div class="form-group col-sm-12">
                                <label for="pemegang_saham_perseorangan[0]">Nama Lengkap</label>
                                <input type="text" class="form-control" id="pemegang_saham_perseorangan[0]" name="pemegang_saham[perseorangan][0][nama]" placeholder="Sutansyah Marahakim">
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="ktp_pemegang_saham_perseorangan[0]">No KTP</label>
                                <input type="text" class="form-control" id="ktp_pemegang_saham_perseorangan[0]" name="pemegang_saham[perseorangan][0][no_ktp]" placeholder="1234567890" onKeyPress="return good_chars(event, '0123456789', this)">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-12">
                                <label for="alamat_pemegang_saham_perseorangan[0]">Alamat Sesuai KTP</label>
                                <input type="text" class="form-control" id="alamat_pemegang_saham_perseorangan[0]" name="pemegang_saham[perseorangan][0][alamat]" placeholder="Jl. ABC No. 123">
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="npwp_pemegang_saham_perseorangan[0]">No NPWP</label>
                                <input type="text" class="form-control" id="npwp_pemegang_saham_perseorangan[0]" name="pemegang_saham[perseorangan][0][no_npwp]" placeholder="1234567890" onKeyPress="return good_chars(event, '0123456789', this)">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-12">
                                <label for="persentase_pemegang_saham_perseorangan[0]">Jumlah Persentase Saham</label>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="persentase_pemegang_saham_perseorangan[0]" name="pemegang_saham[perseorangan][0][persentase_saham]" placeholder="30" onKeyPress="return good_chars(event, '.0123456789', this)">
                                        <div class="input-group-addon">
                                            %
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="group-pemegang-saham saham-perusahaan hide">
                        <div class="row">
                            <div class="form-group col-sm-12">
                                <label for="pemegang_saham_perusahaan[0]">Nama PT</label>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            PT
                                        </div>
                                        <input type="text" class="form-control" id="pemegang_saham_perusahaan[0]" name="pemegang_saham[perusahaan][0][nama]" placeholder="Kolaborasi Kapital Indonesia" onKeyPress="return prevent_number(event)">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="no_akta_perusahaan[0]">No Akta Pendirian</label>
                                <input type="text" class="form-control" id="no_akta_perusahaan[0]" name="pemegang_saham[perusahaan][0][no_akta]" placeholder="1234567890">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-12">
                                <label for="tanggal_pendirian_perusahaan[0]">Tanggal Pendirian</label>
                                <input type="text" class="form-control datepicker" id="tanggal_pendirian_perusahaan[0]" name="pemegang_saham[perusahaan][0][tanggal_pendirian]" placeholder="2018-01-11" data-date-max-date="now">
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="sk_menkumham_perusahaan[0]">No SK Menkumham</label>
                                <input type="text" class="form-control" id="sk_menkumham_perusahaan[0]" name="pemegang_saham[perusahaan][0][no_sk]" placeholder="1234567890">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-12">
                                <label for="npwp_pemegang_saham_perusahaan[0]">No NPWP Perusahaan</label>
                                <input type="text" class="form-control" id="npwp_pemegang_saham_perusahaan[0]" name="pemegang_saham[perusahaan][0][no_npwp]" placeholder="1234567890" onKeyPress="return good_chars(event, '0123456789', this)">
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="persentase_pemegang_saham_perusahaan[0]">Jumlah Persentase Saham</label>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="persentase_pemegang_saham_perusahaan[0]" name="pemegang_saham[perusahaan][0][persentase_saham]" placeholder="30" onKeyPress="return good_chars(event, '.0123456789', this)">
                                        <div class="input-group-addon">
                                            %
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } else { ?>
                <?php
                $no = 1;
                foreach ($pemegang_saham_perseorangan as $saham_perseorangan) {
                    ?>
                    <div class="item-pemegang-saham">
                        <div class="item-label">Pemegang Saham <?php echo $no ?></div>
                        <hr>
                        <?php if ($no > 2) { ?>
                            <div class="pull-right">
                                <div class="btn btn-default btn-delete-pemegang-saham"><i class="fa fa-trash"></i></div>
                            </div>
                        <?php } ?>
                        <div class="form-group">
                            <label for="jenis_pemegang_saham[<?php echo $no - 1; ?>]">Jenis Pemegang Saham</label>
                            <select class="form-control" id="jenis_pemegang_saham[<?php echo $no - 1; ?>]" name="jenis_pemegang_saham[<?php echo $no - 1; ?>]" disabled="">
                                <option value="perseorangan">Perseorangan</option>
                            </select>
                            <input type="hidden" name="jenis_pemegang_saham[<?php echo $no - 1; ?>]" value="perseorangan">
                        </div>
                        <div class="group-pemegang-saham saham-perseorangan">
                            <div class="row">
                                <div class="form-group col-sm-12">
                                    <label for="pemegang_saham_perseorangan[<?php echo $no - 1; ?>]">Nama Lengkap</label>
                                    <input type="text" class="form-control" id="pemegang_saham_perseorangan[<?php echo $no - 1; ?>]" name="pemegang_saham[perseorangan][<?php echo $no - 1; ?>][nama]" placeholder="Sutansyah Marahakim" value="<?php echo isset($saham_perseorangan->nama) ? $saham_perseorangan->nama : '' ?>">
                                </div>
                                <div class="form-group col-sm-12">
                                    <label for="ktp_pemegang_saham_perseorangan[<?php echo $no - 1; ?>]">No KTP</label>
                                    <input type="text" class="form-control" id="ktp_pemegang_saham_perseorangan[<?php echo $no - 1; ?>]" name="pemegang_saham[perseorangan][<?php echo $no - 1; ?>][no_ktp]" placeholder="1234567890" onKeyPress="return good_chars(event, '0123456789', this)" value="<?php echo isset($saham_perseorangan->no_ktp) ? $saham_perseorangan->no_ktp : '' ?>">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-sm-12">
                                    <label for="alamat_pemegang_saham_perseorangan[<?php echo $no - 1; ?>]">Alamat Sesuai KTP</label>
                                    <input type="text" class="form-control" id="alamat_pemegang_saham_perseorangan[<?php echo $no - 1; ?>]" name="pemegang_saham[perseorangan][<?php echo $no - 1; ?>][alamat]" placeholder="Jl. ABC No. 123" value="<?php echo isset($saham_perseorangan->alamat) ? $saham_perseorangan->alamat : '' ?>">
                                </div>
                                <div class="form-group col-sm-12">
                                    <label for="npwp_pemegang_saham_perseorangan[<?php echo $no - 1; ?>]">No NPWP</label>
                                    <input type="text" class="form-control" id="npwp_pemegang_saham_perseorangan[<?php echo $no - 1; ?>]" name="pemegang_saham[perseorangan][<?php echo $no - 1; ?>][no_npwp]" placeholder="1234567890" onKeyPress="return good_chars(event, '0123456789', this)" value="<?php echo isset($saham_perseorangan->no_npwp) ? $saham_perseorangan->no_npwp : '' ?>">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-sm-12">
                                    <label for="persentase_pemegang_saham_perseorangan[<?php echo $no - 1; ?>]">Jumlah Persentase Saham</label>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="persentase_pemegang_saham_perseorangan[<?php echo $no - 1; ?>]" name="pemegang_saham[perseorangan][<?php echo $no - 1; ?>][persentase_saham]" placeholder="30" onKeyPress="return good_chars(event, '.0123456789', this)" value="<?php echo isset($saham_perseorangan->persentase_saham) ? $saham_perseorangan->persentase_saham : '' ?>">
                                            <div class="input-group-addon">
                                                %
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                    $no++;
                }
                ?>
                <?php
                foreach ($pemegang_saham_perusahaan as $saham_perusahaan) {
                    ?>
                    <div class="item-pemegang-saham">
                        <div class="item-label">Pemegang Saham <?php echo $no ?></div>
                        <hr>    
                        <?php if ($no > 2) { ?>
                            <div class="pull-right">
                                <div class="btn btn-default btn-delete-pemegang-saham"><i class="fa fa-trash"></i></div>
                            </div>
                        <?php } ?>
                        <div class="form-group">
                            <label for="jenis_pemegang_saham[<?php echo $no - 1; ?>]">Jenis Pemegang Saham</label>
                            <select class="form-control" id="jenis_pemegang_saham[<?php echo $no - 1; ?>]" name="jenis_pemegang_saham[<?php echo $no - 1; ?>]" disabled="">
                                <option value="perusahaan">Perusahaan</option>
                            </select>
                            <input type="hidden" name="jenis_pemegang_saham[<?php echo $no - 1; ?>]" value="perusahaan">
                        </div>
                        <div class="group-pemegang-saham saham-perusahaan">
                            <div class="row">
                                <div class="form-group col-sm-12">
                                    <label for="pemegang_saham_perusahaan[<?php echo $no - 1; ?>]">Nama PT</label>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                PT
                                            </div>
                                            <input type="text" class="form-control" id="pemegang_saham_perusahaan[<?php echo $no - 1; ?>]" name="pemegang_saham[perusahaan][<?php echo $no - 1; ?>][nama]" placeholder="Kolaborasi Kapital Indonesia" onKeyPress="return prevent_number(event)" value="<?php echo isset($saham_perusahaan->nama) ? $saham_perusahaan->nama : '' ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-sm-12">
                                    <label for="no_akta_perusahaan[<?php echo $no - 1; ?>]">No Akta Pendirian</label>
                                    <input type="text" class="form-control" id="no_akta_perusahaan[<?php echo $no - 1; ?>]" name="pemegang_saham[perusahaan][<?php echo $no - 1; ?>][no_akta]" placeholder="1234567890" value="<?php echo isset($saham_perusahaan->no_akta) ? $saham_perusahaan->no_akta : '' ?>">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-sm-12">
                                    <label for="tanggal_pendirian_perusahaan[<?php echo $no - 1; ?>]">Tanggal Pendirian</label>
                                    <input type="text" class="form-control datepicker" id="tanggal_pendirian_perusahaan[<?php echo $no - 1; ?>]" name="pemegang_saham[perusahaan][<?php echo $no - 1; ?>][tanggal_pendirian]" placeholder="2018-01-11" data-date-max-date="now" value="<?php echo isset($saham_perusahaan->tanggal_pendirian) ? $saham_perusahaan->tanggal_pendirian : '' ?>">
                                </div>
                                <div class="form-group col-sm-12">
                                    <label for="sk_menkumham_perusahaan[<?php echo $no - 1; ?>]">No SK Menkumham</label>
                                    <input type="text" class="form-control" id="sk_menkumham_perusahaan[<?php echo $no - 1; ?>]" name="pemegang_saham[perusahaan][<?php echo $no - 1; ?>][no_sk]" placeholder="1234567890" value="<?php echo isset($saham_perusahaan->no_sk) ? $saham_perusahaan->no_sk : '' ?>">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-sm-12">
                                    <label for="npwp_pemegang_saham_perusahaan[<?php echo $no - 1; ?>]">No NPWP Perusahaan</label>
                                    <input type="text" class="form-control" id="npwp_pemegang_saham_perusahaan[<?php echo $no - 1; ?>]" name="pemegang_saham[perusahaan][<?php echo $no - 1; ?>][no_npwp]" placeholder="1234567890" onKeyPress="return good_chars(event, '0123456789', this)" value="<?php echo isset($saham_perusahaan->no_npwp) ? $saham_perusahaan->no_npwp : '' ?>">
                                </div>
                                <div class="form-group col-sm-12">
                                    <label for="persentase_pemegang_saham_perusahaan[<?php echo $no - 1; ?>]">Jumlah Persentase Saham</label>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="persentase_pemegang_saham_perusahaan[<?php echo $no - 1; ?>]" name="pemegang_saham[perusahaan][<?php echo $no - 1; ?>][persentase_saham]" placeholder="30" onKeyPress="return good_chars(event, '.0123456789', this)" value="<?php echo isset($saham_perusahaan->persentase_saham) ? $saham_perusahaan->persentase_saham : '' ?>">
                                            <div class="input-group-addon">
                                                %
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                    $no++;
                }
                ?>
            <?php }
            ?>
        </div>
        <div class="form-group">
            <div class="btn-group dropup">
                <div id="btn-add-pemegang-saham" class="btn btn-link pull-left dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="fa fa-stack">
                        <i class="fa fa-circle"></i>
                        <i class="fa fa-plus-circle"></i>   
                    </span>
                    Tambah Pemegang Saham
                </div>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li class="btn-add-pemegang-saham" data-type="perseorangan"><div>Perseorangan</div></li>
                    <li class="btn-add-pemegang-saham" data-type="perusahaan"><div>Perusahaan</div></li>
                </ul>
            </div>
        </div>
    </div>
</div>

