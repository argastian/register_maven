<div id="form-1" class="form-maven hide">
    <div class="form-title">
        <div class="form-title-image" style="background-image: url(<?php echo base_url('assets/images/form_1.png'); ?>)">
        </div>
        <div class="form-title-text">
            <h4>Tentukan Nama & Tujuan Perusahaanmu.</h4>
            <p>Pilihlah nama yang bagus dan sesuai, karena sebuah nama dapat merepresentasikan citra perusahaanmu. <a href="<?php echo ($config_app->faq_nama) ? $config_app->faq_nama : '#'; ?>" target="<?php echo ($config_app->faq_nama) ? '_BLANK' : ''; ?>">FAQ</a></p>
        </div>
    </div>
    <div class="form-container">
        <div class="form-group">
            <label for="nama_perusahaan_1">Pilih Nama 1</label>
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon">
                        PT
                    </div>
                    <input type="text" class="form-control" id="nama_perusahaan_1" name="nama_perusahaan_1" placeholder="Kolaborasi Kapital Indonesia" onKeyPress="return prevent_number(event)" value="<?php echo isset($perusahaan->nama_perusahaan_1) ? $perusahaan->nama_perusahaan_1 : ''; ?>">
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="nama_perusahaan_2">Pilih Nama 2</label>                                   
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon">
                        PT
                    </div>
                    <input type="text" class="form-control" id="nama_perusahaan_2" name="nama_perusahaan_2" placeholder="Kapital Kolaborasi Indonesia" onKeyPress="return prevent_number(event)" value="<?php echo isset($perusahaan->nama_perusahaan_1) ? $perusahaan->nama_perusahaan_2 : ''; ?>">
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="nama_perusahaan_3">Pilih Nama 3</label>
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon">
                        PT
                    </div>
                    <input type="text" class="form-control" id="nama_perusahaan_3" name="nama_perusahaan_3" placeholder="Kolaborasi Kapital Indonesia" onKeyPress="return prevent_number(event)" value="<?php echo isset($perusahaan->nama_perusahaan_1) ? $perusahaan->nama_perusahaan_3 : ''; ?>">
                </div>
            </div>
        </div>
        <div class="form-group textarea">
            <label for="tujuan_pendirian">Tujuan Perusahaan</label>
            <textarea class="form-control" id="tujuan_pendirian" name="tujuan_pendirian" placeholder="Jelaskan secara singkat bisnis yang akan Anda jalani menggunakan PT ini"><?php echo isset($perusahaan->nama_perusahaan_1) ? $perusahaan->tujuan_pendirian : ''; ?></textarea>
        </div>
    </div>
</div>