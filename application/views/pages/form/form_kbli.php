<div id="form-5" class="form-maven hide">
    <div class="form-title">
        <div class="form-title-image" style="background-image: url(<?php echo base_url('assets/images/form_5.png'); ?>)">
        </div>
        <div class="form-title-text">
            <h4>Tentukan Bidang Usaha</h4>
            <p>Pilih tipe bidang usaha yang sesuai dengan kegiatan perusahaanmu.</p>
        </div>
    </div>
    <div class="form-container">
        <div class="form-group">
            <label for="tujuan_pendirian">Tujuan Pendirian Perusahaan</label>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt.
            </p>
            <div class="form-group textarea">
                <textarea class="form-control" id="tujuan_pendirian" placeholder="Tujuan Perusahaan ini adalah"></textarea>
            </div>
        </div>
        <div class="form-group m-b-0">
            <label for="kbli[0]">Klasifikasi Baku Lapangan Usaha</label>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt.
            </p>
            <div class="row">
                <div id="container-kbli" class="col-sm-14 col-xs-12">
                    <div class="form-group item-kbli">
                        <input type="text" class="form-control" id="kbli[0]" placeholder="Pertanian">
                    </div>
                </div>
                <div class="col-sm-10 col-xs-12">
                    <div id="btn-add-kbli" class="btn btn-link pull-left">
                        <span class="fa fa-stack">
                            <i class="fa fa-circle"></i>
                            <i class="fa fa-plus-circle"></i>   
                        </span>
                        Tambah KBLI
                    </div>
                </div>  
            </div>
        </div>
    </div>
</div>