<div id="resume-form" class="hide" data-simplebar>
    <table class="table table-profil">
        <tr>
            <td><strong>Nama Lengkap</strong></td>
            <td id="col_nama_lengkap"><?php echo isset($client->nama_lengkap) ? $client->nama_lengkap : '' ?></td>
        </tr>
        <tr>
            <td><strong>Alamat Email</strong></td>
            <td id="col_alamat_email"><?php echo isset($client->alamat_email) ? $client->alamat_email : '' ?></td>
        </tr>
        <tr>
            <td><strong>No Telpon</strong></td>
            <td id="col_no_telpon"><?php echo isset($client->no_telpon) ? $client->no_telpon : '' ?></td>
        </tr>
    </table>
    <h4>Nama & Tujuan Perusahaan</h4>
    <table class="table table-bordered">        
        <tr>
            <td><strong>Nama 1</strong></td>
            <td id="col_nama_perusahaan_1"></td>
        </tr>
        <tr>
            <td><strong>Nama 2</strong></td>
            <td id="col_nama_perusahaan_2"></td>
        </tr>
        <tr>
            <td><strong>Nama 3</strong></td>
            <td id="col_nama_perusahaan_3"></td>
        </tr>
        <tr>
            <td><strong>Tujuan Perusahaan</strong></td>
            <td id="col_tujuan_pendirian"></td>
        </tr>
    </table>
    <h4>Alamat Domisili</h4>
    <table class="table table-bordered hide" id="table-domisili-vo">
        <tr>
            <td colspan="2" class="text-center"><strong>Virtual Office</strong></td>
        </tr>
        <tr>
            <td><strong>Alamat Domisili</strong></td>
            <td id="col_alamat_domisili_vo"></td>
        </tr>
        <tr>
            <td><strong>Nama Gedung</strong></td>
            <td id="col_nama_gedung_vo"></td>
        </tr>
        <tr>
            <td><strong>Provinsi</strong></td>
            <td id="col_provinsi_vo"></td>
        </tr>
        <tr>
            <td><strong>Kota Madya</strong></td>
            <td id="col_kota_madya_vo"></td>
        </tr>
        <tr>
            <td><strong>Kecamatan</strong></td>
            <td id="col_kecamatan_vo"></td>
        </tr>
        <tr>
            <td><strong>Kelurahan</strong></td>
            <td id="col_kelurahan_vo"></td>
        </tr>
    </table>
    <table class="table table-bordered hide" id="table-domisili-sewa">
        <tr>
            <td colspan="2" class="text-center"><strong>Sewa</strong></td>
        </tr>
        <tr>
            <td><strong>Alamat Domisili</strong></td>
            <td id="col_alamat_domisili_sewa"></td>
        </tr>
        <tr>
            <td><strong>Nama Gedung</strong></td>
            <td id="col_nama_gedung_sewa"></td>
        </tr>
        <tr>
            <td><strong>Provinsi</strong></td>
            <td id="col_provinsi_sewa"></td>
        </tr>
        <tr>
            <td><strong>Kota Madya</strong></td>
            <td id="col_kota_madya_sewa"></td>
        </tr>
        <tr>
            <td><strong>Kecamatan</strong></td>
            <td id="col_kecamatan_sewa"></td>
        </tr>
        <tr>
            <td><strong>Kelurahan</strong></td>
            <td id="col_kelurahan_sewa"></td>
        </tr>
    </table>
    <table class="table table-bordered hide" id="table-domisili-sendiri">
        <tr>
            <td colspan="2" class="text-center"><strong>Milik Sendiri</strong></td>
        </tr>
        <tr>
            <td><strong>Alamat Domisili</strong></td>
            <td id="col_alamat_domisili_sendiri"></td>
        </tr>
        <tr>
            <td><strong>Nama Gedung</strong></td>
            <td id="col_nama_gedung_sendiri"></td>
        </tr>
        <tr>
            <td><strong>Provinsi</strong></td>
            <td id="col_provinsi_sendiri"></td>
        </tr>
        <tr>
            <td><strong>Kota Madya</strong></td>
            <td id="col_kota_madya_sendiri"></td>
        </tr>
        <tr>
            <td><strong>Kecamatan</strong></td>
            <td id="col_kecamatan_sendiri"></td>
        </tr>
        <tr>
            <td><strong>Kelurahan</strong></td>
            <td id="col_kelurahan_sendiri"></td>
        </tr>
    </table>
    <h4>Modal</h4>
    <table class="table table-bordered">
        <tr>
            <td><strong>Modal Dasar</strong></td>
            <td id="col_modal_dasar" class="text-right"></td>
        </tr>
        <tr>
            <td><strong>Modal Disetor & Ditempatkan</strong></td>
            <td id="col_modal_disetor" class="text-right"></td>
        </tr>
        <tr>
            <td><strong>Nominal Saham/Lembar</strong></td>
            <td id="col_nominal_saham" class="text-right"></td>
        </tr>
        <tr>
            <td><strong>Jumlah Saham</strong></td>
            <td id="col_jumlah_saham" class="text-right"></td>
        </tr>
    </table>
    <h4>Pemegang Saham</h4>
    <div id="table-pemegang-saham">
    </div>
    <h4>Pengurus Perusahaan</h4>
    <div id="table-pengurus">
    </div>
</div>