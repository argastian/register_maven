<div id="form-5" class="form-maven hide">
    <div class="form-title">
        <div class="form-title-image" style="background-image: url(<?php echo base_url('assets/images/form_4.png'); ?>)">
        </div>
        <div class="form-title-text">
            <h4>Tentukan Susunan Pengurus Perusahaan</h4>
            <p>Lengkapi detail nama hingga jabatan setiap pengurus di perusahaanmu. <a href="<?php echo ($config_app->faq_pengurus) ? $config_app->faq_pengurus : '#'; ?>" target="<?php echo ($config_app->faq_pengurus) ? '_BLANK' : ''; ?>">FAQ</a></p>
        </div>
    </div>
    <div class="form-container">        
        <div id="container-pengurus">
            <?php if (!isset($pengurus_perusahaan) || isset($pengurus_perusahaan) && $pengurus_perusahaan == '') { ?>
                <div class="item-pengurus">
                    <div class="item-label">Pengurus Perusahaan 1</div>
                    <hr>
                    <div class="form-group">
                        <label for="jenis_jabatan[0]">Jenis Jabatan</label>
                        <select class="form-control" id="jenis_jabatan[0]" name="pengurus[0][jenis_jabatan]">
                            <option value="direksi">Dewan Direksi</option>
                            <option value="komisaris">Dewan Komisaris</option>
                        </select>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <label for="jabatan[0]">Nama Jabatan</label>
                            <input type="text" class="form-control" id="jabatan[0]" name="pengurus[0][nama_jabatan]" placeholder="Direktur">
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="pengurus[0]">Nama Pengurus</label>
                            <input type="text" class="form-control" id="pengurus[0]" name="pengurus[0][nama_pengurus]" placeholder="Sutansyah Marahakim">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <label for="ktp_pengurus[0]">No KTP</label>
                            <input type="text" class="form-control" id="ktp_pengurus[0]" name="pengurus[0][no_ktp]" placeholder="1234567890" onKeyPress="return good_chars(event, '0123456789', this)">
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="npwp_pengurus[0]">No NPWP</label>
                            <input type="text" class="form-control" id="npwp_pengurus[0]" name="pengurus[0][no_npwp]" placeholder="1234567890" onKeyPress="return good_chars(event, '0123456789', this)">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <label for="alamat_pengurus[0]">Alamat Sesuai KTP</label>
                            <input type="text" class="form-control" id="alamat_pengurus[0]" name="pengurus[0][alamat]" placeholder="Jl. ABC No. 123">
                        </div>
                    </div>
                </div>
                <div class="item-pengurus">
                    <div class="item-label">Pengurus Perusahaan 2</div>
                    <hr>
                    <div class="form-group">
                        <label for="jenis_jabatan[1]">Jenis Jabatan</label>
                        <select class="form-control" id="jenis_jabatan[1]" name="pengurus[1][jenis_jabatan]">
                            <option value="direksi">Dewan Direksi</option>
                            <option value="komisaris" selected>Dewan Komisaris</option>
                        </select>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <label for="jabatan[1]">Nama Jabatan</label>
                            <input type="text" class="form-control" id="jabatan[1]" name="pengurus[1][nama_jabatan]" placeholder="Komisaris">
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="pengurus[1]">Nama Pengurus</label>
                            <input type="text" class="form-control" id="pengurus[1]" name="pengurus[1][nama_pengurus]" placeholder="Sutansyah Marahakim">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <label for="ktp_pengurus[1]">No KTP</label>
                            <input type="text" class="form-control" id="ktp_pengurus[1]" name="pengurus[1][no_ktp]" placeholder="1234567890" onKeyPress="return good_chars(event, '0123456789', this)">
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="npwp_pengurus[1]">No NPWP</label>
                            <input type="text" class="form-control" id="npwp_pengurus[1]" name="pengurus[1][no_npwp]" placeholder="1234567890" onKeyPress="return good_chars(event, '0123456789', this)">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <label for="alamat_pengurus[1]">Alamat Sesuai KTP</label>
                            <input type="text" class="form-control" id="alamat_pengurus[1]" name="pengurus[1][alamat]" placeholder="Jl. ABC No. 123">
                        </div>
                    </div>
                </div>
            <?php } else { ?>
                <?php
                $no = 1;
                foreach ($pengurus_perusahaan as $pengurus) {
                    ?>
                    <div class="item-pengurus">
                        <div class="item-label">Pengurus Perusahaan <?php echo $no; ?></div>
                        <hr>
                        <?php if ($no > 2) { ?>
                            <div class="pull-right">
                                <div class="btn btn-default btn-delete-pengurus"><i class="fa fa-trash"></i></div>
                            </div>
                        <?php } ?>
                        <div class="form-group">
                            <label for="jenis_jabatan[<?php echo $no - 1; ?>]">Jenis Jabatan</label>
                            <select class="form-control" id="jenis_jabatan[<?php echo $no - 1; ?>]" disabled="">
                                <?php if ($pengurus->jenis_jabatan == 'direksi') { ?>
                                    <option value="direksi">Dewan Direksi</option>
                                <?php } ?>
                                <?php if ($pengurus->jenis_jabatan == 'komisaris') { ?>
                                    <option value="komisaris" selected>Dewan Komisaris</option>
                                <?php } ?>
                            </select>
                            <input type="hidden" name="pengurus[<?php echo $no - 1; ?>][jenis_jabatan]" value="<?php echo isset($pengurus->jenis_jabatan) ? $pengurus->jenis_jabatan : '' ?>">
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-12">
                                <label for="jabatan[<?php echo $no - 1; ?>]">Nama Jabatan</label>
                                <input type="text" class="form-control" id="jabatan[<?php echo $no - 1; ?>]" name="pengurus[<?php echo $no - 1; ?>][nama_jabatan]" placeholder="Komisaris" value="<?php echo isset($pengurus->nama_jabatan) ? $pengurus->nama_jabatan : '' ?>">
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="pengurus[<?php echo $no - 1; ?>]">Nama Pengurus</label>
                                <input type="text" class="form-control" id="pengurus[<?php echo $no - 1; ?>]" name="pengurus[<?php echo $no - 1; ?>][nama_pengurus]" placeholder="Sutansyah Marahakim" value="<?php echo isset($pengurus->nama_pengurus) ? $pengurus->nama_pengurus : '' ?>">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-12">
                                <label for="ktp_pengurus[<?php echo $no - 1; ?>]">No KTP</label>
                                <input type="text" class="form-control" id="ktp_pengurus[<?php echo $no - 1; ?>]" name="pengurus[<?php echo $no - 1; ?>][no_ktp]" placeholder="1234567890" onKeyPress="return good_chars(event, '0123456789', this)" value="<?php echo isset($pengurus->no_ktp) ? $pengurus->no_ktp : '' ?>">
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="npwp_pengurus[<?php echo $no - 1; ?>]">No NPWP</label>
                                <input type="text" class="form-control" id="npwp_pengurus[<?php echo $no - 1; ?>]" name="pengurus[<?php echo $no - 1; ?>][no_npwp]" placeholder="1234567890" onKeyPress="return good_chars(event, '0123456789', this)" value="<?php echo isset($pengurus->no_npwp) ? $pengurus->no_npwp : '' ?>">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-12">
                                <label for="alamat_pengurus[<?php echo $no - 1; ?>]">Alamat Sesuai KTP</label>
                                <input type="text" class="form-control" id="alamat_pengurus[<?php echo $no - 1; ?>]" name="pengurus[<?php echo $no - 1; ?>][alamat]" placeholder="Jl. ABC No. 123" value="<?php echo isset($pengurus->alamat) ? $pengurus->alamat : '' ?>">
                            </div>
                        </div>
                    </div>
                    <?php
                    $no++;
                }
                ?>
            <?php } ?>
        </div>
        <div class="form-group">
            <div class="btn-group dropup">
                <div id="btn-add-pengurus-perusahaan" class="btn btn-link pull-left dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="fa fa-stack">
                        <i class="fa fa-circle"></i>
                        <i class="fa fa-plus-circle"></i>   
                    </span>
                    Tambah Pengurus Perusahaan
                </div>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li class="btn-add-pengurus-perusahaan" data-type="direksi"><div>Dewan Direksi</div></li>
                    <li class="btn-add-pengurus-perusahaan" data-type="komisaris"><div>Dewan Komisaris</div></li>
                </ul>
            </div>
        </div>
    </div>
</div>