<div>

    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="<?php echo ($tab == 1) ? 'active' : '' ?>"><a href="#reg" aria-controls="reg" role="tab" data-toggle="tab">Halaman Registrasi</a></li>
        <li role="presentation" class="<?php echo ($tab == 2) ? 'active' : '' ?>"><a href="#mail" aria-controls="mail" role="tab" data-toggle="tab">Mail Server</a></li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane <?php echo ($tab == 1) ? 'active' : '' ?>" id="reg">
            <?php $this->load->view('pages/administrator/setting_parts/halaman_registrasi') ?>
        </div>
        <div role="tabpanel" class="tab-pane <?php echo ($tab == 2) ? 'active' : '' ?>" id="mail">
            <?php $this->load->view('pages/administrator/setting_parts/mail_server') ?>
        </div>
    </div>

</div>