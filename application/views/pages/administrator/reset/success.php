<div class="reset">
    <p>Tautan untuk mereset password telah terkirim ke email Anda. Ikuti petunjuk yang ada dalam email tersebut. Jika email tidak ditemukan mungkin masuk ke dalam folder spam.</p>
    <a href="<?php echo site_url('administrator') ?>" class="btn btn-info btn-block">Kembali ke Halaman Login</a>
</div>