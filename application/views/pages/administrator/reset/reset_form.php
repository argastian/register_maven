<div class="reset">
    <?php
    if ($this->session->flashdata('msg')) {
        echo $this->session->flashdata('msg');
    }
    ?>
    <p>Masukan alamat email Anda agar Kami dapat mengirim tautan untuk mereset password Anda</p>
    <?php echo form_open('', array('method' => 'POST')); ?>
    <input type="email" name="email" placeholder="Email" required="required" />
    <button type="submit" class="btn btn-primary btn-block">Proses</button>
</form>
</div>