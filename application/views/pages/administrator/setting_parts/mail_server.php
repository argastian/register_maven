<?php echo form_open(site_url('administrator/setting/submit_mail'), array('class' => 'form-horizontal')); ?>
<div class="form-container">
    <div class="form-group">
        <label for="email_protocol" class="col-sm-6">Email Protocol</label>
        <div class="col-sm-18">
            <input type="text" class="form-control" id="email_protocol" name="email_protocol" placeholder="smtp" value="<?php echo $config_mail->email_protocol ?>">
        </div>
    </div>
    <div class="form-group">
        <label for="email_smtp_host" class="col-sm-6">SMTP Host</label>
        <div class="col-sm-18">
            <input type="text" class="form-control" id="email_smtp_host" name="email_smtp_host" placeholder="ssl://mail.maven.id" value="<?php echo $config_mail->email_smtp_host ?>">
        </div>
    </div>
    <div class="form-group">
        <label for="email_smtp_port" class="col-sm-6">SMTP Port</label>
        <div class="col-sm-18">
            <input type="text" class="form-control" id="email_smtp_port" name="email_smtp_port" placeholder="465" value="<?php echo $config_mail->email_smtp_port ?>">
        </div>
    </div>
    <div class="form-group">
        <label for="email_smtp_user" class="col-sm-6">SMTP User</label>
        <div class="col-sm-18">
            <input type="text" class="form-control" id="email_smtp_user" name="email_smtp_user" placeholder="Username" value="<?php echo $config_mail->email_smtp_user ?>">
        </div>
    </div>
    <div class="form-group">
        <label for="email_smtp_pass" class="col-sm-6">SMTP Password</label>
        <div class="col-sm-18">
            <input type="password" class="form-control" id="email_smtp_pass" name="email_smtp_pass" placeholder="Password" value="<?php echo $config_mail->email_smtp_pass ?>">
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-24">
            <button type="submit" class="btn btn-primary pull-right">SIMPAN</button>
        </div>
    </div>
</div>
</form>