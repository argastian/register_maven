<?php echo form_open(site_url('administrator/setting/submit_reg'), array('class' => 'form-horizontal')); ?>
<div class="form-container">
    <div class="form-group">
        <label for="api_key_detect_language" class="col-sm-6">API Key Detect Language</label> 
        <div class="col-sm-18">
            <input type="text" class="form-control" id="api_key_detect_language" name="api_key_detect_language" placeholder="344ea31bc2c08d10828870b49b1c4fed" value="<?php echo $config_app->api_key_detect_language ?>" required="">
            <small>API Key diperoleh setelah registrasi di <a href="https://detectlanguage.com/" target="_BLANK">https://detectlanguage.com/</a></small>
        </div>
    </div>
    <div class="form-group">
        <label for="email_notifikasi_client" class="col-sm-6">Email Notifikasi Client Baru</label>
        <div class="col-sm-18">
            <input type="email" class="form-control" id="email_notifikasi_client" name="email_notifikasi_client" placeholder="jhon.doe@example.com" value="<?php echo $config_app->email_notifikasi_client ?>">
            <small>Jika lebih dari 1 pisahkan dengan koma (email1@gmail.com, email2@gmail.com)</small>
        </div>
    </div>
    <div class="form-group">
        <label for="link_youtube" class="col-sm-6">URL Video Youtube</label>
        <div class="col-sm-18">
            <input type="text" class="form-control" id="no_telpon" name="link_youtube" placeholder="https://www.youtube.com/embed/y8JnipoOcU4?list=PL2wGkZL_--7F3tkQz9RzukbYpASvAvlM4" value="<?php echo html_escape($config_app->link_youtube) ?>" required="">
            <small>Klik kanan pada video youtube kemudian klik "salin kode semat"</small>
        </div>
    </div>
    <div class="form-group">
        <label for="faq_profile" class="col-sm-6">URL FAQ Profile</label>
        <div class="col-sm-18">
            <input type="text" class="form-control" id="faq_profile" name="faq_profile" placeholder="http://maven.id/faq#profile" value="<?php echo $config_app->faq_profile ?>">
        </div>
    </div>
    <div class="form-group">
        <label for="faq_nama" class="col-sm-6">URL FAQ Nama Perusahaan</label>
        <div class="col-sm-18">
            <input type="text" class="form-control" id="faq_nama" name="faq_nama" placeholder="http://maven.id/faq#nama-perusahaan" value="<?php echo $config_app->faq_nama ?>">
        </div>
    </div>
    <div class="form-group">
        <label for="faq_alamat" class="col-sm-6">URL FAQ Alamat Perusahaan</label>
        <div class="col-sm-18">
            <input type="text" class="form-control" id="faq_alamat" name="faq_alamat" placeholder="http://maven.id/faq#alamat-perusahaan" value="<?php echo $config_app->faq_alamat ?>">
        </div>
    </div>
    <div class="form-group">
        <label for="faq_modal" class="col-sm-6">URL FAQ Modal Perusahaan</label>
        <div class="col-sm-18">
            <input type="text" class="form-control" id="faq_modal" name="faq_modal" placeholder="http://maven.id/faq#modal-perusahaan" value="<?php echo $config_app->faq_modal ?>">
        </div>
    </div>
    <div class="form-group">
        <label for="faq_pemegang_saham" class="col-sm-6">URL FAQ Pemegang Saham</label>
        <div class="col-sm-18">
            <input type="text" class="form-control" id="faq_pemegang_saham" name="faq_pemegang_saham" placeholder="http://maven.id/faq#pemegang-saham" value="<?php echo $config_app->faq_pemegang_saham ?>">
        </div>
    </div>
    <div class="form-group">
        <label for="faq_pengurus" class="col-sm-6">URL FAQ Pengurus Perusahaan</label>
        <div class="col-sm-18">
            <input type="text" class="form-control" id="faq_pengurus" name="faq_pengurus" placeholder="http://maven.id/faq#pengurus-perusahaan" value="<?php echo $config_app->faq_pengurus ?>">
        </div>
    </div>
    <div class="form-group">
        <label for="alamat_kantor" class="col-sm-6">Alamat Kantor</label>
        <div class="col-sm-18">
            <textarea class="form-control" id="alamat_kantor" name="alamat_kantor" placeholder="1635 Franklin Street Montgomery"><?php echo $config_app->alamat_kantor ?></textarea>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-24">
            <button type="submit" class="btn btn-primary pull-right">SIMPAN</button>
        </div>
    </div>
</div>
</form>