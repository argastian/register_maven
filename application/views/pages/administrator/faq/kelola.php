<?php echo form_open(site_url('administrator/faq/submit'), array('class' => 'form-horizontal')); ?>
<input type="hidden" name="id_faq" value="<?php echo isset($faq->id_faq) ? $faq->id_faq : ''; ?>">
<div class="form-container">
    <div class="form-group">
        <label for="tanya" class="col-sm-6">Tanya</label> 
        <div class="col-sm-18">
            <textarea class="form-control" id="tanya" name="tanya" required=""><?php echo isset($faq->tanya) ? $faq->tanya : ''; ?></textarea>
        </div>
    </div>
    <div class="form-group">
        <label for="jawab" class="col-sm-6">Jawab</label> 
        <div class="col-sm-18">
            <textarea class="form-control" id="jawab" name="jawab" required=""><?php echo isset($faq->jawab) ? $faq->jawab : ''; ?></textarea>
        </div>
    </div>
    <div class="form-group">
        <label for="kategori" class="col-sm-6">Kategori</label> 
        <div class="col-sm-18">
            <?php echo form_dropdown('kategori', faq_category(), (isset($faq->kategori) ? $faq->kategori : ''), array("class" => "form-control")); ?>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-24">
            <a href="<?php echo site_url('administrator/faq'); ?>" class="btn btn-default pull-left">KEMBALI</a>
            <button type="submit" class="btn btn-primary pull-right">SIMPAN</button>
        </div>
    </div>
</div>
</form>