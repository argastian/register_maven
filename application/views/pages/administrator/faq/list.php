<div id="btn-add-container" class="pull-left col-sm-12">
    <a href="<?php echo site_url('administrator/tambah-faq'); ?>" class="btn btn-primary">Tambah</a>
</div>
<table id="mvtable" class="table table-bordered table-striped table-faq" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th>No</th>
            <th>Tanya</th>
            <th>Jawab</th>
            <th>Kategori</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php
        if ($faq) {
            foreach ($faq as $item) {
                ?>
                <tr>
                    <td></td>
                    <td><?php echo $item->tanya; ?></td>
                    <td><?php echo $item->jawab; ?></td>
                    <td class="text-center"><?php echo faq_category($item->kategori); ?></td>
                    <td><a href="<?php echo site_url('administrator/kelola-faq/' . $item->id_faq); ?>" class="btn btn-xs btn-success btn-edit">Ubah</a> <a href="#" class="btn btn-xs btn-danger btn-delete" data-id_faq="<?php echo $item->id_faq; ?>" data-token="<?php echo $this->security->get_csrf_hash(); ?>">Hapus</a></td>
                </tr>
                <?php
            }
        }
        ?>
    </tbody>
</table>