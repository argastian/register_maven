<?php
echo form_open(site_url('administrator/profile/submit'), array('class' => 'form-horizontal'));
?>
<input type="hidden" name="kode" value="<?php echo $profile->kode ?>">
<div class="form-container">
    <div class="form-group">
        <label for="nama" class="col-sm-6">Nama</label> 
        <div class="col-sm-18">
            <input type="text" class="form-control" id="nama" name="nama" placeholder="344ea31bc2c08d10828870b49b1c4fed" value="<?php echo html_escape($profile->nama) ?>" required="">            
        </div>
    </div>
    <div class="form-group">
        <label for="email" class="col-sm-6">Email</label>
        <div class="col-sm-18">
            <input type="email" class="form-control" id="email" name="email" placeholder="jhon.doe@example.com" value="<?php echo html_escape($profile->email) ?>" required="">
        </div>
    </div>
    <div class="form-group">
        <label for="username" class="col-sm-6">Username</label>
        <div class="col-sm-18">
            <input type="text" class="form-control" id="username" name="username" placeholder="jhondoe" value="<?php echo html_escape($profile->username) ?>" required="">
        </div>
    </div>
    <div class="form-group">
        <label for="password" class="col-sm-6">Password Baru</label>
        <div class="col-sm-18">
            <input type="password" class="form-control" id="password" name="password" placeholder="Password Baru">
        </div>
    </div>
    <div class="form-group">
        <label for="retype_password" class="col-sm-6">Ulangi Password</label>
        <div class="col-sm-18">
            <input type="password" class="form-control" id="retype_password" name="retype_password" placeholder="Ulangi Password">
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-24">
            <button type="submit" class="btn btn-primary pull-right">SIMPAN</button>
        </div>
    </div>
</div>
</form>