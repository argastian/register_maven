<table id="mvtable" class="table table-bordered table-striped" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th>No</th>
            <th>Nama</th>
            <th>Email</th>
            <th>Telepon</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php
        if ($clients) {
            foreach ($clients as $client) {
                ?>
                <tr>
                    <td></td>
                    <td><?php echo $client->nama_lengkap; ?></td>
                    <td><?php echo $client->alamat_email; ?></td>
                    <td><?php echo $client->no_telpon; ?></td>
                    <td><a href="<?php echo site_url('administrator/detail-client/' . $client->kode) ?>" class="btn btn-xs btn-info">Detail</a></td>
                </tr>
                <?php
            }
        }
        ?>
    </tbody>
</table>