<?php

if (!function_exists('build_success_message')) {

    function build_success_message($str) {
        return "<div class='alert alert-success alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>$str</div>";
    }

}

if (!function_exists('build_warning_message')) {

    function build_warning_message($str) {
        return "<div class='alert alert-warning alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>$str</div>";
    }

}

if (!function_exists('build_error_message')) {

    function build_error_message($str) {
        return "<div class='alert alert-danger alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>$str</div>";
    }

}

function display_nama_wilayah($id_wilayah) {
    $split = explode(" ", $id_wilayah);
    $exclude = array('DKI');
    $result = array();
    foreach ($split as $word) {
        $result[] = in_array($word, $exclude) ? $word : ucfirst(strtolower($word));
    }
    echo implode(" ", $result);
}

function format_nama_wilayah($id_wilayah) {
    $split = explode(" ", $id_wilayah);
    $exclude = array('DKI');
    $result = array();
    foreach ($split as $word) {
        $result[] = in_array($word, $exclude) ? $word : ucfirst(strtolower($word));
    }
    return implode(" ", $result);
}

function generate_unique_id() {
    $id = base_convert(microtime(false), 10, 36);
    return generate_random_string(2) . $id . generate_random_string(3);
}

function generate_random_string($length) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

if (!function_exists('display_date_time')) {

    function display_date_time($date) {
        $date = strtotime($date);
        $nama_bulan = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
        return date("d", $date) . " " . $nama_bulan[(date("n", $date) - 1)] . " " . date("Y", $date) . " " . date("H:i", $date);
    }

}

if (!function_exists('display_number')) {

    function display_number($nominal, $separator = '.') {
        return number_format($nominal, 0, '', $separator);
    }

}

function base64url_encode($data) {
    return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
}

function base64url_decode($data) {
    return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
}

function faq_category($key = "") {
    $faq_category = array(
        "nama_tujuan" => "Nama & Tujuan Perusahaan",
        "domisili" => "Alamat Domisili",
        "modal" => "Modal Perusahaan",
        "organ" => "Susunan Organ Perusahaan",
        "bidang" => "Bidang Usaha",
        "file_domisili" => "Kelengkapan Berkas Domisili",
        "file_organ" => "Kelengkapan File Organ",
        "file_lain" => "Surat Keterangan Lainnya"
    );
    if ($key != "") {
        return $faq_category[$key];
    } else {
        return $faq_category;
    }
}
